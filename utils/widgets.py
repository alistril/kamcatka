from django.forms import widgets
from django.template.loader import render_to_string
from django.conf import settings

class BootstrapDateTimeInput(widgets.DateTimeInput):
    def __init__(self, *args, **kwargs):
        self.picker_format = kwargs.pop("picker_format")
        return super(BootstrapDateTimeInput, self).__init__(*args,**kwargs)

    def render(self, name, value, attrs=None):
        rendered = render_to_string('template_bootstrap_datetime.html', {
                                                                         'attrs'    : attrs,
                                                                         'name'     : name,
                                                                         'value'    : value,
                                                                         'format'   : self.picker_format
                                                                         })
        return rendered
    
    
class BootstrapImageInput(widgets.FileInput):
    def render(self, name, value, attrs=None):
        rendered = render_to_string('template_bootstrap_imageinput.html', {
                                                                         'attrs'    : attrs,
                                                                         'name'     : name,
                                                                         'value'    : value,
                                                                         "STATIC_URL" : settings.STATIC_URL
                                                                         })
        return rendered