from django.conf import settings
from django.shortcuts import render_to_response
from django.template.context import RequestContext
import os 

def browse_dir(request,template,media_path):
    path = settings.MEDIA_ROOT + "/" + media_path
    file_list = os.listdir(path)   
    return render_to_response(template, {'files': file_list}, context_instance=RequestContext(request))

def basic_context_js(request):
    return render_to_response("basic_context.js", context_instance=RequestContext(request), content_type="text/javascript")