from django.conf.urls import patterns, url
import os

app_dir = os.path.dirname(__file__)

urlpatterns = patterns('utils.views',
    url('^basic_context.js$', "basic_context_js", name='utils-basic-context-js'),
)
