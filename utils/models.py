from django.db.models import DateTimeField,ImageField

from . import forms

class BootstrapDateTimeField(DateTimeField):
    def formfield(self, **kwargs):
        defaults = {"form_class" : forms.BootstrapDateTimeField}
        defaults.update(**kwargs)
        return super(BootstrapDateTimeField, self).formfield(**defaults)

class BootstrapImageField(ImageField):
    def formfield(self, **kwargs):
        defaults = {"form_class" : forms.BootstrapImageField}
        defaults.update(**kwargs)
        return super(BootstrapImageField, self).formfield(**defaults)


from south.modelsinspector import add_introspection_rules
add_introspection_rules([], ["^utils\.models\.BootstrapDateTimeField"])
add_introspection_rules([], ["^utils\.models\.BootstrapImageField"])