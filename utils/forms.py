from django import forms
from .widgets import BootstrapDateTimeInput, BootstrapImageInput

class BootstrapDateTimeField(forms.DateTimeField):    
    def __init__(self, *args, **kwargs):
        ret = super(BootstrapDateTimeField, self).__init__(*args, **kwargs)
        self.widget = BootstrapDateTimeInput(picker_format="yyyy-MM-dd HH:mm:ss")
        return ret
        
class BootstrapImageField(forms.ImageField):    
    def __init__(self, *args, **kwargs):
        ret = super(BootstrapImageField, self).__init__(*args, **kwargs)
        self.widget = BootstrapImageInput()
        return ret
        
