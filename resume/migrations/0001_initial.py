# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Resume'
        db.create_table('resume_resume', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('resume', ['Resume'])

        # Adding model 'Experience'
        db.create_table('resume_experience', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('ckeditor.fields.RichTextField')()),
            ('date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('resume', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['resume.Resume'])),
        ))
        db.send_create_signal('resume', ['Experience'])

        # Adding model 'Link'
        db.create_table('resume_link', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('href', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('experience', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['resume.Experience'])),
        ))
        db.send_create_signal('resume', ['Link'])


    def backwards(self, orm):
        # Deleting model 'Resume'
        db.delete_table('resume_resume')

        # Deleting model 'Experience'
        db.delete_table('resume_experience')

        # Deleting model 'Link'
        db.delete_table('resume_link')


    models = {
        'resume.experience': {
            'Meta': {'object_name': 'Experience'},
            'date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('ckeditor.fields.RichTextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'resume': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['resume.Resume']"})
        },
        'resume.link': {
            'Meta': {'object_name': 'Link'},
            'experience': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['resume.Experience']"}),
            'href': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'resume.resume': {
            'Meta': {'object_name': 'Resume'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['resume']