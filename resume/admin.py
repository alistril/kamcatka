from django.contrib import admin
from mezzanine.pages.admin import PageAdmin
from .models import Experience,Link,Resume,PageWithResume

class LinkInline(admin.TabularInline):
    model = Link

class ExperienceAdmin(admin.ModelAdmin):
    list_display = ('date', 'description','resume')
    inlines = [
        LinkInline,
    ]
    

admin.site.register(Experience, ExperienceAdmin)
admin.site.register(Link)
admin.site.register(Resume)
admin.site.register(PageWithResume,PageAdmin)