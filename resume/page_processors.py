from mezzanine.pages.page_processors import processor_for
from expeditions.models import Expedition

@processor_for("my-expeditions")
def expedition_list(request, page):    
    expeditions = Expedition.objects.all()
    return {"expeditions": expeditions}