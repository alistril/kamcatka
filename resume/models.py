from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from mezzanine.pages.models import Page

from mezzanine.core.fields import RichTextField
from transmeta import TransMeta
try:
    from _pyio import __metaclass__
except:
    __metaclass__ = type
from transmeta_pages.meta import MetaPageTranslate
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

class Resume(models.Model):
    name = models.CharField(max_length=255)
    date_column_name = models.CharField(max_length=255,default=u"Name")
    description_column_name = models.CharField(max_length=255,default=u"Description")
    picture_column_name = models.CharField(max_length=255,default=u"Snapshot")
    
    def __unicode__(self):
        return unicode(self.name)
    
class Experience(models.Model):
    __metaclass__ = TransMeta    
    description = RichTextField(verbose_name="description")            
        
    date = models.DateField(null=True,blank=True)
    picture = models.ImageField(null=True,blank=True,upload_to="uploads")
    picture_reasonable = ImageSpecField(image_field='picture',
                                       processors=[ResizeToFill(700, 500)],
                                       format='PNG')
    picture_thumbnail = ImageSpecField(image_field='picture',
                                       processors=[ResizeToFill(250, 200)],
                                       format='PNG')
    resume = models.ForeignKey(Resume)
    
    class Meta:
        translate = ('description', )
        ordering = ('date',)
        
    def __unicode__(self):
        return unicode(self.date)

class Link(models.Model):
    __metaclass__ = TransMeta
    
    title = models.CharField(max_length=255,verbose_name="title")
    href = models.CharField(max_length=255) 
    experience = models.ForeignKey(Experience)
    
    class Meta:
        translate = ('title', )
        
    def __unicode__(self):
        return unicode(self.title)


class PageWithResume(Page):
    __metaclass__ = MetaPageTranslate
    
    content = RichTextField(verbose_name="content")
    resume = models.ForeignKey(Resume)
    
    class Meta:
        translate = ('content', )
