import imaplib
import email
import re
import pytz
from datetime import datetime
try:
    from collections import OrderedDict
except:
    from ordereddict import OrderedDict
from geopy import geocoders
from geopy.exc import GeocoderQuotaExceeded
from expeditions.models import Coordinate
from django.contrib.gis.geos import Point
import time as time_module
import csv

def extract_body(payload):
    if isinstance(payload,str):
        return payload
    else:
        return '\n'.join([extract_body(part.get_payload()) for part in payload])

def get_gmail_coordinates_ordered_dict(login,password):
    re_latitude = re.compile('^[\w\s]*Latitude\:(\-*[0-9]+\.*[0-9]*)[\w\s\-]*$',re.MULTILINE)
    re_longitude = re.compile('^[\w\s\:\.0-9\-]*Longitude\:(\-*[0-9]+\.*[0-9]*)[\w\s]*$',re.MULTILINE)
    re_time = re.compile('^[\w\s\:\.0-9\-]*GPS\ location\ Date\/Time\:([0-9]+\/[0-9]+\/[0-9]+\s*[0-9]+\:[0-9]+\:[0-9]+)\s*([A-Z]+)[\w\s]*$',re.MULTILINE)
    
    conn = imaplib.IMAP4_SSL("imap.gmail.com", 993)
    conn.login(login,password)
    conn.select()
    typ, data = conn.search(None, 'UNSEEN')
    beacon_dict = {}
    try:
        for num in data[0].split():
            typ, msg_data = conn.fetch(num, '(RFC822)')
            for response_part in msg_data:
                if isinstance(response_part, tuple):
                    msg = email.message_from_string(response_part[1])
                    subject=msg['subject']                   
                    payload=msg.get_payload()
                    body=extract_body(payload)
                    #res = re_longitude.match( body )

                    res = re_latitude.match(body)
                    if res:
                        latitude = float(res.group(1))
                    else:
                        raise Exception("no match for latitude")
                    
                    res = re_longitude.match(body)
                    if res:
                        longitude = float(res.group(1))
                    else:
                        raise Exception("no match for longitude")
                    
                    res = re_time.match(body)
                    if res:
                        time = res.group(1)
                        time_zone = res.group(2)
                        timezone = pytz.timezone("America/Vancouver")

                        date_time = timezone.localize(datetime.strptime(time,"%m/%d/%Y %H:%M:%S"))                        
                    else:
                        raise Exception("no match for time")
                    
                    beacon_dict.update({date_time : {'latitude' : latitude, 'longitude' : longitude}})
    
            #typ, response = conn.store(num, '+FLAGS', r'(\Seen)')
    finally:
        try:
            conn.close()
        except:
            pass
        conn.logout()
    od = OrderedDict(sorted(beacon_dict.items(),key=lambda t: t[0]))
    return od

def write_csv(login,password,filename):
    myfile = open(filename, 'wb')
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    wr.writerow(['Time','Lng','Lat'])
    od = get_gmail_coordinates_ordered_dict(login,password)
    for key,value in od.items()[1:-1]:
        wr.writerow([key,value['longitude'],value['latitude']])
        
    myfile.close()
    
def get_gmail_coordinates(login,password):
    od = get_gmail_coordinates_ordered_dict(login,password)
    if not len(od):
        return []
    orderedlist = []
    #g = geocoders.Google('AIzaSyBHJss7PcRCF6LetuPMWBIgtok-NITCrMM')
    g = geocoders.googlev3.GoogleV3()

    key,value = od.items()[0]
    
    for attempts in range(1,10):
        try:
            place, (lat, lng) = g.geocode("%f,%f" % (value['latitude'],value['longitude']))
            orderedlist.append(Coordinate(name=place,datetime=key,datetime_precision="0 days,0 hours, 0 minutes, 0 seconds",address=place,location=Point(value['longitude'],value['latitude'])))
        except GeocoderQuotaExceeded:
            time_module.sleep(attempts)
            print "sleeping %d secs on (%f,%f)" % (attempts, value['latitude'],value['longitude'])
        else:
            break
    
    for key,value in od.items()[1:-1]:
        #handle request bombardment security
        for attempts in range(1,10):
            try:
                place, (lat, lng) = g.geocode("%f,%f" % (value['latitude'],value['longitude']))
                orderedlist.append(Coordinate(name=place,datetime=key,datetime_precision="0 days,0 hours, 0 minutes, 0 seconds",address=place,location=Point(value['longitude'],value['latitude'])))
            except GeocoderQuotaExceeded:
                time_module.sleep(attempts)
                print "sleeping %d secs on (%f,%f)" % (attempts, value['latitude'],value['longitude'])
            else:
                break
            
    key,value = od.items()[-1]
    for attempts in range(1,10):
        try:
            place, (lat, lng) = g.geocode("%f,%f" % (value['latitude'],value['longitude']))
            orderedlist.append(Coordinate(name=place,datetime=key,datetime_precision="0 days,0 hours, 0 minutes, 0 seconds",address=place,location=Point(value['longitude'],value['latitude'])))
        except GeocoderQuotaExceeded:
            time_module.sleep(attempts)
            print "sleeping %d secs on (%f,%f)" % (attempts, value['latitude'],value['longitude'])
        else:
            break
        
    return orderedlist
