import multiprocessing

bind = "localhost:18731"
workers = multiprocessing.cpu_count() * 2 + 1
worker_class = "gevent"
