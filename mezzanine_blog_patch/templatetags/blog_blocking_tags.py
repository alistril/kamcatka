from django import template
from mezzanine_blog_patch.models import BlogPostBlockedInPlace 
from django.contrib.gis.geoip import GeoIP

register = template.Library()

class IsBlockedForIpNode(template.Node):
    def __init__(self, nodelist, blogpost,ip):
        self.blogpost = template.Variable(blogpost)
        self.ip       = template.Variable(ip)
        self.nodelist = nodelist

    def render(self, context):
        try:
            blogpost = self.blogpost.resolve(context)
            ip = self.ip.resolve(context)
            
            g = GeoIP()
            country = g.country(ip)
            if BlogPostBlockedInPlace.objects.filter(blogpost=blogpost,place__iso2=country['country_code']).exists():
                return ""
            else:
                return self.nodelist.render(context)
                    
        except template.VariableDoesNotExist:
            return self.nodelist.render(context)
        
@register.tag
def deny_if_blocked(parser, token):
    nodelist = parser.parse(('enddeny',))
    parser.delete_first_token()
    
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, blogpost, ip = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires two arguments" % token.contents.split()[0])

    return IsBlockedForIpNode(nodelist, blogpost,ip)

