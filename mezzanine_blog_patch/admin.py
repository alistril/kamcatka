from django.contrib import admin
from .models import BlogPostBlockedInPlace

class BlogPostBlockedInPlaceAdmin(admin.ModelAdmin):
    list_display = ('blogpost','place')

admin.site.register(BlogPostBlockedInPlace,BlogPostBlockedInPlaceAdmin)
