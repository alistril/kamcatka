from django.db import models
from mezzanine.blog.models import BlogPost
#this is actually a disguised manytomanyfield
class BlogPostBlockedInPlace(models.Model):
    blogpost = models.ForeignKey(BlogPost)
    place = models.ForeignKey("places.Place")
    
    