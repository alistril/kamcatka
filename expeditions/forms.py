from django import forms

from .models import Coordinate,CoordinateDescription
from django.forms.widgets import HiddenInput
from utils.widgets import BootstrapDateTimeInput
from django.contrib.gis.measure import D
from django.utils.timezone import localtime

from mezzanine.conf import settings
from django.utils.translation import ugettext as _

class CoordinateForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        self.hitchgraph = kwargs.pop("hitchgraph")
        return super(CoordinateForm, self).__init__(*args,**kwargs) 
    
    def clean(self):
        settings.use_editable()
        closest_coord_in_time       = self.hitchgraph.coordinates_qs().filter(datetime__lte=self.cleaned_data['datetime']).distance(self.cleaned_data['location']).latest()
        
        time_gap = (self.cleaned_data['datetime'] - closest_coord_in_time.datetime).days or 1
        
        if closest_coord_in_time.distance < D(km=settings.MAX_KILOMETERS_PER_DAY)*time_gap:
            if closest_coord_in_time.distance < D(km=settings.MIN_NOTICABLE_DISTANCE):
                self.coordinate_met = closest_coord_in_time #just edit this coordinate, don't bother creating another one                
        else: #the distance is too big to be realistic
            #is the closest coordiante a better choice?
            closest_coord_in_space  = self.hitchgraph.coordinates_qs().distance(self.cleaned_data['location']).order_by("distance")[0]
            
            if closest_coord_in_space.distance < D(km=settings.MAX_KILOMETERS_PER_DAY)*time_gap:
                self._errors["datetime"] = (_("It appears your time is not right. Did you write the correct date and time? You say we met around %s on %s. \
                                                However on %s, we were in %s which is %d kilometers away! It's unliklely we have travelled such a big distance (bigger than %d). \
                                                Your location seems to be near this location: %s. We reached this location on %s. Maybe your right time is closer to that moment...") % (
                                               self.cleaned_data['name'],
                                               localtime(self.cleaned_data['datetime']).strftime('%c'),
                                               localtime(closest_coord_in_time.datetime).strftime('%c'),
                                               closest_coord_in_time.name or closest_coord_in_time.address,
                                               closest_coord_in_time.distance.km,
                                               D(km=settings.MAX_KILOMETERS_PER_DAY).km*time_gap,
                                               closest_coord_in_space.name or closest_coord_in_space.address,
                                               localtime(closest_coord_in_space.datetime).strftime('%c')
                                               ),)
                del self.cleaned_data["datetime"]
            else:
                self._errors["name"] = (_("It appears your location is not right. Check on the map if the location you wrote is correct. \
                    Reposition the marker on the map manually if necessary. You say we met around %s on %s. \
                    However on %s, we were in %s which is %d kilometers away! It's unliklely we have travelled such a big distance (bigger than %d).") %  (
                        self.cleaned_data['name'],
                        localtime(self.cleaned_data['datetime']).strftime('%c'),
                        localtime(closest_coord_in_time.datetime).strftime('%c'),
                        closest_coord_in_time.name or closest_coord_in_time.address,
                        closest_coord_in_time.distance.km,
                        D(km=settings.MAX_KILOMETERS_PER_DAY).km*time_gap,
                    ),)
                del self.cleaned_data["name"]
                                                             
        return self.cleaned_data

    class Meta:
        model = Coordinate
        fields = ("name", "address","datetime","location")
        
        widgets = {
            'location': HiddenInput(),
            'datetime' : BootstrapDateTimeInput(picker_format="yyyy-MM-dd HH:mm:ss")
        }

        

class CoordinateDescriptionForm(forms.ModelForm):
    class Meta:
        model = CoordinateDescription
        fields = ("picture", "comments")
        
class ImageForm(forms.Form):
    picture = forms.ImageField()
