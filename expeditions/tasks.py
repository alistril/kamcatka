from celery import task
from spot_client import spot,google
from django.conf import settings
import datetime

from expeditions.models import Coordinate, HitchGraph,Displacement
from django.contrib.gis.geos.geometry import Point
from mezzanine.conf import settings
from django.contrib.auth.models import User 

import logging

logger = logging.getLogger(__name__)


@task()
def add_gps_coordinates():
    settings.use_editable()
    sp = spot.SPOTClient(settings.GPS_BEACON_CONFIG["SPOT"])    
    
    hitchgraph = HitchGraph.objects.get(pk=settings.ACTIVE_HITCHGRAPH)
    
    try:
        messages = sp.messages.after_moment(hitchgraph.coordinates_qs().filter(initiator=Coordinate.GPS).latest().datetime)
    except Coordinate.DoesNotExist:
        messages = sp.messages.all_available()
    
    gn = google.GoogleNamer(settings.GPS_BEACON_CONFIG["GOOGLE"])
    gn.add_names(messages)
    
    message_type_to_category = { 
                                spot.SPOTMessageType.OK     : Coordinate.OK,
                                spot.SPOTMessageType.CUSTOM : Coordinate.CAMP,
                                spot.SPOTMessageType.HELP   : Coordinate.HELP,
                                spot.SPOTMessageType.SOS    : Coordinate.SOS, 
                                }
    for message in messages:        
        coord = Coordinate.objects.create(
                                          initiator= Coordinate.GPS,
                                          category = message_type_to_category[message.messageType],
                                          datetime = message.datetime,
                                          name     = message.name,
                                          address  = message.name,
                                          location = Point(message.position.y,message.position.x),
                                          battery_state = message.battery_state
                                          )
        
        coord.reposition(hitchgraph,coord.datetime)
        if message.content: #add without user, kind of as an operator
            coord.coordinatedescription_set.create(comments=message.content)
        #am I in between the same user?
        try:
            common_users = User.objects.filter(
                                               pk__in=coord.get_next_coordinate().coordinatedescription_set.filter(
                                                                                           user__in=coord.get_prev_coordinate().coordinatedescription_set.values_list('user',flat=True)
                                                                                           ).values_list('user',flat=True)
                                               )
                                                        
            for user in common_users: #there shouldn't be so many
                coord.coordinatedescription_set.create(user = user)
        except Displacement.DoesNotExist:
            pass
            

    #and now transform to coordinates    
