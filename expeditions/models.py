from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.contrib.gis.geoip import GeoIP

from django.contrib.auth.models import User 
from mezzanine.pages.models import Page
 
from mezzanine.core.fields import RichTextField
import timedelta
from transmeta import TransMeta
from django.db.models import Q
from django.db.models.signals import pre_delete,pre_save
from django.dispatch import receiver

from location_field.models import LocationField

from django.core.exceptions import ImproperlyConfigured
from utils.models import BootstrapImageField
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

try:
    from _pyio import __metaclass__
except:
    __metaclass__ = type
    
from transmeta_pages.meta import MetaPageTranslate

class CoordinateVisibilityMixin(models.Model):
    show_on_map = models.BooleanField(default=True)
    show_on_hitchgraph = models.BooleanField(default=True)    
    
    class Meta:
        abstract = True
        
class Coordinate(models.Model):
    USER    = 1
    GPS     = 2
    SITE    = 3    
    
    INITIATOR_CHOICES = (
        (USER, 'Person we hitchhiked with'),
        (GPS, 'GPS beacon'),
        (SITE, 'Site administration')
    )
    
    OK      = 1
    CAMP    = 2
    HELP    = 3
    SOS     = 4
    
    CATEGORY_CHOICES = (
        (OK, 'Casual location report'),
        (CAMP, 'Camp location report'),
        (HELP, 'Distress signal'),
        (SOS, 'SOS')
    )
    
    GOOD   = 1
    LOW    = 2

    BATTERY_STATE_CHOICES = (
        (GOOD, 'Enough battery'),
        (LOW, 'Low battery')
    )

    name = models.CharField(max_length=255, null=True, blank=True)
    
    initiator       = models.IntegerField(choices=INITIATOR_CHOICES,default=USER)
    category        = models.IntegerField(choices=CATEGORY_CHOICES,default=OK)
    battery_state   = models.IntegerField(choices=BATTERY_STATE_CHOICES,null=True,blank=True)
    
    address = models.CharField("address",max_length=255,null=True)    
    location = LocationField(based_fields=[address], zoom=7, default=Point(0, 0))    
    
    datetime = models.DateTimeField(null=True,blank=True)
    datetime_precision = timedelta.fields.TimedeltaField(null=True,blank=True)
    
    objects = models.GeoManager()
    
    def get_next_coordinate(self):
        return self.source_displacement_related.destination
        
        
    def get_prev_coordinate(self):        
        return self.destination_displacement_related.source
        
    def start_of(self):
        try:
            return self.start_coordinate_related.get()
        except HitchGraph.DoesNotExist:
            return None
        
    def finish_of(self):
        try:
            return self.finish_coordinate_related.get()
        except HitchGraph.DoesNotExist:
            return None
    
    def get_hitchgraph(self):
        if self.pk is None:
            return None
        try:
            return self.source_displacement_related.hitchgraph
        except Displacement.DoesNotExist: 
            try:
                return self.destination_displacement_related.hitchgraph
            except Displacement.DoesNotExist: 
                try:
                    return self.start_coordinate_related.get()
                except HitchGraph.DoesNotExist:
                    try:
                        return self.finish_coordinate_related.get()
                    except HitchGraph.DoesNotExist:
                        return None

    #try repositioning at the end of a sub-hitchgraph, if available (ie try to preserve groups)
    def reposition(self,hitchgraph,datetime):
        coordinate_users = self.coordinatedescription_set.values_list('user',flat=True).distinct()
        try:            
            #closest coordinate just after me or start (if start is after me)
            closest_coordinate_after_candidates = hitchgraph.coordinates_qs().filter(
                                                                                    Q(destination_displacement_related__source__datetime__lte = datetime) |
                                                                                    Q(destination_displacement_related = None),
                                                                                    datetime__gte=datetime                                                                                    
                                                                                    )
            closest_coordinate_after_candidate = closest_coordinate_after_candidates.distance(self.location).order_by("distance")[0:1].get()
            #Hey aftter's datetime is the same as mine. If I try to get after him (after the after) the distance to him will be
            #the same anyway Me<---(D)---->after same as after<---(D)---->Me
            #This illustrates the philosphphy: always push the marker closer to the finish line
            #don't worry about overruning the string, if an exception is thrown, we'll be added as finish    
            if closest_coordinate_after_candidate.datetime == datetime:
                closest_coordinate_after_candidate = closest_coordinate_after_candidate.source_displacement_related.destination
            try:
                closest_coordinate_before_candidate = closest_coordinate_after_candidate.destination_displacement_related.source
                #are we breaking user continuity? Or user group continuity to be more precise
                #the test here tests whether the after and before and myself are disjoint sets or not.
                #If they're not disjoined, then we say the group continuity is preserved
                if closest_coordinate_after_candidate.coordinatedescription_set.filter(user__in=coordinate_users).exists() and closest_coordinate_before_candidate.coordinatedescription_set.filter(user__in=coordinate_users).exists():
                    #we are not braking continuity, let's select our coordinate!
                    previous_coordinate = closest_coordinate_before_candidate
                else:
                    try:
                        end_of_sub_hitchgraphs = hitchgraph.last_coordinates_for_users(coordinate_users).filter(datetime__lte=datetime).distance(self.location).latest()
                        #if the closest coordinate before is equal to my timestamp, then takeing the end_of_sub_hitchgraphs as
                        #previous coordinate will is a reasonable possibility because it preserves group
                        if closest_coordinate_before_candidate.datetime == datetime:
                            previous_coordinate = end_of_sub_hitchgraphs
                        #otherwise I am bigger than closest_coordinate_before_candidate, I should be added at the end of my datetime class
                        else:           
                            #if the next coordinate is further in time than I must follow its source otherwise I'll end up somewhere 
                            #before the next coordinate which might have the same timestamp 
                            if closest_coordinate_after_candidate.datetime > end_of_sub_hitchgraphs.datetime:
                                previous_coordinate = closest_coordinate_before_candidate
                            else:
                                previous_coordinate = end_of_sub_hitchgraphs
                    except Coordinate.DoesNotExist: #there are no ends of sub-hitchgraph before my new date
                        previous_coordinate = closest_coordinate_before_candidate
                
                next_coordinate = previous_coordinate.source_displacement_related.destination
                displacement = next_coordinate.destination_displacement_related
                
                #whatever happened, if we are to break a displacement where we would be either previous or next
                #do nothing, this would cause an empty displacement otherwise!!
                if next_coordinate==self or previous_coordinate==self: 
                    return
                
                displacement.delete()  
                hitchgraph.displacement_set.create(source=previous_coordinate,destination=self)
                hitchgraph.displacement_set.create(source=self,destination=next_coordinate)
                
            except Displacement.DoesNotExist: #there is no prev coordinate, we are the new start
                #if we are the start already, no operation
                if hitchgraph.start == self:
                    return
                old_start = hitchgraph.start
                hitchgraph.start = self
                hitchgraph.save()
                hitchgraph.displacement_set.create(source=self,destination=old_start)
        except (Displacement.DoesNotExist, Coordinate.DoesNotExist): #there are no coordinate after the new me, I am the new finish
            #if we are the finish already, no operation
            if hitchgraph.finish == self:
                return
            old_finish = hitchgraph.finish
            hitchgraph.finish = self
            hitchgraph.save()
            hitchgraph.displacement_set.create(source=old_finish,destination=self)
            
    ########################################################
    #THIS MARKS ALL GPS BEACONS FOR ALL USERS IF NECESSARY #
    ########################################################
    #give gps to the user where it is needed
    def mark_gps(self,hitchgraph):        
        coordinate_users = User.objects.filter(coordinatedescription__coordinate=self).distinct()
        for user in coordinate_users:
            hitchgraph.mark_gps_for_user(user)
                     
    def __unicode__(self):
        if self.name:
            return unicode(self.name)
        else:
            return unicode(self.address)
    
    def get_admin_url(self):
        return "/admin/expeditions/coordinate/%d/" % self.id
    class Meta:
        ordering = ['datetime']
        get_latest_by = 'datetime'


@receiver(pre_delete, sender=Coordinate, dispatch_uid='coordinate_pre_delete')
def coordinate_pre_delete(sender, instance, using,**kwargs):
    try:
        displacement_before = instance.destination_displacement_related
        coordinate_before = displacement_before.source
    except Displacement.DoesNotExist:
        displacement_before = None
                             
    try: 
        displacement_after = instance.source_displacement_related                    
        coordinate_after = displacement_after.destination
    except Displacement.DoesNotExist:
        displacement_after = None
        
    try:
        if displacement_before is None and displacement_after is None: #the coordinate is alone in the hitchgraph, delete the whole hitchgraph
            hitchgraph = instance.start_coordinate_related.get()
            hitchgraph.delete()
            return    
        elif displacement_before is None: #is start            
            hitchgraph = instance.start_coordinate_related.get() #there should be only one
            hitchgraph.start = coordinate_after
            hitchgraph.save()
            displacement_after.delete()
        elif displacement_after is None: #is finish
            hitchgraph = instance.finish_coordinate_related.get()
            hitchgraph.finish = coordinate_before
            hitchgraph.save()            
            displacement_before.delete()            
        else: #is somewhere in the middle
            hitchgraph = displacement_before.hitchgraph
            displacement_before.delete()
            displacement_after.delete()
            hitchgraph.displacement_set.create(source=coordinate_before,destination=coordinate_after)
    except HitchGraph.DoesNotExist:
        return #no hitchgraph, no remodelling action


#reposition coordinate on hitchgraph
@receiver(pre_save, sender=Coordinate, dispatch_uid='coordinate_pre_save_update')
def coordinate_pre_save(sender, instance, raw, using, update_fields,**kwargs):
    if raw:
        return
    
    if instance.pk: # update case
        old_instance = Coordinate.objects.get(pk=instance.pk)
        if(old_instance.datetime==instance.datetime): #no repositioning if date is same
            return
        try:
            displacement_before = instance.destination_displacement_related
            coordinate_before = displacement_before.source
        except Displacement.DoesNotExist:
            displacement_before = None
                                 
        try: 
            displacement_after = instance.source_displacement_related                    
            coordinate_after = displacement_after.destination
        except Displacement.DoesNotExist:
            displacement_after = None
        
        try:
            if displacement_before is None and displacement_after is None: #the coordinate is alone in the hitchgraph, the hitchgraph will be deleted automatically anyway by cascade, no action
                return    
            elif displacement_before is None: #is start
                hitchgraph = instance.start_coordinate_related.get()            
            elif displacement_after is None: #is finish
                hitchgraph = instance.finish_coordinate_related.get()
            else: #is somewhere in the middle
                hitchgraph = displacement_before.hitchgraph
        except HitchGraph.DoesNotExist:
            return #no hitchgraph, no remodelling action
        

        if displacement_before is None and displacement_after is None: #the coordinate is alone in the hitchgraph, the hitchgraph will be deleted automatically anyway by cascade, no action
            return    
        elif displacement_before is None: #is start
            if instance.datetime <= old_instance.datetime: #just reposition start before, no need to change anything
                return
            elif instance.datetime > old_instance.datetime: #if start has moved forward
                if coordinate_after.datetime < instance.datetime: #my next coordinate is earlier than me, I must reposition myself
                    hitchgraph.start = coordinate_after
                    hitchgraph.save()
                    displacement_after.delete() #don't need the displacement old-me--->new start anymore (it's nonsence ending at start too)
                    instance.reposition(hitchgraph,instance.datetime)
                else: #I am just moving in time a little bit, no need for repositioning
                    return            
        elif displacement_after is None: #is finish
            if instance.datetime >= old_instance.datetime: #just reposition finish after, no need to change anything
                return
            elif instance.datetime <  old_instance.datetime: #if finish has moved backwards
                if coordinate_before.datetime > instance.datetime: #my previous coordinate is later than me, I must reposition myself
                    hitchgraph.finish = coordinate_before
                    hitchgraph.save()
                    displacement_before.delete()
                    instance.reposition(hitchgraph,instance.datetime)
                else: #I am just moving in time a little bit, no need for repositioning
                    return
        else: #is somewhere in the middle
            if coordinate_before.datetime > instance.datetime or coordinate_after.datetime < instance.datetime:
                hitchgraph = displacement_before.hitchgraph
                displacement_before.delete()
                displacement_after.delete()
                hitchgraph.displacement_set.create(source=coordinate_before,destination=coordinate_after)
                instance.reposition(hitchgraph,instance.datetime)



class CoordinateDescription(models.Model):    
    coordinate = models.ForeignKey(Coordinate)
    user = models.ForeignKey(User,null=True,blank=True)
    picture = BootstrapImageField(null=True,blank=True,upload_to="uploads")
    picture_reasonable = ImageSpecField(image_field='picture',
                                       processors=[ResizeToFill(600, 400)],
                                       format='PNG')
    picture_thumbnail = ImageSpecField(image_field='picture',
                                       processors=[ResizeToFill(240, 300)],
                                       format='PNG')
    picture_thumbnail_mini = ImageSpecField(image_field='picture',
                                       processors=[ResizeToFill(150, 150)],
                                       format='PNG')

    comments = models.TextField(null=True,blank=True)
    
    def __unicode__(self):
        return "CoordinateDescription of %s by %s" % (self.coordinate.name or self.coordinate.address, self.user or "none")


class Expedition(Page):
    __metaclass__ = MetaPageTranslate

    destination = models.CharField(max_length=255, null=True, blank=True,verbose_name="destination")
    story = RichTextField(verbose_name="story")    
    #date = models.DateField(null=True,blank=True)
    #picture = models.ImageField(null=True,blank=True,upload_to="uploads")
    
    class Meta:
        translate = ('destination', 'story', )
    
class OrderedBySourceManager(models.Manager):
    def get_query_set(self):
        default_queryset = super(OrderedBySourceManager, self).get_query_set()
        return default_queryset.all().order_by('source__datetime')

    use_for_related_fields = True
    
class OrderedBySourceAndFiltredOnUserManager(models.Manager):
    def set_user(self, user):
        self.filter_user = user
        
    def get_query_set(self):
        default_queryset = super(OrderedBySourceManager, self).get_query_set()
        
        return default_queryset.filter(source__coordinatedescription__user=self.filter_user,destination__coordinatedescription__user=self.filter_user).distinct().order_by('source__datetime')
    
    use_for_related_fields = True

class HitchGraph(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    start = models.ForeignKey("Coordinate",related_name="start_coordinate_related",on_delete=models.DO_NOTHING)
    finish = models.ForeignKey("Coordinate",related_name="finish_coordinate_related",on_delete=models.DO_NOTHING)
    
    expedition = models.ManyToManyField(Expedition)
    
    places_blocked = models.ManyToManyField("places.Place",null=True,blank=True)

    def is_blocked(self,ip):
        g = GeoIP()
        country = g.country(ip)
        return self.places_blocked.filter(name=country).exists()

        
    def coordinates_qs(self):
        return Coordinate.objects.filter(Q(source_displacement_related__hitchgraph=self) | 
                                  Q(destination_displacement_related__hitchgraph=self) |
                                  Q(start_coordinate_related=self) | #start/finish relateds refer to the hitchgraph related to those coordinates
                                  Q(finish_coordinate_related=self)
                                  ).distinct()
                                  
    def extract_user(self,*args):
        if len(args):
            user = args[0]
        elif self.filter_user: 
            user = self.filter_user
        else:
            raise ImproperlyConfigured("User not configured for hitchgraph")
        return user
    
    def coordinate_before_user_qs(self,*args):
        user = self.extract_user(*args)
        before_coords_qs = self.coordinates_qs().exclude(coordinatedescription__user=user).filter(
                                                        source_displacement_related__destination__coordinatedescription__user=user
                                                                ).distinct().order_by("datetime")
        return before_coords_qs 
        
    def displacement_before_user_qs(self,*args):        
        user = self.extract_user(*args)
        before_coords_qs = self.displacement_set.filter(destination__coordinatedescription__user=user).exclude(source__coordinatedescription__user=user).distinct().order_by("source__datetime")
        return before_coords_qs
    
    def coordinate_after_user_qs(self,*args):
        user = self.extract_user(*args)
        before_coords_qs = self.coordinates_qs().exclude(coordinatedescription__user=user).filter(
                                                        destination_displacement_related__source__coordinatedescription__user=user
                                                                ).distinct().order_by("datetime")
        return before_coords_qs 
        
    def displacement_after_user_qs(self,*args):        
        user = self.extract_user(*args)
        before_coords_qs = self.displacement_set.filter(source__coordinatedescription__user=user).exclude(destination__coordinatedescription__user=user).distinct().order_by("source__datetime")
        return before_coords_qs
            
    def last_coordinates_for_user(self,*args):
        #select coord belonging to hitchgraph which is a source of a displacement of which the destination is NOT user-based
        #and which is still user-based
        #That coordinate will be the alst user-based coordinate
        user = self.extract_user(*args)
        last_coord_qs = self.coordinates_qs().filter(coordinatedescription__user=user).distinct().exclude(
                                                        source_displacement_related__destination__coordinatedescription__user=user
                                                                ).order_by("datetime")
        return last_coord_qs
        
    def first_coordinates_for_user(self,*args):
        #select coord belonging to hitchgraph which is a destination of a displacement of which the source is NOT user-based
        #and which is still user-based
        #That coordinate will be the first user-based coordinate
        user = self.extract_user(*args)
        first_coord_qs = self.coordinates_qs().filter(coordinatedescription__user=user).distinct().exclude(
                                                        destination_displacement_related__source__coordinatedescription__user=user
                                                            ).order_by("datetime")
        return first_coord_qs

    def last_coordinates_for_users(self,user_list):
        #select coord belonging to hitchgraph which is a source of a displacement of which the destination is NOT user-based
        #and which is still user-based
        #That coordinate will be the alst user-based coordinate
        last_coord_qs = self.coordinates_qs().filter(coordinatedescription__user__in=user_list).distinct().exclude(
                                                        source_displacement_related__destination__coordinatedescription__user__in=user_list
                                                                ).order_by("datetime")
        return last_coord_qs
    
    def first_coordinates_for_users(self,user_list):
        #select coord belonging to hitchgraph which is a destination of a displacement of which the source is NOT user-based
        #and which is still user-based
        #That coordinate will be the first user-based coordinate
        first_coord_qs = self.coordinates_qs().filter(coordinatedescription__user__in=user_list).distinct().exclude(
                                                        destination_displacement_related__source__coordinatedescription__user__in=user_list
                                                            ).order_by("datetime")
        return first_coord_qs
                                                                        
    def filter_by_user(self,user):
        self.filter_user = user
    
    def displacement_set_filtered_by_user(self,*args):
        user = self.extract_user(*args)
        return self.displacement_set.filter(source__coordinatedescription__user=user,destination__coordinatedescription__user=user).distinct().order_by('source__datetime')
    
    def __init__(self,*args,**kwargs):
        self.filter_user = None;
        return super(HitchGraph,self).__init__(*args,**kwargs)
    
    ########################################################
    #THIS MARKS ALL GPS BEACONS FOR A USERS IF NECESSARY   #
    ########################################################
    #TODO: implement as a task
    #give gps to the user where it is needed
    #mark coordinates when they are GPS and no user is associated to them.
    def mark_gps_for_user(self, user):
        ends_of_sub_hitchgraphs = self.last_coordinates_for_user(user).order_by("datetime")
        last_end = None
        for end in ends_of_sub_hitchgraphs:
            if last_end is None:
                last_end = end
                continue
            else:
                #I don't like this but I see no better solution without redesigning the db
                foreign_coordinates = self.coordinates_qs().filter(
                                                                    datetime__lt=end.datetime,
                                                                    datetime__gt=last_end.datetime                
                                                                    ).exclude(coordinatedescription__user=user) #.exclude(initiator=Coordinate.GPS)
                                                                    
                empty_coordinates = self.coordinates_qs().filter(
                                                                    datetime__lte=end.datetime,
                                                                    datetime__gte=last_end.datetime,
                                                                    initiator=Coordinate.GPS,
                                                                    coordinatedescription__user=None
                                                                    )
                if empty_coordinates.exists() and not foreign_coordinates.exists(): #most of the space in-between is "free"
                    cur = last_end.source_displacement_related.destination
                    while cur!=end: #should not twrow any exceptions since cur and end are well-defined and the graph is continuous
                        if cur.initiator==Coordinate.GPS:
                            if cur.coordinatedescription_set.exclude(user=None).exists():
                                #this is a GPS coordinate with other users
                                if cur.coordinatedescription_set.filter(user=user).exists():
                                    #this coordinate is already mine
                                    #do nothing
                                    pass                                    
                                else:
                                    #this coordinate belongs to another. abort everything, the gps trail owning ends here
                                    break
                            else:
                                #this coordinate either does not have a description or is described by an empty user (auto gps)                                
                                cur.coordinatedescription_set.create(user=user)
                        else:
                            #this is not a gps coordinate. abor everything, the gps trail ends here
                            break
                        cur = cur.source_displacement_related.destination
                    
    
                                
    def __unicode__(self):
        return u"%s --(%d)--> %s" % (unicode(self.start),self.displacement_set.all().count(),unicode(self.finish))

    class Meta:
        ordering = ['-id']
    
class Crew(models.Model):
    names = models.CharField(max_length=255, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    picture = models.ImageField(null=True,blank=True,upload_to="uploads")
    
    def __unicode__(self):
        return self.names
        
class Displacement(models.Model):
    source = models.OneToOneField("Coordinate",related_name="source_displacement_related")
    destination = models.OneToOneField("Coordinate",related_name="destination_displacement_related")    
    #crew = models.OneToOneField("Crew",null=True,blank=True)
    
    hitchgraph = models.ForeignKey("HitchGraph",null=True,blank=True)
    
    objects = OrderedBySourceManager()
    
    def length(self):
        return self.source.location.distance(self.destination.location)
        
    def __unicode__(self):
        return u"%s ----> %s" % (unicode(self.source),unicode(self.destination))
    
    def get_admin_url(self):
        return "/admin/expeditions/displacement/%d/" % self.id


class DisplacementDescription(models.Model):
    #__metaclass__ = TransMeta
    
    #class Meta:
    #    translate = ('comments', )
    CAR = 'CAR'
    BOAT = 'BOAT'
    PLANE = 'PLANE'
    BUS = 'BUS'
    TRAIN = 'TRAIN'
    FOOT = 'FOOT'
    BIKE = 'BIKE'
    RAFT = 'RAFT'
    OTHER = 'OTHER'
    UNKNOWN = 'UNKNOWN'
    
    TRANSPORT_CHOICES = (
        (CAR, 'Car'),
        (BOAT, 'Boat'),
        (PLANE, 'Plane'),
        (BUS, 'Bus'),
        (TRAIN, 'Train'),
        (FOOT, 'On foot'),
        (BIKE, 'Bike'),
        (RAFT, 'Canoe/Kayak/Raft'),
        (OTHER, 'Other'),
        (UNKNOWN, '?')
    )
    displacement = models.ForeignKey("Displacement",null=True,blank=True)    
    
    comments = models.TextField(null=True,blank=True)
    cheating = models.BooleanField(default=False)    
        
    transport = models.CharField(max_length=255,
                                      choices=TRANSPORT_CHOICES,
                                      default=CAR)


