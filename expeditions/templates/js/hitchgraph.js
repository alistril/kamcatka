////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// Inital data /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

var source,destination;

$(document).ready(function(){
		
	{% for graph in graphs %}
		hitchgraph = new HitchGraph({
			id: {{graph.id}},
			coordinate_set : new CoordinateCollection(),
			displacement_set : new DisplacementCollection()
		});
		
		{% if user_only %}		
			{% for displacement in graph.displacement_set_filtered_by_user.all %}		
				source = {% include "js/hitchgraph_new_coordinate.js" with coordinate=displacement.source %};
				destination = {% include "js/hitchgraph_new_coordinate.js" with coordinate=displacement.destination %};
				
				hitchgraph.get("coordinate_set").add(source);
				hitchgraph.get("coordinate_set").add(destination);
				hitchgraph.get("displacement_set").add( 
					new Displacement({
						"source" : hitchgraph.get("coordinate_set").get({{ displacement.source.id }}),
						"destination" : hitchgraph.get("coordinate_set").get({{ displacement.destination.id }})
					})
				);
			{% endfor %}
			
			var map_view = new MapView({ el: $("#map_canvas{{graph.id}}"), model : hitchgraph });
			map_view.render();
		{% else %}
			{% for displacement in graph.displacement_set.all %}		
				source = {% include "js/hitchgraph_new_coordinate.js" with coordinate=displacement.source %};
				destination = {% include "js/hitchgraph_new_coordinate.js" with coordinate=displacement.destination %};
				
				hitchgraph.get("coordinate_set").add(source);
				hitchgraph.get("coordinate_set").add(destination);
				hitchgraph.get("displacement_set").add( 
					new Displacement({
						"source" : hitchgraph.get("coordinate_set").get({{ displacement.source.id }}),
						"destination" : hitchgraph.get("coordinate_set").get({{ displacement.destination.id }})
					})
				);
			{% endfor %}
			
			var map_view = new MapView({ el: $("#map_canvas{{graph.id}}"), model : hitchgraph });
			map_view.render();
		{% endif %}
	{% endfor %}
   
});
