{% extends "base.js" %}

{% block path_to_templates %}
'hitchgraph/templates'
{% endblock %}

{% block paths_to_behaviours %}
{{ block.super }},
'behaviours/expeditions','jquery'
{% endblock %}

{% block behaviours_objects %}
{{ block.super }},
Behaviour,$
{% endblock %}

{% block behaviour_arguments %}
{
	hitchgraph_res 	: 	{
		url : "{% url hitchgraph-list %}",
		data : {
			user : "{{user}}",
			expedition : "{{expedition.id}}"
		}
	}
}
{% endblock %}