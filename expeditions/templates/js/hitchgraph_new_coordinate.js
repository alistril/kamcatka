new Coordinate({
			id : {{ coordinate.id }}, 
			name : {% if coordinate.name %} "{{ coordinate.name|escapejs}}" {%else%} undefined {% endif %},
			address : "{{ coordinate.address|escapejs }}",
			location: new google.maps.LatLng ({{ coordinate.location.0 }},{{ coordinate.location.1 }}),
			datetime : moment("{{ coordinate.datetime|date:"c" }}"),
			coordinatedescription_set : new CoordinateDescriptionCollection([
			                {% for description in coordinate.coordinatedescription_set.all %}
			                new CoordinateDescription({
			                	{% if description.user %}
			                	userprofile : new UserProfile({
			                		username : "{{ description.user }}",
			                		date_met : moment("{{ description.user.get_profile.when|date:"c" }}")
			                	}),
			                	{% endif %}
			                	picture_url : "{{ description.picture }}"
			                	{% if description.comments %}
			                	,
			                	comments : "{{ description.comments|linebreaks|escapejs }}"
			                	{% endif %}
			                }),
			                {% endfor %}
			                ])
		})
