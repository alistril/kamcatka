from django.contrib import admin
from django.db import models
from mezzanine.pages.admin import PageAdmin
from .models import Displacement,HitchGraph,Expedition,Coordinate,Crew,CoordinateDescription,DisplacementDescription
from django import forms

from django.forms.widgets import Select
from django.contrib.admin.widgets import RelatedFieldWidgetWrapper

from django.db.models.fields.related import OneToOneRel
from django.template.loader import render_to_string
from django.conf import settings
from django.conf.urls import patterns,url
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from mezzanine.utils.urls import admin_url
from beacon.utils import get_gmail_coordinates
from django.contrib.admin.actions import delete_selected
from django.contrib import messages
from django.db import transaction
from django.forms import Textarea
from transmeta import canonical_fieldname
from django.forms.models import BaseInlineFormSet

class CoordinateAdmin(admin.ModelAdmin):
    list_display = ('address','name','datetime','datetime_precision','registered_with','representation')
    search_fields = ['address','name', '=coordinatedescription__user__username']
            
    def registered_with(self,object):
        return unicode(",".join([(description.user.username or "Unknown user") for description in object.coordinatedescription_set.exclude(user=None) ])) or u"no one"        
    
    def representation(self,object):
        return render_to_string("expeditions/coordinate.html",{"coordinate":object})
    representation.allow_tags = True
    
    class Media:
        js = ("/utils/basic_context.js", settings.STATIC_URL+'js/libs/require/require.js', settings.STATIC_URL+"js/main/user_profiles.js")

class DisplacementAdminForm(forms.ModelForm):
    model = Displacement
    
    def __init__(self, *args, **kwargs):        
        self.parent_object = kwargs.pop('parent_object', None)
        super(DisplacementAdminForm, self).__init__(*args, **kwargs)
        
        if self.instance.pk:
            self.fields['source'].queryset      = self.instance.hitchgraph.coordinates_qs()
            self.fields['destination'].queryset = self.instance.hitchgraph.coordinates_qs()
        elif self.parent_object:
            self.fields['source'].queryset      = self.parent_object.coordinates_qs()
            self.fields['destination'].queryset = self.parent_object.coordinates_qs()


class DisplacementAdminFormSet(BaseInlineFormSet):
    def _construct_form(self, i, **kwargs):
        kwargs['parent_object'] = self.instance
        return super(DisplacementAdminFormSet, self)._construct_form(i, **kwargs)
    
class DisplacementInline(admin.TabularInline):
    form = DisplacementAdminForm
    formset = DisplacementAdminFormSet
    model = Displacement
    extra = 1    
    
    def formfield_for_dbfield(self, db_field, **kwargs):
        
        field = super(DisplacementInline, self).formfield_for_dbfield(db_field, **kwargs)
        db_fieldname = canonical_fieldname(db_field)
        
        if db_fieldname!=db_field.name:
            field.help_text = field.label
            field.label = db_fieldname
            
        return field    
        
class DisplacementAdmin(admin.ModelAdmin):
    form = DisplacementAdminForm
    list_display = ('source','destination','graph',)
    
    def graph(self,object):
        return render_to_string("expeditions/displacement.html",{"displacement":object,"display_source":True})
    graph.allow_tags = True

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(DisplacementAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        db_fieldname = canonical_fieldname(db_field)
        
        if db_fieldname!=db_field.name:
            field.help_text = field.label
            field.label = db_fieldname
            
        return field
            
    class Media:
        css = {"all" : (settings.STATIC_URL+"css/hitchgraph.css",settings.STATIC_URL+"js/qTip2/dist/basic/jquery.qtip.css",)}
        js = ('http://code.jquery.com/jquery-1.8.2.min.js',settings.STATIC_URL+"js/qTip2/dist/basic/jquery.qtip.js",settings.STATIC_URL+"js/hitchgraph.js",)

class HitchGraphAdminForm(forms.ModelForm):
    model = HitchGraph
    
    def __init__(self, *args, **kwargs):
        super(HitchGraphAdminForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['start'].queryset = self.instance.coordinates_qs()
            self.fields['finish'].queryset = self.instance.coordinates_qs()
    
    
class HitchGraphAdmin(admin.ModelAdmin):
    form = HitchGraphAdminForm
    list_display = ('start', 'finish', 'graph', 'number_of_hitches')
    change_list_template = "admin/hitchgraph_change_list.html"
    inlines = [DisplacementInline]
    actions = ['delete_hitchgraph_and_dependencies','concatenate_hitchgraphs']
    
    def concatenate_hitchgraphs_helper(self, hg1, hg2):                
        c1 = Coordinate.objects.create(
                               name=hg1.finish.name,
                               datetime=hg1.finish.datetime,
                               datetime_precision=hg1.finish.datetime_precision,
                               address=hg1.finish.address,                               
                               location=hg1.finish.location,
                               )
        c2 = Coordinate.objects.create(
                               name=hg2.start.name,
                               datetime=hg2.start.datetime,
                               datetime_precision=hg2.start.datetime_precision,
                               address=hg2.start.address,                               
                               location=hg2.start.location,  
                               )
        last_displacement_hg1 = Displacement.objects.get(destination=hg1.finish)
        first_displacement_hg2 = Displacement.objects.get(source=hg2.start)
        last_displacement_hg1.destination = c1
        first_displacement_hg2.source = c2
        last_displacement_hg1.save()
        first_displacement_hg2.save()
        Displacement.objects.create(source=c1,destination=c2,hitchgraph=hg1)
        Displacement.objects.filter(hitchgraph=hg2).update(hitchgraph=hg1)
        
        old_hg1_finish = hg1.finish
        hg1.finish=hg2.finish
        hg1.save()
        
        
        old_hg1_finish.delete()
        hg2.start.delete()
        hg2.delete()
        return hg1
        
    @transaction.commit_on_success
    def concatenate_hitchgraphs(self, request, queryset):
        if queryset.count()<2:
            messages.error(request, "you have to select at least 2 items")
        else:
            qs = queryset.all().order_by('start')
            hg1 = qs[0]
            for hg in qs[1:]:                
                hg2 = hg
                if hg1.finish.datetime <= hg2.start.datetime:
                    hg1 = self.concatenate_hitchgraphs_helper(hg1,hg2)
                else:
                    messages.error(request, "hitchgraphs %s starts at %s and finishes at %s. Hitchgraph %s starts at %s and finishes at %s. This is incompatible." % 
                                   (hg1,hg1.start.datetime,hg1.finish.datetime,hg2,hg2.start.datetime,hg2.finish.datetime)
                                   )
                    transaction.rollback() 
                    return
                
            
        self.message_user(request, "Hitchgraphs concatenated.")
        
    def delete_hitchgraph_and_dependencies(self, request, queryset):
        from django.db.models import Q        
        queryset = Coordinate.objects.filter(
                                          Q(source_displacement_related__hitchgraph__id__in=queryset.values_list('id')) |
                                          Q(destination_displacement_related__hitchgraph__id__in=queryset.values_list('id')) 
                                          )

        res = delete_selected(self,request,queryset)
        res.template_name = "admin/delete_related_addresses_confirmation.html"        
        return res
    delete_hitchgraph_and_dependencies.short_description = "delete addresses and other dependencies using these"

    def graph(self,object):
        return render_to_string("expeditions/hitchgraph.html",{"graph":object, "is_admin":True})
    graph.allow_tags = True
    
    def number_of_hitches(self,object):
        return object.displacement_set.all().count()
    
    class Media:
        css = {"all" : (settings.STATIC_URL+"css/hitchgraph.css",settings.STATIC_URL+"css/hitchgraph_admin.css",)}
        js = ('http://code.jquery.com/jquery-1.8.2.min.js',settings.STATIC_URL+"js/hitchgraph.js",)
    
    def get_urls(self):
        urls = super(HitchGraphAdmin, self).get_urls()
        my_urls = patterns('',
            url(r'^import_from_email/$', self.import_from_email, name="expeditions_hitchgraph_import_from_email")
        )
        return my_urls + urls
    
    class EmailCredentialsForm(forms.Form):
        login = forms.CharField()
        password = forms.CharField( widget=forms.PasswordInput )

        

    def import_from_email(self, request):
        if request.method == "POST":
            form = self.EmailCredentialsForm(request.POST)

            if form.is_valid():
                login = form.cleaned_data["login"]
                password = form.cleaned_data["password"]
                
                coords = get_gmail_coordinates(login,password)
                if not len(coords):
                    messages.error(request, "nothing to import")
                    return redirect(admin_url(self.model,"changelist"))
                firstcoord = coords[0]
                lastcoord = coords[-1]
                firstcoord.save()
                lastcoord.save()
                hg = HitchGraph.objects.create(start=firstcoord,finish=lastcoord)
                for coord in coords[1:]:
                    coord.save()                  
                    Displacement.objects.create(source=firstcoord,destination=coord,hitchgraph=hg)
                    firstcoord = coord  
                                
                count = len(coords)
                self.message_user(request, "Successfully imported %d %s(s)." % (count,"object"))                

                return redirect(admin_url(self.model,"changelist"))            
            else:
                return render_to_response("admin/import_from_email.html",{"email_credentials_form" : form}, RequestContext(request))
        else:
            form = self.EmailCredentialsForm()
            return render_to_response("admin/import_from_email.html",{"email_credentials_form" : form}, RequestContext(request))
                

class CrewAdmin(admin.ModelAdmin):
    list_display = ('names','description','image',)
    
    def image(self,obj):
        if obj.picture:
            return "<img src='%s' width='100px'/>" % obj.picture.url
        else:
            return "[Not set]"
    image.allow_tags = True

"""
class ExpeditionAdmin(admin.ModelAdmin):
    formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }

    class Media:
        js = ('ckeditor/ckeditor.js',) 
"""
class CoordinateDescriptionAdmin(admin.ModelAdmin):
    list_display = ('coordinate','user','descriptipn','image')
    search_fields = ['coordinate__address','coordinate__name', 'comments', '=user__username']
    
    def descriptipn(self,obj):
        if obj.comments:
            if len(obj.comments) > 500:
                return obj.comments[:500] + "..."
            else:
                return obj.comments
        else:
            return "no description"
        
    def image(self,obj):
        if obj.picture:
            return "<img src='%s' width='100px'/>" % obj.picture.url
        else:
            return "[Not set]"
    image.allow_tags = True
    
admin.site.register(Coordinate,CoordinateAdmin)
admin.site.register(CoordinateDescription,CoordinateDescriptionAdmin)

admin.site.register(Crew,CrewAdmin)
admin.site.register(Displacement,DisplacementAdmin)
admin.site.register(DisplacementDescription)
admin.site.register(HitchGraph,HitchGraphAdmin)
admin.site.register(Expedition, PageAdmin)
