from  django.test import TestCase
from .models import HitchGraph,Coordinate,Displacement
from django.contrib.gis.geos import Point
from django.contrib.auth.models import User
import datetime
import pytz
import simplejson

class MoveExtremeTestCase(TestCase):
    def setUp(self):
        self.lasse = User.objects.create(username="lasse")
        self.prague = Coordinate.objects.create(name="Prague",location=Point(0,0),datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC))
        self.prague.coordinatedescription_set.create(user=self.lasse)
        
        self.brno = Coordinate.objects.create(name="Brno",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.brno.coordinatedescription_set.create(user=self.lasse)        
        
        self.h = HitchGraph.objects.create(start=self.prague,finish=self.brno,name="hitchgraph")
        self.h.displacement_set.create(source=self.prague,destination=self.brno)
    
    def assert_invariants(self):
        self.h = HitchGraph.objects.get()
        self.assertEqual(Coordinate.objects.count(), 2)
        self.assertEqual(Displacement.objects.count(), 1)

    def assert_no_change(self):        
        self.assert_invariants()
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.brno)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
    
    def test_start_before(self):
        self.prague.datetime=datetime.datetime(2012, 1, 1,0,0,0,0,pytz.UTC)
        self.prague.save()
        
        self.assert_no_change()
        
    def test_start_after_no_reposition(self):
        self.prague.datetime=datetime.datetime(2013, 1, 1,1,0,0,0,pytz.UTC)
        self.prague.save()
        
        self.assert_no_change()
        
    def test_start_after_reposition_by_date(self):
        self.prague.datetime=datetime.datetime(2013, 1, 3,0,0,0,0,pytz.UTC)
        self.prague.save()
        
        self.assert_invariants()        
        self.assertEqual(self.h.start, self.brno)
        self.assertEqual(self.h.finish, self.prague)        
        self.h.displacement_set.get(source=self.brno,destination=self.prague)
        
    def test_finish_after(self):
        self.brno.datetime=datetime.datetime(2013, 2, 1,0,0,0,0,pytz.UTC)
        self.brno.save()
        
        self.assert_no_change()
        
    def test_finish_before_no_reposition(self):
        self.brno.datetime=datetime.datetime(2013, 1, 1,1,0,0,0,pytz.UTC)
        self.brno.save()
        
        self.assert_no_change()
        
    def test_finish_before_no_reposition_equality(self):
        self.brno.datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC)
        self.brno.save()
        
        self.assert_no_change()
        
    def test_finish_before_reposition(self):
        self.brno.datetime=datetime.datetime(2012, 1, 1,0,0,0,0,pytz.UTC)
        self.brno.save()
        
        self.assert_invariants()        
        self.assertEqual(self.h.start, self.brno)
        self.assertEqual(self.h.finish, self.prague)        
        self.h.displacement_set.get(source=self.brno,destination=self.prague)
            
class MoveTestCase(TestCase):
    def setUp(self):
        self.lasse = User.objects.create(username="lasse")
        self.bosse = User.objects.create(username="bosse")
        self.ole = User.objects.create(username="ole")
        self.prague = Coordinate.objects.create(name="Prague",location=Point(0,0),datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC))
        self.prague.coordinatedescription_set.create(user=self.lasse)
        
        self.brno = Coordinate.objects.create(name="Brno",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.brno.coordinatedescription_set.create(user=self.lasse)
        
        self.wien = Coordinate.objects.create(name="Wien",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.wien.coordinatedescription_set.create(user=self.lasse)
        self.wien.coordinatedescription_set.create(user=self.bosse)
        
        self.maribor = Coordinate.objects.create(name="Maribor",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.maribor.coordinatedescription_set.create(user=self.bosse)
        
        self.zagreb = Coordinate.objects.create(name="Zagreb",location=Point(0,0),datetime=datetime.datetime(2013, 1, 3,0,0,0,0,pytz.UTC))        
        
        self.sarajevo = Coordinate.objects.create(name="Sarejevo",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.sarajevo.coordinatedescription_set.create(user=self.ole)
        
        self.beograd = Coordinate.objects.create(name="Beograd",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.beograd.coordinatedescription_set.create(user=self.ole)
        
        self.h = HitchGraph.objects.create(start=self.prague,finish=self.beograd,name="hitchgraph")
        self.h.displacement_set.create(source=self.prague,destination=self.brno)
        self.h.displacement_set.create(source=self.brno,destination=self.wien)
        self.h.displacement_set.create(source=self.wien,destination=self.maribor)
        self.h.displacement_set.create(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.create(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.create(source=self.sarajevo,destination=self.beograd)    
    
    def assert_invariants(self):
        self.h = HitchGraph.objects.get()
        self.assertEqual(Coordinate.objects.count(), 7)
        self.assertEqual(Displacement.objects.count(), 6)

    def assert_no_change(self):        
        self.assert_invariants()
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.beograd)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
    
    def test_start_before(self):
        self.prague.datetime=datetime.datetime(2012, 1, 1,0,0,0,0,pytz.UTC)
        self.prague.save()
        
        self.assert_no_change()
        
    def test_start_after_no_reposition(self):
        self.prague.datetime=datetime.datetime(2013, 1, 1,1,0,0,0,pytz.UTC)
        self.prague.save()
        
        self.assert_no_change()
        
    def test_start_after_reposition_by_date(self):
        self.prague.datetime=datetime.datetime(2013, 1, 2,1,0,0,0,pytz.UTC)
        self.prague.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.brno)
        self.assertEqual(self.h.finish, self.beograd)
        
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.prague)
        self.h.displacement_set.get(source=self.prague,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        
    def test_start_after_reposition_by_date_at_end(self):
        self.prague.datetime=datetime.datetime(2013, 1, 4,1,0,0,0,pytz.UTC)
        self.prague.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.brno)
        self.assertEqual(self.h.finish, self.prague)
        
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)        
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        self.h.displacement_set.get(source=self.beograd,destination=self.prague)
    
    def test_start_after_reposition_by_graph(self):
        #just a little modification of brno for this test to work
        self.brno.datetime = datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC)
        self.brno.save()
        self.prague.datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC)
        self.prague.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.brno)
        self.assertEqual(self.h.finish, self.beograd)

        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.prague)
        self.h.displacement_set.get(source=self.prague,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)        
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        
    def test_start_after_reposition_by_graph_at_end(self):
        self.prague.datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC)
        self.prague.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.brno)
        self.assertEqual(self.h.finish, self.prague)
        
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)        
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        self.h.displacement_set.get(source=self.beograd,destination=self.prague)
    
    def test_finish_after(self):
        self.beograd.datetime=datetime.datetime(2013, 2, 1,0,0,0,0,pytz.UTC)
        self.beograd.save()
        
        self.assert_no_change()
        
    def test_finish_before_no_reposition(self):
        self.sarajevo.datetime=datetime.datetime(2013, 1, 3,9,0,0,0,pytz.UTC)
        self.sarajevo.save()
        self.beograd.datetime=datetime.datetime(2013, 1, 3,10,0,0,0,pytz.UTC)
        self.beograd.save()
        
        self.assert_no_change()
        
    def test_finish_before_reposition_by_date(self):
        self.beograd.datetime=datetime.datetime(2013, 1, 3,1,0,0,0,pytz.UTC)
        self.beograd.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.sarajevo)
        
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.beograd)
        self.h.displacement_set.get(source=self.beograd,destination=self.sarajevo)

        
    def test_finish_before_reposition_by_date_at_beginning(self):
        self.beograd.datetime=datetime.datetime(2012, 1, 1,0,0,0,0,pytz.UTC)
        self.beograd.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.beograd)
        self.assertEqual(self.h.finish, self.sarajevo)
        
        self.h.displacement_set.get(source=self.beograd,destination=self.prague)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)        
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        
    
    def test_finish_before_reposition_by_graph(self):
        self.beograd.datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC)
        self.beograd.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.sarajevo)
        
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.beograd)
        self.h.displacement_set.get(source=self.beograd,destination=self.zagreb)        
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        
    def test_finish_before_reposition_by_graph_at_beginning(self):
        self.beograd.datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC)
        self.beograd.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.sarajevo)
        
        self.h.displacement_set.get(source=self.prague,destination=self.beograd)
        self.h.displacement_set.get(source=self.beograd,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)        
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)

    def test_anon_move_after_start(self):
        self.zagreb.datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC)
        self.zagreb.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.beograd)
        
        self.h.displacement_set.get(source=self.prague,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)

    def test_anon_move_before_start(self):
        self.zagreb.datetime=datetime.datetime(2012, 1, 1,0,0,0,0,pytz.UTC)
        self.zagreb.save()
        self.assert_invariants()
        
        self.assertEqual(self.h.start, self.zagreb)
        self.assertEqual(self.h.finish, self.beograd)
        
        self.h.displacement_set.get(source=self.zagreb,destination=self.prague)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
       
    def test_anon_move_after_finish(self):
        self.zagreb.datetime=datetime.datetime(2013, 1, 4,1,0,0,0,pytz.UTC)
        self.zagreb.save()
        self.assert_invariants()

        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.zagreb)        
        
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        self.h.displacement_set.get(source=self.beograd,destination=self.zagreb)
    
    def test_anon_dontmove1(self):
        self.zagreb.datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC)
        self.zagreb.save()
        self.assert_no_change()
        
    def test_anon_dontmove2(self):
        self.zagreb.datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC)
        self.zagreb.save()
        self.assert_no_change()
        
    def test_registered_move_after_finish(self):
        self.brno.datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC)
        self.brno.save()
        
        self.assert_invariants()

        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.brno)        

        self.h.displacement_set.get(source=self.prague,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        self.h.displacement_set.get(source=self.beograd,destination=self.brno)
        
        
    def test_registered_move_after_stay_on_subgraph(self):
        self.wien.datetime=datetime.datetime(2013, 1, 3,0,0,0,0,pytz.UTC)
        self.wien.save()
        
        self.assert_invariants()

        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.beograd)        
        
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
         
class DeleteTestCase(TestCase):
    def setUp(self):
        self.prague = Coordinate.objects.create(name="Prague",location=Point(0,0),datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC))
        self.munchen = Coordinate.objects.create(name="Munchen",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.geneve = Coordinate.objects.create(name="Geneve",location=Point(0,0),datetime=datetime.datetime(2013, 1, 3,0,0,0,0,pytz.UTC))
        self.paris = Coordinate.objects.create(name="Paris",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.h = HitchGraph.objects.create(start=self.prague,finish=self.paris,name="hitchgraph")
        self.h.displacement_set.create(source=self.prague,destination=self.munchen)
        self.h.displacement_set.create(source=self.munchen,destination=self.geneve)
        self.h.displacement_set.create(source=self.geneve,destination=self.paris)
        

    def test_middle(self):
        munchen = Coordinate.objects.get(name="Munchen")
        munchen.delete()        
        with self.assertRaises(Displacement.DoesNotExist):
            self.h.displacement_set.get(source=munchen)
        with self.assertRaises(Displacement.DoesNotExist):
            self.h.displacement_set.get(destination=munchen)
        self.h.displacement_set.get(source=self.prague, destination=self.geneve)
        self.assertEqual(self.h.displacement_set.count(), 2)
        self.assertEqual(Coordinate.objects.count(), 3)

    def test_finish(self):
        self.paris.delete()
        self.h = HitchGraph.objects.get()
        with self.assertRaises(Displacement.DoesNotExist):
            self.h.displacement_set.get(destination=self.paris)        

        self.h.displacement_set.get(source=self.prague, destination=self.munchen)
        self.h.displacement_set.get(source=self.munchen, destination=self.geneve)        
        
        self.assertEqual(self.h.finish, self.geneve)
        self.assertEqual(self.h.displacement_set.count(), 2)
        self.assertEqual(Coordinate.objects.count(), 3)

        
    def test_start(self):        
        self.prague.delete()
        self.h = HitchGraph.objects.get()
        with self.assertRaises(Displacement.DoesNotExist):
            self.h.displacement_set.get(source=self.prague)        

               
        self.h.displacement_set.get(source=self.munchen, destination=self.geneve)
        self.h.displacement_set.get(source=self.geneve, destination=self.paris)
        self.assertEqual(self.h.start, self.munchen)
        self.assertEqual(self.h.displacement_set.count(), 2)
        self.assertEqual(Coordinate.objects.count(), 3)
        
    def test_delete_all_except_extemities(self):
        self.munchen.delete()
        self.geneve.delete()
        
        self.h = HitchGraph.objects.get()
        
        displacement = Displacement.objects.get()
        self.assertEqual(displacement.hitchgraph, self.h)
        
        self.assertEqual(Coordinate.objects.count(), 2)
        
    def test_delete_all_except_start(self):
        self.test_delete_all_except_extemities()
        
        self.h.finish.delete()
        self.h = HitchGraph.objects.get()
        with self.assertRaises(Displacement.DoesNotExist):
            Displacement.objects.get()
            
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.prague)
        self.assertEqual(Coordinate.objects.count(), 1)
    
    def test_delete_all_except_finish(self):
        self.test_delete_all_except_extemities()
        
        self.h.start.delete()
        self.h = HitchGraph.objects.get()
        with self.assertRaises(Displacement.DoesNotExist):
            Displacement.objects.get()
        
        self.assertEqual(self.h.start, self.paris)
        self.assertEqual(self.h.finish, self.paris)
        self.assertEqual(Coordinate.objects.count(), 1)

    def test_delete_all(self):
        self.test_delete_all_except_finish()
        self.h.finish.delete()
        with self.assertRaises(HitchGraph.DoesNotExist):
            HitchGraph.objects.get()
        with self.assertRaises(Displacement.DoesNotExist):
            Displacement.objects.get()
        with self.assertRaises(Coordinate.DoesNotExist):
            Coordinate.objects.get()
        
    def test_bulk_delete(self):        
        Coordinate.objects.filter(pk__in=[2,3]).delete()
        Displacement.objects.get()
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.paris)
        self.assertEqual(Coordinate.objects.count(), 2)        
        self.assertEqual(Displacement.objects.count(), 1)
        
    def test_bulk_delete_all(self):        
        Coordinate.objects.all().delete()
        with self.assertRaises(HitchGraph.DoesNotExist):
            HitchGraph.objects.get()
        with self.assertRaises(Displacement.DoesNotExist):
            Displacement.objects.get()
        with self.assertRaises(Coordinate.DoesNotExist):
            Coordinate.objects.get()

class AddCoordinateTestCase(TestCase):
    fixtures = ['users.json','test_your_way.json']

    def setUp(self):
        """
        self.lisa = User.objects.create_user('lisa', 'lisa@lisa.com', 'lisalisa')
        
        self.prague = Coordinate.objects.create(name="Prague",location=Point(0,0),datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC))
        self.brno = Coordinate.objects.create(name="Brno",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.wien = Coordinate.objects.create(name="Wien",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        #self.wien.coordinatedescription_set.create(user=self.lisa)
        
        self.maribor = Coordinate.objects.create(name="Maribor",location=Point(0,0),datetime=datetime.datetime(2013, 1, 3,0,0,0,0,pytz.UTC))
        self.zagreb = Coordinate.objects.create(name="Zagreb",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))        
        
        self.sarajevo = Coordinate.objects.create(name="Sarejevo",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.beograd = Coordinate.objects.create(name="Beograd",location=Point(0,0),datetime=datetime.datetime(2013, 1, 5,0,0,0,0,pytz.UTC))

        self.h = HitchGraph.objects.create(start=self.prague,finish=self.beograd,name="The actual way")
        self.h.displacement_set.create(source=self.prague,destination=self.brno)
        self.h.displacement_set.create(source=self.brno,destination=self.wien)
        self.h.displacement_set.create(source=self.wien,destination=self.maribor)
        self.h.displacement_set.create(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.create(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.create(source=self.sarajevo,destination=self.beograd)
        """
    
    def assert_invariants(self):
        self.h = HitchGraph.objects.get(name="The actual way")
        
        self.assertEqual(self.h.coordinates_qs().count(), 9)
        self.assertEqual(self.h.displacement_set.count(), 8)

    def assert_no_change(self):        
        self.assert_invariants()
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.brno)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)

    def test_insert_middle(self):
        user = self.client.login(username='adria',password='adriaadria')
        
        response = self.client.get('/your-way/')
        
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/your-way/', {                                              
                                              'name'        : 'pirot', 
                                              'address'     : 'pirot',
                                              'datetime'    : '2013-03-16 04:00:00',
                                              'location'    : '47.079007,15.439796'
                                              })
        
        self.assertNotEqual(response.status_code, 500)        
        self.assert_invariants()
        
        self.pirot = self.h.coordinates_qs().get(address="pirot",name="pirot")
        self.assertEqual(self.pirot.coordinatedescription_set.count(),1)
        self.assertEqual(self.pirot.coordinatedescription_set.get().user.username,"adria")
        
        self.assertEqual(self.h.start.address, "Prague")
        self.assertEqual(self.h.finish.address, "Sofia")
        
        self.assertEqual(self.pirot.source_displacement_related.destination.name, "Nis")
        self.assertEqual(self.pirot.destination_displacement_related.source.name, "Belgrade")
        
class CoordinateDescriptionTestCase(TestCase):
    def setUp(self):
        self.lisa = User.objects.create_user('lisa', 'lisa@lisa.com', 'lisalisa')
        self.lasse = User.objects.create_user('lasse', 'lasse@lasse.com', 'lasse')
        
        self.prague = Coordinate.objects.create(name="Prague",location=Point(0,0),datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC))
        self.brno = Coordinate.objects.create(name="Brno",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.brno.coordinatedescription_set.create(user=self.lisa)
        
        self.wien = Coordinate.objects.create(name="Wien",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.wien.coordinatedescription_set.create(user=self.lasse)
        
        self.maribor = Coordinate.objects.create(name="Maribor",location=Point(0,0),datetime=datetime.datetime(2013, 1, 3,0,0,0,0,pytz.UTC))
        self.zagreb = Coordinate.objects.create(name="Zagreb",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))        
        
        self.sarajevo = Coordinate.objects.create(name="Sarejevo",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.beograd = Coordinate.objects.create(name="Beograd",location=Point(0,0),datetime=datetime.datetime(2013, 1, 5,0,0,0,0,pytz.UTC))

        self.h = HitchGraph.objects.create(start=self.prague,finish=self.beograd,name="The actual way")
        self.h.displacement_set.create(source=self.prague,destination=self.brno)
        self.h.displacement_set.create(source=self.brno,destination=self.wien)
        self.h.displacement_set.create(source=self.wien,destination=self.maribor)
        self.h.displacement_set.create(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.create(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.create(source=self.sarajevo,destination=self.beograd)
    
    def assert_invariants(self):
        self.h = HitchGraph.objects.get(name="The actual way")
        
        self.assertEqual(self.h.coordinates_qs().count(), 9)
        self.assertEqual(self.h.displacement_set.count(), 8)

    def assert_no_change(self):        
        self.assert_invariants()
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.brno)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)

    def test_modify_name(self):
        self.client.login(username='lisa',password='lisalisa')
        
        response = self.client.put('/expeditions/res/coordinates/%d/' % self.brno.id, 
                                   simplejson.dumps(
                                                    {
                                                     "id": 2, 
                                                     "name": "Brno1", 
                                                     "address": "Brno", 
                                                     "location": 
                                                     {
                                                      "type": "Point", 
                                                      "coordinates": [49.1951, 16.6068]
                                                      }, 
                                                     "datetime": "2013-03-06T06:00:00+01:00", 
                                                     }
                                    ),
                                    content_type='application/json'
                                   )
        
        self.assertNotEqual(response.status_code, 500)
        self.brno = Coordinate.objects.get(id=2)
        self.assertEqual(self.brno.name, "Brno1")       

    def test_modify_description(self):
        self.client.login(username='lisa',password='lisalisa')        
        
        response = self.client.put('/expeditions/res/coordinate_descriptions/%d/' % 1, 
                                   simplejson.dumps(
                                                    {
                                                        "comments": "hello",
                                                        "coordinate": self.brno.id,
                                                        "id": 1,
                                                        "userprofile": self.lisa.id
                                                        }
                                                    ),
                                                    content_type='application/json'
                                    )
        
        self.assertNotEqual(response.status_code, 500)
        self.brno = Coordinate.objects.get(id=self.brno.id)
        self.assertEqual(self.brno.coordinatedescription_set.count(), 1)
        self.assertEqual(self.brno.coordinatedescription_set.get().comments, "hello")

        def test_tryadd_description(self):
            self.client.login(username='lisa',password='lisalisa')        
            
            response = self.client.put('/expeditions/res/coordinate_descriptions/%d/' % 1, 
                                       simplejson.dumps(
                                                        {
                                                            "comments": "bye",
                                                            "coordinate": self.brno.id,
                                                            "id": 2,
                                                            "userprofile": self.lisa.id
                                                            }
                                                        ),
                                                        content_type='application/json'
                                        )
            
            self.assertNotEqual(response.status_code, 500)
            self.brno = Coordinate.objects.get(id=self.brno.id)
            self.assertEqual(self.brno.coordinatedescription_set.count(), 1)
            self.assertEqual(self.brno.coordinatedescription_set.get().comments, "bye")
        
    def test_modify_description_nologin(self):
        response = self.client.put('/expeditions/res/coordinate_descriptions/%d/' % 1, 
                                   simplejson.dumps(
                                                    {
                                                        "comments": "hello",
                                                        "coordinate": self.brno.id,
                                                        "id": 1,
                                                        "userprofile": self.lisa.id
                                                        }
                                                    ),
                                                    content_type='application/json'
                                    )

        self.assertEqual(response.status_code, 403)
        self.brno = Coordinate.objects.get(id=self.brno.id)
        
        self.assertEqual(self.brno.coordinatedescription_set.count(), 1)
        self.assertIsNone(self.brno.coordinatedescription_set.get().comments)
        
    def test_modify_description_wronglogin(self):
        self.client.login(username='lasse',password='lasselasse')
        
        response = self.client.put('/expeditions/res/coordinate_descriptions/%d/' % 1, 
                                   simplejson.dumps(
                                                    {
                                                        "comments": "hello",
                                                        "coordinate": self.brno.id,
                                                        "id": 1,
                                                        "userprofile": self.lisa.id
                                                        }
                                                    ),
                                                    content_type='application/json'
                                    )

        self.assertEqual(response.status_code, 403)
        self.brno = Coordinate.objects.get(id=self.brno.id)
        self.assertEqual(self.brno.coordinatedescription_set.count(), 1)
        self.assertIsNone(self.brno.coordinatedescription_set.get().comments)

