from rest_framework import serializers
from .models import Displacement, HitchGraph, Coordinate, CoordinateDescription
from user_profiles.models import ProfileCard

import json
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.db.models import PointField
from rest_framework.fields import WritableField,CharField
from django.db.models import Q

class RelatedIdField(serializers.RelatedField):
    def to_native(self, value):
        return value.id
    
class GeometryField(WritableField):
    """
    A field to handle GeoDjango Geometry fields
    """
    type_name = 'GeometryField'

    def to_native(self, value):        
        if isinstance(value, dict) or value is None:
            return value
        
        # Get GeoDjango geojson serialization and then convert it _back_ to
        # a Python object
        return json.loads(value.geojson)
    
    def from_native(self, value):
        try:
            value = GEOSGeometry(json.dumps(value))
        except ValueError:
            pass
        return value
    
class PictureField(CharField):
    def to_native(self, value):
        if value:
            return value.url
        else:
            return None

class GeoModelSerializer(serializers.ModelSerializer):    
    def get_field(self, model_field):
        if isinstance(model_field, PointField):
            return GeometryField()
        else:
            return super(GeoModelSerializer,self).get_field(model_field)
        
class UserProfileSerializer(GeoModelSerializer):
    id = serializers.IntegerField(source='user.id')    
    username = serializers.CharField(source='user.username')
    date_met = serializers.DateField(source='when')
    place_met = serializers.CharField(source='where')
    
    class Meta:
        model = ProfileCard
        fields = ('id', 'date_met', 'place_met', 'location', 'username')
    

    
class CoordinateDescriptionSerializer(serializers.ModelSerializer):
    userprofile = serializers.IntegerField(source='user.id')
    picture = PictureField()
    picture_thumbnail = serializers.SerializerMethodField('get_picture_thumbnail')
    picture_thumbnail_mini = serializers.SerializerMethodField('get_picture_thumbnail_mini')
    picture_reasonable = serializers.SerializerMethodField('get_picture_reasonable')
    
        
    def get_picture_thumbnail(self,obj):
        if obj.picture_thumbnail.source_file:
            return obj.picture_thumbnail.url
        else:
            return None
        
    def get_picture_reasonable(self,obj):
        if obj.picture_reasonable.source_file:
            return obj.picture_reasonable.url
        else:
            return None
        
    def get_picture_thumbnail_mini(self,obj):        
        if obj.picture_thumbnail_mini.source_file:
            return obj.picture_thumbnail_mini.url
        else:
            return None

    class Meta:
        model = CoordinateDescription
        fields = ('id', 'coordinate', 'userprofile', 'picture', 'comments', 'picture_thumbnail', 'picture_reasonable', 'picture_thumbnail_mini')
    
class CoordinateSerializer(GeoModelSerializer):
    coordinatedescription_set = CoordinateDescriptionSerializer(many=True,read_only = True)    
    
    class Meta:
        model = Coordinate
        fields = ('id', 'initiator', 'category', 'name', 'address', 'location', 'datetime', 'coordinatedescription_set','battery_state')

class CoordinateSimpleSerializer(GeoModelSerializer):
    owners = serializers.SlugRelatedField(many=True, read_only=True, slug_field='user', source="coordinatedescription_set")
    
    class Meta:
        model = Coordinate
        fields = ('id', 'initiator', 'category','name', 'address', 'location', 'datetime', 'owners','battery_state')


class DisplacementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Displacement
        fields = ('id', 'source', 'destination','hitchgraph')
        
class HitchGraphSerializer(serializers.ModelSerializer):
    displacement_set = DisplacementSerializer(many=True, read_only = True)
    coordinate_set = serializers.SerializerMethodField('get_coordinate_user_query_set')
    userprofile_set = serializers.SerializerMethodField('get_user_query_set')
    
    def get_coordinate_user_query_set(self,obj):
        return CoordinateSerializer(
                                    obj.coordinates_qs(), 
                                    many=True
                                    ).data
                                    
    def get_user_query_set(self,obj):
        return UserProfileSerializer(
                                    ProfileCard.objects.filter(
                                                                Q(user__coordinatedescription__coordinate__source_displacement_related__hitchgraph=obj) |
                                                                Q(user__coordinatedescription__coordinate__destination_displacement_related__hitchgraph=obj) |
                                                                Q(user__coordinatedescription__coordinate__start_coordinate_related=obj) |
                                                                Q(user__coordinatedescription__coordinate__finish_coordinate_related=obj)
                                                               ).distinct(), 
                                                               many=True
                                     ).data


    class Meta:
        model = HitchGraph
        fields = ('id','name', 'start', 'finish', 'displacement_set', 'coordinate_set', 'userprofile_set')

class HitchGraphUserSerializer(HitchGraphSerializer):
    displacement_set = serializers.SerializerMethodField('get_displacement_user_query_set')
      
    start_set = RelatedIdField(source='first_coordinates_for_user',read_only=True,many=True)
    finish_set = RelatedIdField(source='last_coordinates_for_user',read_only=True,many=True)
    
    start = serializers.SerializerMethodField('get_start_id')
    finish = serializers.SerializerMethodField('get_finish_id')
    
    coordinate_before_user_set = serializers.SerializerMethodField('get_coordinate_before_user_set')
    coordinate_after_user_set = serializers.SerializerMethodField('get_coordinate_after_user_set')
    displacement_before_user_set = serializers.SerializerMethodField('get_displacement_before_user_set')
    displacement_after_user_set = serializers.SerializerMethodField('get_displacement_after_user_set')
    
    def get_coordinate_before_user_set(self,obj):
        return CoordinateSimpleSerializer(obj.coordinate_before_user_qs(),many=True).data
    
    def get_coordinate_after_user_set(self,obj):
        return CoordinateSimpleSerializer(obj.coordinate_after_user_qs(),many=True).data
    
    def get_displacement_before_user_set(self,obj):
        return DisplacementSerializer(obj.displacement_before_user_qs(),many=True).data
    
    def get_displacement_after_user_set(self,obj):
        return DisplacementSerializer(obj.displacement_after_user_qs(),many=True).data
     
    def get_start_id(self,obj):        
        #select coord belonging to hitchgraph which is a destination of a displacement of which the source is NOT user-based
        #and which is still user-based
        #That coordinate will be the first user-based coordinate
        """
        first_coord = Coordinate.objects.filter(
                                            Q(source_displacement_related__hitchgraph=obj) | Q(destination_displacement_related__hitchgraph=obj),
                                            coordinatedescription__user=obj.filter_user
                                            ).distinct().exclude(
                                                    destination_displacement_related__source__coordinatedescription__user=obj.filter_user
                                                            );
        """
        return obj.first_coordinates_for_user(obj.filter_user)[0].id
    
    def get_finish_id(self,obj):
        #select coord belonging to hitchgraph which is a source of a displacement of which the destination is NOT user-based
        #and which is still user-based
        #That coordinate will be the alst user-based coordinate
        """
        last_coord = Coordinate.objects.filter(
                                            Q(source_displacement_related__hitchgraph=obj) | Q(destination_displacement_related__hitchgraph=obj),
                                            coordinatedescription__user=obj.filter_user
                                            ).distinct().exclude(
                                                    source_displacement_related__destination__coordinatedescription__user=obj.filter_user
                                                            );
        return last_coord[0].id
        """
        return obj.last_coordinates_for_user(obj.filter_user).latest().id
    
    def get_first_coordinates_query_set(self,obj):
        return CoordinateSerializer(
                                    obj.first_coordinates_for_user(obj.filter_user), 
                                    many=True
                                    ).data
    
    def get_last_coordinates_query_set(self,obj):
        return CoordinateSerializer(
                                    obj.last_coordinates_for_user(obj.filter_user), 
                                    many=True
                                    ).data
            
    def get_displacement_user_query_set(self,obj):
        return DisplacementSerializer(obj.displacement_set_filtered_by_user(), many=True).data
    
    def get_coordinate_user_query_set(self,obj):
        return CoordinateSerializer(
                                    obj.coordinates_qs().filter(coordinatedescription__user=obj.filter_user).distinct(), 
                                    many=True
                                    ).data
                                    
    def get_user_query_set(self,obj):
        return UserProfileSerializer(
                                    ProfileCard.objects.filter(
                                                                Q(user__coordinatedescription__coordinate__source_displacement_related__hitchgraph=obj) |
                                                                Q(user__coordinatedescription__coordinate__destination_displacement_related__hitchgraph=obj) |
                                                                Q(user__coordinatedescription__coordinate__start_coordinate_related=obj) |
                                                                Q(user__coordinatedescription__coordinate__finish_coordinate_related=obj),
                                                                user__coordinatedescription__coordinate__coordinatedescription__user=obj.filter_user                                                                
                                                               ).distinct(), 
                                                               many=True
                                     ).data
    
    class Meta:
        model = HitchGraph
        fields = ('id','name', 'start', 'finish', 'displacement_set', 'coordinate_set', 'userprofile_set', 
                  'start_set', 'finish_set', 'coordinate_before_user_set', 'coordinate_after_user_set',
                  'displacement_before_user_set', 'displacement_after_user_set')
        #fields = ('id','displacement_before_user_set', 'whatever')
        
