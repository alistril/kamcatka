from mezzanine.conf import register_setting

register_setting(
    name="ACTIVE_HITCHGRAPH",
    description="Hitchgraph to be selected by default in various operations.",
    editable=True,
    default=1,
)