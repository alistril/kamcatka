from django import forms
from django.http import HttpResponseRedirect
from mezzanine.pages.page_processors import processor_for
from expeditions.models import HitchGraph
from .forms import CoordinateForm,CoordinateDescriptionForm
from .models import Coordinate,HitchGraph
from django.db.models import Q

@processor_for("your-way")
def location_editor(request, page):
    hitchgraph = page.expedition.hitchgraph_set.get()    
    form = CoordinateForm(hitchgraph=hitchgraph)
    fromDescription = CoordinateDescriptionForm()
    hitchgraph.filter_by_user(request.user) #init the custom manager that'll be able to filter displacements on user
        
    if request.method == "POST":
        form = CoordinateForm(request.POST,hitchgraph=hitchgraph)
        
        if form.is_valid():            
            coord_with_user = form.save() #form has to be saved to reposition
            coord_with_user.reposition(hitchgraph,coord_with_user.datetime)
            
            fromDescription = CoordinateDescriptionForm(request.POST)
            if fromDescription.is_valid():
                description = fromDescription.save(commit=False)
                description.user = request.user
                description.coordinate = coord_with_user
                description.save()                
            else:
                #TODO: check if this is right
                coord_with_user.coordinatedescription_set.create(user=request.user)
            
            hitchgraph.mark_gps_for_user(request.user)
    return {"form": form,"form_description":fromDescription,"user_hitchgraphs":[hitchgraph]}