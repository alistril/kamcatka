from django.conf.urls import patterns, url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import HitchGraphList,UserProfileList,CoordinateDetail,CoordinateStartOfActiveDetail, \
                    CoordinateDescriptionDetail,HitchGraphDetail,save_file

import os

app_dir = os.path.dirname(__file__)

urlpatterns = patterns('expeditions.views',
    url('^res/$', 'api_root'),
    url('^res/hitchgraphs/$', HitchGraphList.as_view(), name='hitchgraph-list'),
    url('^res/hitchgraphs/(?P<pk>\d+)/$', HitchGraphDetail.as_view(), name='hitchgraph-detail'),
    url('^res/userprofiles/$', UserProfileList.as_view(), name='userprofile-list'),
    url('^res/coordinates/(?P<pk>\d+)/$', CoordinateDetail.as_view(), name='coordinate-detail'),
    url('^res/coordinates/start_of_active_hitchgraph/$', CoordinateStartOfActiveDetail.as_view(), name='coordinate-start-of-active-hitchgraph'),    
    url('^res/coordinates/finish_of_active_hitchgraph/$', CoordinateStartOfActiveDetail.as_view(), name='coordinate-finish-of-active-hitchgraph'),
    url('^res/coordinate_descriptions/(?P<pk>\d+)/$', CoordinateDescriptionDetail.as_view(), name='coordinatedescription-detail'),
    url('^res/coordinate_descriptions/upload_picture/$', save_file, name='coordinatedescription-upload-pic')
)
# Format suffixes
urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])
urlpatterns+= patterns('',
    #resources
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    
)
urlpatterns+= patterns('expeditions.views',
                       url('^hitchgraphs/(\d+)/map/$', 'hitchgraph_detail_map', name='hitchgraph-detail-map')
                       )