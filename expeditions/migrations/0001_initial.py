# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Coordinate'
        db.create_table(u'expeditions_coordinate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('location', self.gf('location_field.models.LocationField')()),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('datetime_precision', self.gf('timedelta.fields.TimedeltaField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'expeditions', ['Coordinate'])

        # Adding model 'CoordinateDescription'
        db.create_table(u'expeditions_coordinatedescription', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('coordinate', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['expeditions.Coordinate'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'expeditions', ['CoordinateDescription'])

        # Adding model 'Expedition'
        db.create_table(u'expeditions_expedition', (
            (u'page_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['pages.Page'], unique=True, primary_key=True)),
            ('destination_en', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('destination_fr', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('destination_ru', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('destination_zh', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('story_en', self.gf('mezzanine.core.fields.RichTextField')(null=True, blank=True)),
            ('story_fr', self.gf('mezzanine.core.fields.RichTextField')(null=True, blank=True)),
            ('story_ru', self.gf('mezzanine.core.fields.RichTextField')(null=True, blank=True)),
            ('story_zh', self.gf('mezzanine.core.fields.RichTextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'expeditions', ['Expedition'])

        # Adding model 'HitchGraph'
        db.create_table(u'expeditions_hitchgraph', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('start', self.gf('django.db.models.fields.related.ForeignKey')(related_name='start_coordinate_related', to=orm['expeditions.Coordinate'])),
            ('finish', self.gf('django.db.models.fields.related.ForeignKey')(related_name='finish_coordinate_related', to=orm['expeditions.Coordinate'])),
        ))
        db.send_create_signal(u'expeditions', ['HitchGraph'])

        # Adding M2M table for field expedition on 'HitchGraph'
        db.create_table(u'expeditions_hitchgraph_expedition', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('hitchgraph', models.ForeignKey(orm[u'expeditions.hitchgraph'], null=False)),
            ('expedition', models.ForeignKey(orm[u'expeditions.expedition'], null=False))
        ))
        db.create_unique(u'expeditions_hitchgraph_expedition', ['hitchgraph_id', 'expedition_id'])

        # Adding model 'Crew'
        db.create_table(u'expeditions_crew', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('names', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal(u'expeditions', ['Crew'])

        # Adding model 'Displacement'
        db.create_table(u'expeditions_displacement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('django.db.models.fields.related.OneToOneField')(related_name='source_displacement_related', unique=True, null=True, to=orm['expeditions.Coordinate'])),
            ('destination', self.gf('django.db.models.fields.related.OneToOneField')(related_name='destination_displacement_related', unique=True, null=True, to=orm['expeditions.Coordinate'])),
            ('hitchgraph', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['expeditions.HitchGraph'], null=True, blank=True)),
        ))
        db.send_create_signal(u'expeditions', ['Displacement'])

        # Adding model 'DisplacementDescription'
        db.create_table(u'expeditions_displacementdescription', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('displacement', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['expeditions.Displacement'], null=True, blank=True)),
            ('comments', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('cheating', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('transport', self.gf('django.db.models.fields.CharField')(default='CAR', max_length=255)),
        ))
        db.send_create_signal(u'expeditions', ['DisplacementDescription'])


    def backwards(self, orm):
        # Deleting model 'Coordinate'
        db.delete_table(u'expeditions_coordinate')

        # Deleting model 'CoordinateDescription'
        db.delete_table(u'expeditions_coordinatedescription')

        # Deleting model 'Expedition'
        db.delete_table(u'expeditions_expedition')

        # Deleting model 'HitchGraph'
        db.delete_table(u'expeditions_hitchgraph')

        # Removing M2M table for field expedition on 'HitchGraph'
        db.delete_table('expeditions_hitchgraph_expedition')

        # Deleting model 'Crew'
        db.delete_table(u'expeditions_crew')

        # Deleting model 'Displacement'
        db.delete_table(u'expeditions_displacement')

        # Deleting model 'DisplacementDescription'
        db.delete_table(u'expeditions_displacementdescription')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'expeditions.coordinate': {
            'Meta': {'ordering': "['datetime']", 'object_name': 'Coordinate'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'datetime': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'datetime_precision': ('timedelta.fields.TimedeltaField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('location_field.models.LocationField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'expeditions.coordinatedescription': {
            'Meta': {'object_name': 'CoordinateDescription'},
            'comments': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'coordinate': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['expeditions.Coordinate']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        u'expeditions.crew': {
            'Meta': {'object_name': 'Crew'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'names': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'expeditions.displacement': {
            'Meta': {'object_name': 'Displacement'},
            'destination': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'destination_displacement_related'", 'unique': 'True', 'null': 'True', 'to': u"orm['expeditions.Coordinate']"}),
            'hitchgraph': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['expeditions.HitchGraph']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'source_displacement_related'", 'unique': 'True', 'null': 'True', 'to': u"orm['expeditions.Coordinate']"})
        },
        u'expeditions.displacementdescription': {
            'Meta': {'object_name': 'DisplacementDescription'},
            'cheating': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comments': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'displacement': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['expeditions.Displacement']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'transport': ('django.db.models.fields.CharField', [], {'default': "'CAR'", 'max_length': '255'})
        },
        u'expeditions.expedition': {
            'Meta': {'ordering': "('_order',)", 'object_name': 'Expedition', '_ormbases': [u'pages.Page']},
            'destination_en': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'destination_fr': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'destination_ru': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'destination_zh': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'page_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['pages.Page']", 'unique': 'True', 'primary_key': 'True'}),
            'story_en': ('mezzanine.core.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'story_fr': ('mezzanine.core.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'story_ru': ('mezzanine.core.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'story_zh': ('mezzanine.core.fields.RichTextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'expeditions.hitchgraph': {
            'Meta': {'object_name': 'HitchGraph'},
            'expedition': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['expeditions.Expedition']", 'symmetrical': 'False'}),
            'finish': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'finish_coordinate_related'", 'to': u"orm['expeditions.Coordinate']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'start': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'start_coordinate_related'", 'to': u"orm['expeditions.Coordinate']"})
        },
        u'generic.assignedkeyword': {
            'Meta': {'ordering': "('_order',)", 'object_name': 'AssignedKeyword'},
            '_order': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keyword': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'assignments'", 'to': u"orm['generic.Keyword']"}),
            'object_pk': ('django.db.models.fields.IntegerField', [], {})
        },
        u'generic.keyword': {
            'Meta': {'object_name': 'Keyword'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sites.Site']"}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        u'pages.page': {
            'Meta': {'ordering': "('titles',)", 'object_name': 'Page'},
            '_meta_title': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            '_order': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'content_model': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'expiry_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'gen_description': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_menus': ('mezzanine.pages.fields.MenusField', [], {'default': '(1, 2, 3)', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'in_sitemap': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'keywords': ('mezzanine.generic.fields.KeywordsField', [], {'object_id_field': "'object_pk'", 'to': u"orm['generic.AssignedKeyword']", 'frozen_by_south': 'True'}),
            'keywords_string': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'login_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': u"orm['pages.Page']"}),
            'publish_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'short_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['sites.Site']"}),
            'slug': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'titles': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True'})
        },
        u'sites.site': {
            'Meta': {'ordering': "('domain',)", 'object_name': 'Site', 'db_table': "'django_site'"},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['expeditions']