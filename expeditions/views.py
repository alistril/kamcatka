from django.shortcuts import render_to_response
from expeditions.models import Expedition
from django.http import HttpResponseNotFound
from django.contrib.auth.models import User
from django.template.context import RequestContext

from django.contrib.gis.geoip import GeoIP

from rest_framework import generics
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response
from .serializers import HitchGraphSerializer,HitchGraphUserSerializer,UserProfileSerializer,CoordinateSerializer,CoordinateDescriptionSerializer
from .models import HitchGraph,Coordinate,CoordinateDescription
from user_profiles.models import ProfileCard

from expeditions.permissions import IsOwnerOrReadOnly

from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.http import HttpResponse
import simplejson

from mezzanine.conf import settings

from expeditions.forms import ImageForm

@api_view(['GET'])
def api_root(request, format=None):
    """
    The entry endpoint of our API.
    """
    return Response({
        'hitchgraphs': reverse('hitchgraph-list', request=request),
        'userprofiles': reverse('userprofile-list', request=request),
    })

class HitchGraphList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of hitchgraphs.
    """
    model = HitchGraph
    serializer_class = HitchGraphSerializer
    
    def initial(self, request, *args, **kwargs):        
        if "user" in self.request.GET and int(self.request.GET["user"])==1:
            self.serializer_class = HitchGraphUserSerializer
        else:
            self.serializer_class = HitchGraphSerializer
        super(HitchGraphList,self).initial(request, *args, **kwargs)
    
    def get_queryset(self):
        g = GeoIP()      
        country = g.country(self.request.META['REMOTE_ADDR'])
        if "expedition" in self.request.GET:
            hgqs = HitchGraph.objects.filter(expedition__id=self.request.GET["expedition"])
        else:
            hgqs = HitchGraph.objects.all()
        
        if not self.request.user.groups.filter(name="watchers").exists():
            hgqs = hgqs.exclude(places_blocked__iso2=country['country_code'])
        
        if "user" in self.request.GET and int(self.request.GET["user"])==1:
            for graph in hgqs:
                graph.filter_by_user(self.request.user)
            
        return hgqs
    
class HitchGraphDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a outputs a hitchgraph (read-only).
    """
    model = HitchGraph
    serializer_class = HitchGraphSerializer
    
    def get_queryset(self):
        g = GeoIP()      
        country = g.country(self.request.META['REMOTE_ADDR'])
        if not self.request.user.groups.filter(name="watchers").exists():
            hgqs = HitchGraph.objects.exclude(places_blocked__iso2=country['country_code'])
            
        return hgqs

class UserProfileList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of profiles with some embedded user info.
    """
    model = ProfileCard
    serializer_class = UserProfileSerializer
    
class CoordinateDetail(generics.RetrieveUpdateAPIView):
    model = Coordinate
    serializer_class = CoordinateSerializer   
    

class CoordinateStartOfActiveDetail(generics.RetrieveUpdateAPIView):
    model = Coordinate
    serializer_class = CoordinateSerializer
    
    def get_object(self,queryset):
        settings.use_editable()
        return HitchGraph.objects.get(pk=settings.ACTIVE_HITCHGRAPH).start

    
class CoordinateDescriptionDetail(generics.RetrieveUpdateAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsOwnerOrReadOnly,)

    model = CoordinateDescription
    serializer_class = CoordinateDescriptionSerializer



@csrf_exempt
def save_file(request):
    if request.method=="POST":        
        pic = request.FILES['picture']

        path = default_storage.save("descriptions/"+pic.name, ContentFile(pic.read()))
        json = simplejson.dumps({"ok":True,"path": path})
        return HttpResponse('<textarea data-type="application/json">'+json+'</textarea>')
    
        
    
def hitchgraph_detail_map(request,h_id):
    h = HitchGraph.objects.get(id=h_id)
    return render_to_response("expeditions/map_detail.html",{ "hitchgraph" : h}, 
                              context_instance=RequestContext(request))