from django import template
from expeditions.models import HitchGraph 
from django.conf import settings

register = template.Library()

@register.inclusion_tag('expeditions/hitchgraph_js_css.html')
def hitchgraph_header():
    return {"STATIC_URL" : settings.STATIC_URL}

@register.inclusion_tag('expeditions/hitchgraph.html')
def display_hitchgraph(hitchgraph, admin=False):
    ctx = {"graph" : hitchgraph}
    if admin:
        ctx.update({"admin":True})
    return ctx

def is_blocked_for_ip(parser, token):
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, hitchgraph, ip = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError("%r tag requires two arguments" % token.contents.split()[0])

    return IsBlockedForIpNode(hitchgraph,ip)


class IsBlockedForIpNode(template.Node):
    def __init__(self, hitchgraph,ip):
        self.hitchgraph = template.Variable(hitchgraph)
        self.ip         = template.Variable(ip)

    def render(self, context):
        try:
            hitchgraph = self.hitchgraph.resolve(context)
            ip = self.ip.resolve(context)
            return hitchgraph.is_blocked(ip)
        except template.VariableDoesNotExist:
            return False