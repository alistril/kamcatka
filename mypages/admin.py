from django.contrib import admin
from mezzanine.pages.admin import PageAdmin
from .models import MyRichTextPage

admin.site.register(MyRichTextPage, PageAdmin)
