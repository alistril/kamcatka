from mezzanine.pages.models import Page
from mezzanine.core.fields import RichTextField
from mezzanine.generic.fields import CommentsField
from transmeta import TransMeta
try:
    from _pyio import __metaclass__
except:
    __metaclass__ = type
from transmeta_pages.meta import MetaPageTranslate

class MyRichTextPage(Page):    
    __metaclass__ = MetaPageTranslate
    
    content = RichTextField()
    comments = CommentsField()
    class Meta:
        translate = ('content', )