from mezzanine.conf import register_setting

register_setting(
    name="MAX_KILOMETERS_PER_DAY",
    description="Maximum number of kilometers hitched in a day.",
    editable=True,
    default=1000,
)

register_setting(
    name="MIN_NOTICABLE_DISTANCE",
    description="The minimum distance between two locations before it's considered the same.",
    editable=True,
    default=50,
)
