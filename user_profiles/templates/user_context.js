var server_context = {
		STATIC_URL : "{{ STATIC_URL }}",
		MEDIA_URL : "{{ MEDIA_URL }}",
		app_args :  {
			location_default : 	{
				address : "{{address}}",
				lat_lon : "{{lat}},{{lon}}"
			}
		}
};
