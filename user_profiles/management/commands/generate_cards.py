from django.core.management.base import BaseCommand
from user_profiles.models import CardCode
import random

class Command(BaseCommand):
    args = '<nb_cards>'
    help = 'generates a specified number of available cards for logins'

    def handle(self, *args, **options):
        try:
            nb_cards = int(args[0])
        except:
            nb_cards = 5
            
        alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        #uncomment this line for more possibilities
        #alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZazertyuiopqsdfghjklmwxcvbn"
        self.stdout.write("Generating %d cards" % nb_cards)
        nb_success = 0
        for card in range(nb_cards):
            for tries in range(1000):
                candidate = ''.join(random.choice(alphabet) for i in range(5))
                if CardCode.objects.filter(value=candidate).exists():
                    continue
                else:
                    CardCode.objects.create(value=candidate)
                    nb_success = nb_success + 1
                    self.stdout.write("%d. %s" % (nb_success,candidate))
                    break
                

            
        self.stdout.write("Successfully generated %d cards" % nb_success)
