from django.http import HttpResponseRedirect
from mezzanine.utils.models import get_user_model
from django.core.urlresolvers import reverse
import requests

def redirect_to_form(*args, **kwargs):
    if (kwargs["is_new"] and not kwargs['request'].session.get('saved_username')) or \
        (not kwargs["is_new"] and kwargs['request'].user.is_authenticated() and kwargs['request'].user.get_profile() and kwargs['request'].user.get_profile().location[0] == 1.0 and kwargs['request'].user.get_profile().location[0] == 1.0):
        kwargs['request'].session['saved_username']  = kwargs.get('username')
        #only works in the case of facebook
        if kwargs.get('response',None) and isinstance(kwargs['response'],dict):
            if kwargs['response'].get('location',None):
                if isinstance(kwargs['response']['location'],dict):
                    if kwargs['response']['location'].get('id',None):                
                        r = requests.get("http://graph.facebook.com/%s/" % kwargs['response']['location']['id'])                
                        if r.status_code == 200:                    
                            data = r.json()
                            if data.get("name",None) and data.get("location",None) and data["location"].get("latitude",None) and data["location"].get("longitude",None):
                                kwargs['request'].session['saved_location'] = [data["location"]["latitude"] ,data["location"]["longitude"]]
                                kwargs['request'].session['saved_location_name'] = data["name"]
                
    
        return HttpResponseRedirect(reverse("user_profiles-review"))


def publish_created_user(*args,**kwargs):
    print "publish_created_user"
    user = get_user_model().objects.get(username=kwargs['request'].session['saved_username'])
    return {
        'user': user,
        'original_email': user.email,
        'is_new': True
    }