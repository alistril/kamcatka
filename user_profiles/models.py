from django.db.models.signals import post_save
from django.dispatch import receiver

from django.contrib.auth.models import User


from django.contrib.gis.db import models
from django.contrib.gis.geos import Point

from location_field.models import LocationField
from utils.models import BootstrapDateTimeField
import datetime
  
class CardCode(models.Model):
    value   = models.CharField("card code",max_length=5,primary_key=True)
    def __unicode__(self):
        return self.value
    
class ProfileCard(models.Model):
    # This field is required.
    user = models.OneToOneField(User)

    #card_code   = models.CharField("card code",max_length=5,null=True)
    card_code   = models.ForeignKey(CardCode,null=True)
    when        = BootstrapDateTimeField("When did we meet?",default=datetime.datetime.now())
    where       = models.CharField("Where did we meet?",max_length=255,null=True)
    
    location    = LocationField(based_fields=[where], zoom=7, default=Point(1, 1))
    objects     = models.GeoManager()


@receiver(post_save, sender=User)
def user_saved(sender=None, instance=None, **kwargs):    
    ProfileCard.objects.get_or_create(user=instance,defaults={"location":Point(1, 1)})

