from  django.test import TestCase
from expeditions.models import HitchGraph,Coordinate,Displacement
from django.contrib.gis.geos import Point
from django.contrib.auth.models import User
import datetime
import pytz

class RegisterUserTestCase(TestCase):
    def setUp(self):
        
        self.prague = Coordinate.objects.create(name="Prague",location=Point(0,0),datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC))
        self.brno = Coordinate.objects.create(name="Brno",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.wien = Coordinate.objects.create(name="Wien",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.maribor = Coordinate.objects.create(name="Maribor",location=Point(0,0),datetime=datetime.datetime(2013, 1, 3,0,0,0,0,pytz.UTC))
        self.zagreb = Coordinate.objects.create(name="Zagreb",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.sarajevo = Coordinate.objects.create(name="Sarejevo",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.beograd = Coordinate.objects.create(name="Beograd",location=Point(0,0),datetime=datetime.datetime(2013, 1, 5,0,0,0,0,pytz.UTC))

        self.h = HitchGraph.objects.create(start=self.prague,finish=self.beograd,name="The actual way")
        self.h.displacement_set.create(source=self.prague,destination=self.brno)
        self.h.displacement_set.create(source=self.brno,destination=self.wien)
        self.h.displacement_set.create(source=self.wien,destination=self.maribor)
        self.h.displacement_set.create(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.create(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.create(source=self.sarajevo,destination=self.beograd)
    
    def assert_invariants(self):
        self.h = HitchGraph.objects.get()
        
        self.assertEqual(Coordinate.objects.count(), 8)
        self.assertEqual(Displacement.objects.count(), 7)

    def assert_no_change(self):        
        self.assert_invariants()
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.brno)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)

    """
        myFile = open('log.html', 'w')
        myFile.write(str(response))
        myFile.close()
    """
    def test_insert_middle(self):
        response = self.client.get('/account/signup/')
        
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/account/signup/', {                                              
                                              'username'    : 'lisa', 
                                              'password1'   : 'lisalisa',
                                              'password2'   : 'lisalisa',
                                              'email'       : 'lisa@lisa.com',
                                              'card_code'   : 'AAAAA',
                                              'when'        : '2013-01-02 10:00:00',
                                              'where'       : 'graz',
                                              'location'    : '47.079007,15.439796'
                                              })
        
        self.assertNotEqual(response.status_code, 500)        
        self.assert_invariants()
        
        self.graz = self.h.coordinates_qs().get(address="graz")
        self.assertEqual(self.graz.coordinatedescription_set.count(),1)
        self.assertEqual(self.graz.coordinatedescription_set.get().user.username,"lisa")
        
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.beograd)
        
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.graz)
        self.h.displacement_set.get(source=self.graz,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        
    def test_insert_start(self):
        response = self.client.get('/account/signup/')
        
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/account/signup/', {                                              
                                              'username'    : 'lisa', 
                                              'password1'   : 'lisalisa',
                                              'password2'   : 'lisalisa',
                                              'email'       : 'lisa@lisa.com',
                                              'card_code'   : 'AAAAA',
                                              'when'        : '2012-01-01 10:00:00',
                                              'where'       : 'graz',
                                              'location'    : '47.079007,15.439796'
                                              })
        
        self.assertNotEqual(response.status_code, 500)        
        self.assert_invariants()
        
        self.graz = self.h.coordinates_qs().get(address="graz")
        self.assertEqual(self.graz.coordinatedescription_set.get().user.username,"lisa")
        self.assertEqual(self.graz.coordinatedescription_set.count(),1)
        self.assertEqual(self.h.start, self.graz)
        self.assertEqual(self.h.finish, self.beograd)
        
        self.h.displacement_set.get(source=self.graz,destination=self.prague)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)        
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        
    def test_insert_finish(self):
        response = self.client.get('/account/signup/')
        
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/account/signup/', {                                              
                                              'username'    : 'lisa', 
                                              'password1'   : 'lisalisa',
                                              'password2'   : 'lisalisa',
                                              'email'       : 'lisa@lisa.com',
                                              'card_code'   : 'AAAAA',
                                              'when'        : '2014-01-01 10:00:00',
                                              'where'       : 'graz',
                                              'location'    : '47.079007,15.439796'
                                              })
        
        self.assertNotEqual(response.status_code, 500)        
        self.assert_invariants()
        
        self.graz = self.h.coordinates_qs().get(address="graz")
        self.assertEqual(self.graz.coordinatedescription_set.get().user.username,"lisa")
        self.assertEqual(self.graz.coordinatedescription_set.count(),1)
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.graz)
        
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)        
        self.h.displacement_set.get(source=self.wien,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        self.h.displacement_set.get(source=self.beograd,destination=self.graz)

class ModifyUserTestCase(TestCase):
    def setUp(self):
        self.lisa = User.objects.create_user('lisa', 'lisa@lisa.com', 'lisalisa')
        
        self.prague = Coordinate.objects.create(name="Prague",location=Point(0,0),datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC))
        self.brno = Coordinate.objects.create(name="Brno",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.wien = Coordinate.objects.create(name="Wien",location=Point(0,0),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.maribor = Coordinate.objects.create(name="Maribor",location=Point(0,0),datetime=datetime.datetime(2013, 1, 3,0,0,0,0,pytz.UTC))
        
        self.zagreb = Coordinate.objects.create(name="Zagreb",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.zagreb.coordinatedescription_set.create(user=self.lisa)
        
        self.sarajevo = Coordinate.objects.create(name="Sarejevo",location=Point(0,0),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.beograd = Coordinate.objects.create(name="Beograd",location=Point(0,0),datetime=datetime.datetime(2013, 1, 5,0,0,0,0,pytz.UTC))
        
        self.h = HitchGraph.objects.create(start=self.prague,finish=self.beograd,name="The actual way")
        self.h.displacement_set.create(source=self.prague,destination=self.brno)
        self.h.displacement_set.create(source=self.brno,destination=self.wien)
        self.h.displacement_set.create(source=self.wien,destination=self.maribor)
        self.h.displacement_set.create(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.create(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.create(source=self.sarajevo,destination=self.beograd)
    
    def assert_invariants(self):
        self.h = HitchGraph.objects.get()

        self.assertEqual(Coordinate.objects.count(), 7)
        self.assertEqual(Displacement.objects.count(), 6)

    def assert_no_change(self):        
        self.assert_invariants()
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.brno)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)

    """
        myFile = open('log.html', 'w')
        myFile.write(str(response))
        myFile.close()
    """
    def test_change_middle(self):
        user = self.client.login(username='lisa',password='lisalisa')
        
        response = self.client.get('/account/update/')
        
        self.assertEqual(response.status_code, 200)

        response = self.client.post('/account/update/', {                                              
                                              'username'    : 'lisa', 
                                              'email'       : 'lisa@lisa.com',
                                              'card_code'   : 'AAAAA',
                                              'when'        : '2013-01-02 10:00:00',
                                              'where'       : 'graz',
                                              'location'    : '47.079007,15.439796'
                                              })
        
        self.assertNotEqual(response.status_code, 500)        
        self.assert_invariants()
        
        self.graz = self.h.coordinates_qs().get(address="graz")
        self.assertEqual(self.graz.coordinatedescription_set.count(),1)
        self.assertEqual(self.graz.coordinatedescription_set.get().user.username,"lisa")
        
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.beograd)
        
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.graz)
        self.h.displacement_set.get(source=self.graz,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)
        
    def test_login_update_middle(self):
        response = self.client.get('/account/signup/')
        
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/account/signup/', {                                              
                                              'username'    : 'lisa2', 
                                              'password1'   : 'lisalisa',
                                              'password2'   : 'lisalisa',
                                              'email'       : 'lisa2@lisa.com',
                                              'card_code'   : 'AAAAA',
                                              'when'        : '2014-01-01 10:00:00',
                                              'where'       : 'graz',
                                              'location'    : '47.079007,15.439796'
                                              })
        
        self.assertNotEqual(response.status_code, 500)
        
        response = self.client.get('/account/update/')
        
        self.assertEqual(response.status_code, 200)

        for i in range(2):
            response = self.client.post('/account/update/', {                                              
                                              'username'    : 'lisa2', 
                                              'email'       : 'lisa2@lisa.com',
                                              'card_code'   : 'AAAAA',
                                              'when'        : '2013-01-02 10:00:00',
                                              'where'       : 'graz2',
                                              'location'    : '47.079007,15.439796'
                                              })
        
        self.assertNotEqual(response.status_code, 500)        
        
        self.h = HitchGraph.objects.get()

        self.assertEqual(Coordinate.objects.count(), 8)
        self.assertEqual(Displacement.objects.count(), 7)
        
        self.graz = self.h.coordinates_qs().get(address="graz2")
        self.assertEqual(self.graz.coordinatedescription_set.count(),1)
        self.assertEqual(self.graz.coordinatedescription_set.get().user.username,"lisa2")
        
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.beograd)
        
        self.h.displacement_set.get(source=self.prague,destination=self.brno)
        self.h.displacement_set.get(source=self.brno,destination=self.wien)
        self.h.displacement_set.get(source=self.wien,destination=self.graz)
        self.h.displacement_set.get(source=self.graz,destination=self.maribor)
        self.h.displacement_set.get(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.get(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.get(source=self.sarajevo,destination=self.beograd)

class SuspisiousCoordinate(TestCase):
    def setUp(self):
        self.lisa = User.objects.create_user('lisa', 'lisa@lisa.com', 'lisalisa')
        
        self.prague = Coordinate.objects.create(name="Prague",location=Point(50.0755381,14.43780049999998),datetime=datetime.datetime(2013, 1, 1,0,0,0,0,pytz.UTC))
        self.brno = Coordinate.objects.create(name="Brno",location=Point(49.1950602,16.606837100000007),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.wien = Coordinate.objects.create(name="Wien",location=Point(48.2081743,16.37381890000006),datetime=datetime.datetime(2013, 1, 2,0,0,0,0,pytz.UTC))
        self.maribor = Coordinate.objects.create(name="Maribor",location=Point(46.5546503,15.645881199999963),datetime=datetime.datetime(2013, 1, 3,0,0,0,0,pytz.UTC))
        
        self.zagreb = Coordinate.objects.create(name="Zagreb",location=Point(45.8130293,15.977894900000024),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.zagreb.coordinatedescription_set.create(user=self.lisa)
        
        self.sarajevo = Coordinate.objects.create(name="Sarejevo",location=Point(43.8562586, 18.413076300000057),datetime=datetime.datetime(2013, 1, 4,0,0,0,0,pytz.UTC))
        self.beograd = Coordinate.objects.create(name="Beograd",location=Point(44.8205556, 20.462222200000042),datetime=datetime.datetime(2013, 1, 5,0,0,0,0,pytz.UTC))
        
        self.h = HitchGraph.objects.create(start=self.prague,finish=self.beograd,name="The actual way")
        self.h.displacement_set.create(source=self.prague,destination=self.brno)
        self.h.displacement_set.create(source=self.brno,destination=self.wien)
        self.h.displacement_set.create(source=self.wien,destination=self.maribor)
        self.h.displacement_set.create(source=self.maribor,destination=self.zagreb)
        self.h.displacement_set.create(source=self.zagreb,destination=self.sarajevo)
        self.h.displacement_set.create(source=self.sarajevo,destination=self.beograd)
    
    def assert_invariants(self):
        self.h = HitchGraph.objects.get()

        self.assertEqual(Coordinate.objects.count(), 7)
        self.assertEqual(Displacement.objects.count(), 6)

    def assert_no_change(self):        
        self.assert_invariants()
        self.assertEqual(self.h.start, self.prague)
        self.assertEqual(self.h.finish, self.brno)
        self.h.displacement_set.get(source=self.prague,destination=self.brno)

    def test_change_middle(self):
        user = self.client.login(username='lisa',password='lisalisa')
        
        response = self.client.get('/account/update/')
        
        self.assertEqual(response.status_code, 200)
        #insert moldavia just after wien... shouldn't be possible
        response = self.client.post('/account/update/', {                                              
                                              'username'    : 'lisa', 
                                              'email'       : 'lisa@lisa.com',
                                              'card_code'   : 'AAAAA',
                                              'when'        : '2013-01-02 10:00:00',
                                              'where'       : 'Chinisau',
                                              'location'    : '47.02685899999999,28.84155099999998'
                                              })
        
        