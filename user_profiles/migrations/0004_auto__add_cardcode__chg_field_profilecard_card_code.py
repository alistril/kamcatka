# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        
            
        # Adding model 'CardCode'
        db.create_table(u'user_profiles_cardcode', (
            ('value', self.gf('django.db.models.fields.CharField')(max_length=5, primary_key=True)),
        ))
        
        db.send_create_signal(u'user_profiles', ['CardCode'])

        

        # Renaming column for 'ProfileCard.card_code' to match new field type.
        db.rename_column(u'user_profiles_profilecard', 'card_code', 'card_code_id')
        
        for profilecard in orm.ProfileCard.objects.all():
            profilecard.card_code = None
            profilecard.save()
        
        # Changing field 'ProfileCard.card_code'        
        db.alter_column(u'user_profiles_profilecard', 'card_code_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['user_profiles.CardCode'], null=True))
        # Adding index on 'ProfileCard', fields ['card_code']
        db.create_index(u'user_profiles_profilecard', ['card_code_id'])


    def backwards(self, orm):
        # Removing index on 'ProfileCard', fields ['card_code']
        db.delete_index(u'user_profiles_profilecard', ['card_code_id'])

        # Deleting model 'CardCode'
        db.delete_table(u'user_profiles_cardcode')


        # Renaming column for 'ProfileCard.card_code' to match new field type.
        db.rename_column(u'user_profiles_profilecard', 'card_code_id', 'card_code')
        # Changing field 'ProfileCard.card_code'
        db.alter_column(u'user_profiles_profilecard', 'card_code', self.gf('django.db.models.fields.CharField')(max_length=5, null=True))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'user_profiles.cardcode': {
            'Meta': {'object_name': 'CardCode'},
            'value': ('django.db.models.fields.CharField', [], {'max_length': '5', 'primary_key': 'True'})
        },
        u'user_profiles.profilecard': {
            'Meta': {'object_name': 'ProfileCard'},
            'card_code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['user_profiles.CardCode']", 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('location_field.models.LocationField', [], {}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'when': ('utils.models.BootstrapDateTimeField', [], {'default': 'datetime.datetime(2013, 5, 1, 0, 0)', 'blank': 'True'}),
            'where': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        }
    }

    complete_apps = ['user_profiles']