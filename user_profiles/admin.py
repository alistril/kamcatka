from django.contrib import admin
from location_field.models import LocationField
from django.forms import widgets
from .models import ProfileCard,CardCode
from django.contrib.auth.models import User
from django.conf import settings
from django import forms 

from mezzanine.accounts.admin import UserProfileAdmin


class UserAdminForm(forms.ModelForm):    
    def _media(self):
        return super(UserAdminForm, self).media + forms.Media(
                                                        js = (
                                                              "/user_profiles/user_context.js?user=" + str(self.instance.pk), 
                                                              settings.STATIC_URL+'js/libs/require/require.js', 
                                                              settings.STATIC_URL+"js/main/user_profiles.js"
                                                              )
                                                        )
    media = property(_media)


class UserAdmin(UserProfileAdmin):
    form = UserAdminForm
    

class CardCodeAdmin(admin.ModelAdmin):
    list_display = ('value','users',)
    
    def users(self, obj):
        return ",".join(obj.profilecard_set.values_list('user__username',flat=True)) or "not taken"

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(CardCode, CardCodeAdmin)
