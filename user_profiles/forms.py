from mezzanine.accounts.forms import ProfileForm
from django import forms
from expeditions.models import Coordinate,CoordinateDescription,HitchGraph
from django.contrib.gis.measure import D
from mezzanine.conf import settings
from django.utils.translation import ugettext as _

from utils.widgets import BootstrapDateTimeInput
from django.utils.timezone import localtime
import math
from social_auth.models import UserSocialAuth
from user_profiles.models import CardCode

def clean_card_code(card_code):
    try:
        code = CardCode.objects.get(value=card_code)
        if code.profilecard_set.count()>0:
            raise forms.ValidationError("Sorry, this code is already taken by %s" % ",".join(code.profilecard_set.values_list('card_code__profilecard__user__username',flat=True)))
        return code.value            
    except CardCode.DoesNotExist:
        raise forms.ValidationError("Invalid code.")
    

class CardCodeForm(forms.Form):
    card_code = forms.CharField(label = _("Enter the 5 symbols:"),max_length=5)
    
    def clean_card_code(self):
        return clean_card_code(self.cleaned_data["card_code"])
    
class ProfileCardForm(ProfileForm):    
    def __init__(self,*args,**kwargs):        
        settings.use_editable()
        social_creation = kwargs.pop("social_creation",False)
        ret = super(ProfileCardForm, self).__init__(*args,**kwargs)
        self.fields["card_code"] = forms.CharField(label = _("Enter the 5 symbols:"),max_length=5)
        self.coordinate_met = None
        if self.instance.pk:
            #TODO: better estimation of where we met coordinate using before coord not belonging to user and this one does
            coord_descriptions_qs = self.instance.coordinatedescription_set.order_by("coordinate__datetime")
            if coord_descriptions_qs.count():
                self.coord_met_description = coord_descriptions_qs[0]
                self.coordinate_met = self.coord_met_description.coordinate
            else:
                self.coordinate_met = Coordinate() #old profile but without coordinate (for some reason)
                self.coord_met_description = CoordinateDescription()
        else:
            self.coordinate_met = Coordinate() # new profile
            self.coord_met_description = CoordinateDescription()
            
        self.hitchgraph = self.coordinate_met.get_hitchgraph()
        if self.hitchgraph is None:
            if HitchGraph.objects.filter(pk=settings.ACTIVE_HITCHGRAPH).exists():
                self.hitchgraph = HitchGraph.objects.get(pk=settings.ACTIVE_HITCHGRAPH)
            else:
                if HitchGraph.objects.filter(name="The actual way").exists():
                    self.hitchgraph = HitchGraph.objects.get(name="The actual way")
                else:
                    self.hitchgraph = None
        
        if self.instance and self.instance.pk and UserSocialAuth.objects.filter(user=self.instance).exists():            
            self.fields["email"].required = False
        
        if social_creation:
            del self.fields["password1"]
            del self.fields["password2"]
            del self.fields["email"]
            
            
        if (self.instance and not self.instance.pk and UserSocialAuth.objects.filter(user=self.instance).exists()):
            del self.fields["password1"]
            del self.fields["password2"]
        return ret    
    
   
    def clean_card_code(self):
        return clean_card_code(self.cleaned_data["card_code"])
        
    def clean(self):
        settings.use_editable()        
        #TODO: do this closest match in one query
        try:
            closest_coord_in_time_before       = self.hitchgraph.coordinates_qs().filter(datetime__lte=self.cleaned_data['when']).distance(self.cleaned_data['location']).latest()
        except Coordinate.DoesNotExist:
            closest_coord_in_time_before = Coordinate.objects.filter(pk=self.hitchgraph.start.pk).distance(self.cleaned_data['location']).get() #add the distance attribute
        try:
            closest_coord_in_time_after       = self.hitchgraph.coordinates_qs().filter(datetime__gte=self.cleaned_data['when']).distance(self.cleaned_data['location'])[0:1].get()
        except Coordinate.DoesNotExist:
            closest_coord_in_time_after = Coordinate.objects.filter(pk=self.hitchgraph.finish.pk).distance(self.cleaned_data['location']).get() #add the distance attribute
            
        if closest_coord_in_time_before.distance > closest_coord_in_time_after.distance:
            closest_coord_in_time = closest_coord_in_time_after
        else:
            closest_coord_in_time = closest_coord_in_time_before
        time_gap = math.fabs((self.cleaned_data['when'] - closest_coord_in_time.datetime).days) or 1
        
        if closest_coord_in_time.distance < D(km=settings.MAX_KILOMETERS_PER_DAY)*time_gap:
            if closest_coord_in_time.distance < D(km=settings.MIN_NOTICABLE_DISTANCE):
                self.coordinate_met = closest_coord_in_time #just edit this coordinate, don't bother creating another one                
        else: #the distance is too big to be realistic
            #is the closest coordiante a better choice?
            closest_coord_in_space  = self.hitchgraph.coordinates_qs().distance(self.cleaned_data['location']).order_by("distance")[0]
            
            if closest_coord_in_space.distance < D(km=settings.MAX_KILOMETERS_PER_DAY)*time_gap:
                self._errors["when"] = (_("It appears your time is not right. Did you write the correct date and time? You say we met around %s on %s. \
                                                However on %s, we were in %s which is %d kilometers away! It's unliklely we have travelled such a big distance (bigger than %d). \
                                                Your location seems to be near this location: %s. We reached this location on %s. Maybe your right time is closer to that moment...") % (
                                               self.cleaned_data['where'],
                                               localtime(self.cleaned_data['when']).strftime('%c'),
                                               localtime(closest_coord_in_time.datetime).strftime('%c'),
                                               closest_coord_in_time.name or closest_coord_in_time.address,
                                               closest_coord_in_time.distance.km,
                                               D(km=settings.MAX_KILOMETERS_PER_DAY).km*time_gap,
                                               closest_coord_in_space.name or closest_coord_in_space.address,
                                               localtime(closest_coord_in_space.datetime).strftime('%c')
                                               ),)
                del self.cleaned_data["when"]
            else:
                self._errors["where"] = (_("It appears your location is not right. Check on the map if the location you wrote is correct. \
                    Reposition the marker on the map manually if necessary. You say we met around %s on %s. \
                    However on %s, we were in %s which is %d kilometers away! It's unliklely we have travelled such a big distance (bigger than %d).") %  (
                        self.cleaned_data['where'],
                        localtime(self.cleaned_data['when']).strftime('%c'),
                        localtime(closest_coord_in_time.datetime).strftime('%c'),
                        closest_coord_in_time.name or closest_coord_in_time.address,
                        closest_coord_in_time.distance.km,
                        D(km=settings.MAX_KILOMETERS_PER_DAY).km*time_gap,
                    ),)
                del self.cleaned_data["where"]
                                                             
        return self.cleaned_data
        
    def save(self,*args,**kwargs):
        ret = super(ProfileCardForm, self).save(*args,**kwargs)
        
        self.coordinate_met.address = self.cleaned_data["where"]
        self.coordinate_met.datetime = self.cleaned_data["when"]
        self.coordinate_met.location = self.cleaned_data["location"]
        
        if self.coordinate_met.pk is None:
            self.coordinate_met.save()
            self.coordinate_met.reposition(self.hitchgraph,self.coordinate_met.datetime)
        else:
            self.coordinate_met.save()
            
        self.coord_met_description.coordinate = self.coordinate_met
        self.coord_met_description.user = self.instance
        self.coord_met_description.save()
        
        return ret

    class Meta(ProfileForm.Meta):
        #model = User
        fields = ("username", "email")
        widgets = {
            'when' : BootstrapDateTimeInput(picker_format="yyyy-MM-dd HH:mm:ss"),
            'card_code' : forms.TextInput()
        }