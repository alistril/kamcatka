from django.conf.urls import patterns, url
import os

app_dir = os.path.dirname(__file__)

urlpatterns = patterns('user_profiles.views',
    url('^user_context.js$', "user_context_js", name='user_profiles-user-context-js'),
    url('^review$', "profile_detail", name='user_profiles-review'),
    url('^initial_cardcode_submit/$', "initial_cardcode_submit", name='user_profiles-initial-cardcode-submit'),
)
