from django.conf import settings
from django.shortcuts import render_to_response
from django.template.context import RequestContext
import os 
from mezzanine.utils.models import get_user_model
from social_auth.utils import setting
from django.shortcuts import render_to_response, redirect
from django.contrib.messages import info, error
from mezzanine.accounts import get_profile_form
from django.core.urlresolvers import NoReverseMatch, reverse
from mezzanine.utils.views import render
from mezzanine.utils.urls import login_redirect

from django.utils.translation import ugettext_lazy as _
from user_profiles.forms import CardCodeForm
from user_profiles.models import CardCode
from django.http.response import HttpResponseRedirect
from django.contrib.gis.geos import Point

from django.contrib.auth import login as auth_login

def user_context_js(request):
    user = get_user_model().objects.get(pk=request.GET["user"])
    address = user.get_profile().where
    lon = user.get_profile().location.x
    lat = user.get_profile().location.y    
    return render_to_response("user_context.js", {
                                                  "address" : address,
                                                  "lat"     : lat,
                                                  "lon"     : lon,
                                                  },context_instance=RequestContext(request), content_type="text/javascript")
    

def profile_detail(request):
    profile_form = get_profile_form()
    name = setting('SOCIAL_AUTH_PARTIAL_PIPELINE_KEY', 'partial_pipeline')
    
    context = {}
    user = None
    if request.session.get('saved_username',None):
        context.update({ "default_hidden" : True })         
        user = get_user_model().objects.get(username=request.session.get('saved_username'))
        if request.session.get('saved_card_code',None):
            user.get_profile().card_code = CardCode.objects.get(value=request.session['saved_card_code'])
            
        if request.session.get('saved_location',None):        
            user.get_profile().location = Point(request.session['saved_location'][1],request.session['saved_location'][0])
        
        if request.session.get('saved_location_name',None):
            user.get_profile().where = request.session['saved_location_name']
    
    
    form = profile_form(request.POST or None, request.FILES or None,
                        social_creation=request.session.get(name,False),instance=user)
    
    if request.method == "POST" :
        if  form.is_valid():      
            if request.session.get(name,None): 
                backend = request.session[name]['backend']
                form.save()
                info(request, _("Successfully signed up"))
                
                return redirect('socialauth_complete', backend=backend)
            else:
                new_user = form.save()
                auth_login(request, new_user)
                info(request, _("Successfully signed up"))            
                return login_redirect(request)
    
    context.update({"form": form, "title": _("Signup")})
    return render(request, "accounts/account_form.html", context)

def initial_cardcode_submit(request):
    if request.method=="POST":
        f = CardCodeForm(request.POST)
        if f.is_valid():
            request.session['saved_card_code'] = f.cleaned_data['card_code']
        else:
            path = settings.MEDIA_ROOT + "/frontpage/"
            file_list = os.listdir(path)
            return render_to_response("index.html", {'files': file_list,"card_code_form" : f}, context_instance=RequestContext(request))
        if "mezzanine" in request.POST:
            return HttpResponseRedirect(reverse("user_profiles-review"))
        elif "facebook" in request.POST:
            return HttpResponseRedirect(reverse("socialauth_begin",args=("facebook",)))
        elif "twitter" in request.POST:
            return HttpResponseRedirect(reverse("socialauth_begin",args=("twitter",)))
        elif "google" in request.POST:
            return HttpResponseRedirect(reverse("socialauth_begin",args=("google",)))
        elif "weibo" in request.POST:
            return HttpResponseRedirect(reverse("socialauth_begin",args=("weibo",)))
        
