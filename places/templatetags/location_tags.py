from django import template
from django.contrib.gis.geoip import GeoIP


register = template.Library()

@register.filter
def lat_lon(ip):
    g = GeoIP()
    city_str = g.city(ip)
    if city_str:            
        lat = city_str['latitude']
        lon = city_str['longitude']
    else:
        lat = 0.
        lon = 0.
        
    return "%f,%f"% (lat,lon)

@register.filter
def city(ip):
    g = GeoIP()
    city_str = g.city(ip)
    if city_str:            
        return city_str['city']        
    else:
        return ""
    