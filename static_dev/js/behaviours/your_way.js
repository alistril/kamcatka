define([
  'jquery',
  'underscore',
  'moment',
  'gmaps',
  'hitchgraph/app',
  'hitchgraph/models',
  'hitchgraph/views',
  'your_way/views'
], function($, _, moment,gmaps,app,models,views,forms){
	app.addInitializer(function(options){		
		//populate models
		app.hitchgraphs = new models.HitchGraphCollection({url : options.hitchgraph_res });
		app.hitchgraphs.fetch({
			data 	: 	options.hitchgraph_res.data,
			success	: 	function(hitchgraphcollection, response){
				function setup_new_coordinate(endpoint){
					if(endpoint.has("displacement_destination_related")){
						vec = {
								lat : (endpoint.get("location").lat()-endpoint.get("displacement_destination_related").get("source").get("location").lat()),
								lng : (endpoint.get("location").lng()-endpoint.get("displacement_destination_related").get("source").get("location").lng())
						};
						var norm = Math.sqrt((vec.lat*vec.lat+vec.lng*vec.lng));
						vec.lat/=norm;
						vec.lng/=norm;
					}else{
						vec = {lat : 0, lng : 1};
					}

					app.new_coordinate.set({
						location : new gmaps.LatLng(endpoint.get("location").lat()+vec.lat,endpoint.get("location").lng()+vec.lng),
						datetime : moment(endpoint.get("datetime"))
					});
					
				}
				
				h = hitchgraphcollection.at(0);
				//instanciate my profile which might be used to locate my description in the description mix
				app.myself = h.get("userprofile_set").get(app.server_config.USER.id);
				var finish = h.get("finish").get("location");
				var vec;
				if(h.get("finish").has("displacement_destination_related")){
					vec = {
							lat : (finish.lat()-h.get("finish").get("displacement_destination_related").get("source").get("location").lat()),
							lng : (finish.lng()-h.get("finish").get("displacement_destination_related").get("source").get("location").lng())
					};
					var norm = Math.sqrt((vec.lat*vec.lat+vec.lng*vec.lng));
					vec.lat/=norm;
					vec.lng/=norm;
				}else{
					vec = {lat : 0, lng : 1};
				}
				
				app.new_coordinate = new models.Coordinate({
					name : "?",
					location : new gmaps.LatLng(finish.lat()+vec.lat,finish.lng()+vec.lng),
					datetime : moment(h.get("finish").get("datetime")),
					draggable : true,
				});

				app.new_displacement = new models.Displacement({
					source		: h.get("coordinate_set").get(h.get("finish")),
					destination : app.new_coordinate
				});
				
				h.get("displacement_set").add(app.new_displacement);
				//find right index to keep the stuff sorted
				var index_of_displ_source = h.get("coordinate_set").indexOf(app.new_displacement.get("source"));
				h.get("coordinate_set").add(app.new_coordinate,{at:index_of_displ_source+1});
				h.get("coordinate_set").each(function(coordinate){
					coordinate.set("draggable",true);
				});
				
				$(document).ready(function(){
					var layout = new views.GraphMapView({
							el 			: $("#expedition-itineraries"),
							model		: hitchgraphcollection.at(0)
							});

					
					layout.render();
					app.new_coordinate.select();
					var form = new forms.FormView({model:app.new_coordinate});
					
					h.on("coordinate_selected",function(coordinate){
						if(coordinate!=app.new_coordinate && !coordinate.has("displacement_source_related")){
							//nullify related displacement of coordinate
							setup_new_coordinate(coordinate);
							h.get("displacement_set").remove(app.new_displacement);
							var index_of_displ_source = h.get("coordinate_set").indexOf(coordinate);
							app.new_displacement.get("source").set("displacement_source_related",null);
							
							h.get("coordinate_set").remove(app.new_coordinate);
													
							app.new_displacement = new models.Displacement({
								source		: coordinate,
								destination : app.new_coordinate,
								//hitchgraph	: h
							});
							//app.new_coordinate.set("hitchgraph",h);
							
							h.get("coordinate_set").add(app.new_coordinate,{at:index_of_displ_source+1});							
							h.get("displacement_set").add(app.new_displacement);
							
						}
						form.close();
						form = new forms.FormView({model:coordinate});
					});
				});
			}
		});
	
	});
	
	return app;
});