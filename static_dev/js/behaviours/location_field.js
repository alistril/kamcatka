define([
  'jquery',
  'underscore',
  'moment',
  'location_field/app',
  'location_field/models',
  'location_field/views',  
  'datetimepicker'
], function($, _, moment,app,models,views){
	app.addInitializer(function(options){
		$(document).ready(function(){
			var start = new models.Coordinate();
			start.fetch({ 
				url : "/expeditions/res/coordinates/start_of_active_hitchgraph/",
				success : function(){
					$('#id_when_picker').datetimepicker({
				    	 startDate: moment(start.get("datetime")).toDate()
				     });
				},
				error : function(){
					console.warn("error retrieving start");
					$('#id_when_picker').datetimepicker();
				}
			}); 
		});
	});	
	
	
	app.addInitializer(function(options){
		var location;
		var name;
		
		
		$(document).ready(function(){
			if(options.hide_default){
				$(".control-group.input_id_username").hide();
				$(".control-group.input_id_email").hide();
				$(".control-group.input_id_email").hide();
				$(".control-group.input_id_password1").hide();
				$(".control-group.input_id_password2").hide();
			}
			
			if($("#id_location").val() && $("#id_location").val()!="1.0,1.0" && $("#id_location").val()!="0,0"){
				address 	= $($(".location-block").attr("data-fields")).val();
				location 	= $("#id_location").val();
			}else{
				if(options.location_default.lat_lon && (options.location_default.lat_lon!="1.0,1.0" && options.location_default.lat_lon!="0,0") ){
					location = options.location_default.lat_lon;
					address = options.location_default.address;
				}else{
					location = options.location.lat_lon;
					address = options.location.city;
				}
			}
			app.coordinate = 		new models.Coordinate	({
				"address" : address,
				"name" : address,
				"location" : location
				});
			
			based_field_tag = $(".location-block").attr("data-fields");
			
			app.location_input = 	new views.LocationInput	({el:"#id_location",  model: app.coordinate});
			app.place_input = 		new views.PlaceInput	({el:based_field_tag, model: app.coordinate});
			app.map = 				new views.MapView		({el:"#map_location", model: app.coordinate});
			app.map.render();
			
		});
	});
  
	return app; //the app will be automatically started with generated arguments (to become a behaviour)
});