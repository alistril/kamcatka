define([
  'jquery',
  'underscore',  
  'hitchgraph/app',
  'hitchgraph/models',
  'hitchgraph/views_map',
  'handlebars',
  'templates'
], function($, _, app,models,views_map,handlebars,templates){
	app.addInitializer(function(options){
		//populate models
		app.hitchgraph = new models.HitchGraph( {id : options.hitchgraph_res.data.hitchgraph } );
		
		app.hitchgraph.fetch({
			data 	: 	options.hitchgraph_res.data,			
			success	: 	function(hitchgraph, response){
				app.trigger("hitchgraphs_loaded");
				$(document).ready(function(){
					v = new views_map.MapView({
						el 			: $("#expedition-itineraries"),
						model		: app.hitchgraph
						});
					v.render();
					
				});
			}
		});
		
	});
	return app; //the app will be automatically started with generated arguments (to become a behaviour)
});