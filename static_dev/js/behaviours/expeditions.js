define([
  'jquery',
  'underscore',  
  'hitchgraph/app',
  'hitchgraph/models',
  'hitchgraph/views',  
  'datepicker'
], function($, _, app,models,views){
	app.addInitializer(function(options){
		$(document).ready(function(){
		     $('#id_datetime').datepicker({format: 'yyyy-mm-dd'}).on('changeDate', function(ev){	    	 
		    	 $(this).datepicker('hide');
		     });
		});
	});	
	
	app.addInitializer(function(options){
		//populate models
		app.hitchgraphs = new models.HitchGraphCollection({url : options.hitchgraph_res });
		progress_layout = new views.LoadingHitchgraphsView({
			el 			: $("#expedition-loading"),
			collection	: app.hitchgraphs
			});
		progress_layout.on("complete", function(){			
			progress_layout.close();
		});
		progress_layout.render();
		
		app.hitchgraphs.fetch({
			data 	: 	options.hitchgraph_res.data,
			xhr: function() {
                var xhr = $.ajaxSettings.xhr();
                xhr.onprogress = function(evt){
                    var percentComplete = 0;
                    if (evt.lengthComputable) {  
                        percentComplete = evt.loaded / evt.total;
                    }
                    app.hitchgraphs.trigger("progress",Math.round(percentComplete * 100));
                };
                return xhr;
            },
			success	: 	function(hitchgraphcollection, response){
				app.trigger("hitchgraphs_loaded");
				$(document).ready(function(){
					var layout;
					if(hitchgraphcollection.length > 1){
						layout = new views.Tabs({
							el 			: $("#expedition-itineraries"),
							collection	: hitchgraphcollection
							});
					}else if(hitchgraphcollection.length > 0){						
						layout = new views.GraphMapView({
							el 			: $("#expedition-itineraries"),
							model		: hitchgraphcollection.at(0)
							});
					}else{
						//maybe instanciate an empty view
						return;
					}
					layout.render();					
				});
			}
		});
		
	});
	return app; //the app will be automatically started with generated arguments (to become a behaviour)
});