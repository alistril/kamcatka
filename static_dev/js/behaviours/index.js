define([
  'jquery',
  'underscore',  
  'backbone',
  'backbone.marionette' ,
  'bxslider'
], function($, _, Backbone,Marionette){
	var app = new Backbone.Marionette.Application();
	
	app.addInitializer(function(options){
		$(document).ready(function(){
			var end = new Date('05/27/2013 11:59 PM');

		    var _second = 1000;
		    var _minute = _second * 60;
		    var _hour = _minute * 60;
		    var _day = _hour * 24;
		    var timer;

		    function showRemaining() {
		        var now = new Date();
		        var distance = end - now;
		        if (distance < 0) {

		            clearInterval(timer);
		            document.getElementById('time-before-departure').innerHTML = 'GO!';

		            return;
		        }
		        var days = Math.floor(distance / _day);
		        var hours = Math.floor((distance % _day) / _hour);
		        var minutes = Math.floor((distance % _hour) / _minute);
		        var seconds = Math.floor((distance % _minute) / _second);

		        document.getElementById('time-before-departure').innerHTML = days + ' days ';
		        document.getElementById('time-before-departure').innerHTML += hours + ' hrs ';
		        document.getElementById('time-before-departure').innerHTML += minutes + ' mins ';
		        document.getElementById('time-before-departure').innerHTML += seconds + ' secs';
		    }

		    timer = setInterval(showRemaining, 1000);
		});
	});
	
	app.addInitializer(function(options){
		$(document).ready(function(){			
		  $('.bxslider').bxSlider({
			  adaptiveHeight: true,
			  auto: true,
			  autoControls: true
			  });
		  $('.bxslider').show();
		});
	});
	return app; //the app will be automatically started with generated arguments (to become a behaviour)
});