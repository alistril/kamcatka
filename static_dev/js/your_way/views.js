define([
  'jquery',
  'underscore',
  'gmaps',
  'backbone',
  'backbone.marionette',
  'moment',
  'hitchgraph/app',
  'datetimepicker',
  'fileupload',
  'iframe-transport',
  'csrf'
], function(
		$, _, gmaps, Backbone, Marionette, moment, app
		){
	var forms = {};
		////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////// Views ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////
		
		forms.FormView = Backbone.Marionette.View.extend({
			el : "#coordinate_form",
			events : {
				"keyup #id_name" 					: "coordinate_name_changed",
				"keyup #id_address"					: "address_changed",
				"change #id_address"				: "address_changed",
				"changeDate #id_datetime_picker"	: "datetime_changed",
				"change #id_datetime"				: "datetime_changed",
					
				"change #id_comments"				: "comment_changed",
				"change #id_picture"				: "picture_changed",
					
				"click input[type='submit']"		: "form_submitted"
			},
			modelEvents : {
				"change:location"		: "change_location_hiddenfield"
			},
			ui: {
				//coordinate
				name			: "#id_name",
				address			: "#id_address",
				location 		: "#id_location",
				datetime_picker	: "#id_datetime_picker",
				datetime		: "#id_datetime",
				
				comments		: "#id_comments",
				submit			: "input[type='submit']",
				
				//description
				comments		: "#id_comments",
				picture_widget	: "#id_picture_fileupload",
				picture_thumb	: ".fileupload-preview.thumbnail img",
				picture			: "#id_picture",
				picture_loading	: ".picture-loading#id_picture_loading",
			},
			comment_changed  : function(e){					
				if(this.description){					
					this.description.set("comments",this.ui.comments.val());
				}
			},
			picture_changed  : function(e){
				//picture possibly removed
				if(this.ui.picture && !this.ui.picture.val()){
					this.description.set("picture",null);
				}
				if(this.model.isNew() || !this.ui.picture || !this.ui.picture.val()) return; //do not upload the file, it'll be submitted with the form
				this.ui.submit.attr("disabled","disabled");
				this.ui.picture_loading.show();
				var self = this;
				$.ajax({
					url: "/expeditions/res/coordinate_descriptions/upload_picture/",
			        files: this.ui.picture,
			        iframe: true
			    }).complete(function(data) {
			    	var data_json = $.parseJSON(data.responseText);
			    	self.description.set("picture",data_json.path);
			    	self.ui.picture_loading.hide();
			    	self.ui.submit.removeAttr("disabled");
			    });
			},
			datetime_changed : function(e){
				var valid = true;
				if(!app.disable_datetime_checks){
					if(this.coordinate_prev){
						if(this.coordinate_prev.get("datetime").toDate()>e.localDate){
							e.preventDefault();
							alert("You cannot set a date earlier than the previous place ("+(this.coordinate_prev.get("name") || this.coordinate_prev.get("address"))+"). Try to edit that location's date.");
							valid = false;
						}
					}
					if(this.coordinate_next){
						if(this.coordinate_next.get("datetime").toDate()<e.localDate){
							if(this.model.isNew() && this.coordinate_next.is_ownable()){
								valid = true; //if next coordinate is weak (gps or non-owned), we don't block the dates
							}else{
								e.preventDefault();
								alert("You cannot set a date later than the next place ("+(this.coordinate_next.get("name") || this.coordinate_next.get("address"))+"). Try to edit that location's date.");
								valid = false;
							}
						}					
					}
				}
				if(valid) this.model.set("datetime",moment(this.datetimepicker.getLocalDate()));
				else this.datetimepicker.setLocalDate(this.model.get("datetime").toDate());
				//this.ui.datetime.val(this.model.get("datetime").format("YYYY-MM-DD hh:mm:ss"));
				//this.ui.datetime.datepicker('hide');
			},
			coordinate_name_changed : function(){
				this.model.set("name",this.ui.name.val());
				this.model.get("hitchgraph").trigger("coordinate_changed");
			},
			address_changed : function(){
				var self = this;
		        
	            if (this.geocoder) {
	                this.geocoder.geocode({"address": this.ui.address.val()}, function(results, status) {
	                    if (status == gmaps.GeocoderStatus.OK) {	                    	
	                    	self.model.set("location",results[0].geometry.location);
	                    	if(!self.model.get("informed"))
	                    		self.model.set("informed",true);
	                    }
	                });
	            }
			},
			change_location_hiddenfield : function(model,location){
				this.ui.location.val(location.lat()+","+location.lng());
			},
			close : function() {
				this.trigger("before:close");
				this.undelegateEvents();
				this.trigger("close");
			},
			form_submitted : function(e){
				var self = this;
				if(this.status == "update"){
					e.preventDefault();
					this.description.patch();					
					this.model.save({},{
						error : function(){
							console.error("error!!");
						},
						success : function(){
							console.log("success!!");
						}
					});
				}
			},
			initialize 		: function(){
				var self = this;
				if(app.myself){					
					this.description = app.myself.get("coordinatedescription_set").findWhere({coordinate:this.model});
				}else{
					this.description = undefined;
				}
				this.geocoder = new gmaps.Geocoder();
				this.bindUIElements();
				if(this.model.get("displacement_destination_related"))
					this.coordinate_prev = this.model.get("displacement_destination_related").get("source");
				else if(this.model.get("extreme_destination_related"))
					this.coordinate_prev = this.model.get("extreme_destination_related").get("source");
				
				if(this.model.has("displacement_source_related") && !(this.model.get("displacement_source_related").isNew()))
					this.coordinate_next = this.model.get("displacement_source_related").get("destination");
				else if(this.model.has("extreme_source_related"))
					this.coordinate_next = this.model.get("extreme_source_related").get("destination");
				//if I'm new, does previous coordinate have an extreme to limit myself to?
				else if(this.model.isNew() && this.model.get("displacement_destination_related").get("source").has("extreme_source_related")){
					this.coordinate_next = this.model.get("displacement_destination_related").get("source").get("extreme_source_related").get("destination")
				}
				
				this.ui.datetime_picker.datetimepicker();
				this.datetimepicker = this.ui.datetime_picker.data('datetimepicker');

				this.datetimepicker.setLocalDate(this.model.get("datetime").toDate());
				this.ui.name.val(this.model.get("name"));
				this.ui.address.val(this.model.get("address"));
				this.ui.location.val(this.model.get("location").lat() + "," + this.model.get("location").lng());
				this.ui.datetime.val(this.model.get("datetime").format("YYYY-MM-DD hh:mm:ss"));
				
				if(this.description){
					this.ui.comments.val(this.description.get("comments"));
					if(this.description.get("picture")){
						this.ui.picture_widget.removeClass('fileupload-new');
						this.ui.picture_widget.addClass('fileupload-exists');
						this.ui.picture_thumb.attr("src",this.description.get("picture"));
					}else{
						this.ui.picture_widget.removeClass('fileupload-exists');
						this.ui.picture_widget.addClass('fileupload-new');
					}
				}else{
					this.ui.comments.val("");
					this.ui.picture_widget.removeClass('fileupload-exists');
					this.ui.picture_widget.addClass('fileupload-new');
				}

				if(this.model.isNew()){
					this.status = "insert";
					this.ui.submit.val("add new location");
				}else{
					this.status = "update";
					this.ui.submit.val("update");					
				}				
			}			
		});
	
	return forms;
		
}); //-- End of requirejs define
