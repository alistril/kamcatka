define([
  'backbone',
  'backbone.marionette',
], function(Backbone){
	var App = new Backbone.Marionette.Application();
	
	return App;	
});