define([], function(){
	constants = {};
	constants.initiator_types = {
				USER    : 1,
			    GPS     : 2,
			    SITE    : 3
			};
	constants.gps_types = {
				OK      : 1,
			    CAMP    : 2,
			    HELP    : 3,
			    SOS     : 4
			};
	return constants;
});