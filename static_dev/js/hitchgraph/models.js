define([
  'underscore',
  'backbone',
  'gmaps',
  'moment',
  'hitchgraph/app',
  'hitchgraph/constants',
  'backbone.relational'  
], function(_, Backbone, gmaps, moment, app,constants){
	
	var models = {};
	
		////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////// Models //////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////
		models.UserProfile = Backbone.RelationalModel.extend({
			
		    initialize: function(data){
		    	if(_(data).has("location") && _(data.location).has("coordinates")){
		        	this.set(
		        			"location",
		        			new gmaps.LatLng(
		        					data.location.coordinates[1],
		        					data.location.coordinates[0]
		        					)
		        			);
		        }
		        if(_(data).has("date_met")){
		        	this.set("date_met",moment(data.datetime));
		        }
		    }
		});
		
		models.CoordinateDescription = Backbone.RelationalModel.extend({
			urlRoot : "/expeditions/res/coordinate_descriptions",
			relations: [
			            {		            	
			            	type: Backbone.HasOne,
			            	key: 'userprofile',
			            	relatedModel: models.UserProfile,
			            	includeInJSON: true,
			            	reverseRelation: {
			            		key: 'coordinatedescription_set',
			            		includeInJSON: 'id'
			            	},
			            }
			           ],
			patch : function(){
				return this.save({
						"comments" 	: this.get("comments"),
						"picture"	: this.get("picture")
					},{patch:true});
			},
		    initialize: function(){
		        
		    },
		
			has_details : function(){
				return ( this.has("comments") && this.get("comments").length ) || ( this.has("picture") && this.get("picture").length ) ;
			}
		});
		
		models.CoordinateDescriptionCollection = Backbone.Collection.extend({
			model: models.CoordinateDescription,			
		    initialize: function(){
		        
		    }
		});
		
		models.Coordinate = Backbone.RelationalModel.extend({			
			urlRoot : "/expeditions/res/coordinates",
			url : function(){
				return this.urlRoot + "/" + this.id + "/"; 
			},
			defaults: {
			    "draggable":  false
			  },
			relations: [
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'coordinatedescription_set',
			            	relatedModel: models.CoordinateDescription,
			            	includeInJSON: true,
			            	reverseRelation: {
			            		key: 'coordinate',
			            		includeInJSON: 'id'
			            	},
			            }
			           ],
			           
			parse : function(response, options){
				ret = Backbone.RelationalModel.prototype.parse.call(this,response, options);
				if(ret.location){
					ret.location = new gmaps.LatLng(
							ret.location.coordinates[1],
							ret.location.coordinates[0]
        				);
				}
				
				if(ret.datetime){
					ret.datetime = moment(ret.datetime);
				}

				return ret;
			},			
			toJSON : function(){
				data = Backbone.RelationalModel.prototype.toJSON.call(this);
				//convert to geojson instead of default serializition
				if (data.location){
					data.location = {
					        "type": "Point", 
					        "coordinates": [
					            this.get("location").lng(), 
					            this.get("location").lat()
					        ]
					};
				}				
				if(data.datetime){
					data.datetime = this.get("datetime").format();
		        }
				return data;
			},
		    initialize: function(data){
		    	if(!data) return;
		        if(_(data).has("location") && _(data.location).has("coordinates")){
		        	this.set(
		        			"location",
		        			new gmaps.LatLng(
		        					data.location.coordinates[1],
		        					data.location.coordinates[0]
		        					)
		        			);
		        }
		        if(_(data).has("datetime")){
		        	this.set("datetime",moment(data.datetime));
		        }		        
		    }/*,
		    save : function(attrs, options){
		    	options || (options = {});

		        // Get data
		        options.data = JSON.stringify(attrs);

		        // Filter the data to send to the server
		        delete options.geometry;
		        delete options.draggable;
		        
		    	Backbone.Model.prototype.save.call(this, attrs, options);
		    }*/,		    
		    has_details : function(){
		    	var ret = false;
		    	if(this.has("coordinatedescription_set")){
		    		this.get("coordinatedescription_set").each(function(description){
		    			ret = ret || description.has_details();
		    		});
		    	}
				return ret;
			},
			
			select : function(selected){
				this.get("hitchgraph").selectCoordinate(this);
			},
			is_selected : function(){
				if(!this.has("hitchgraph")) 
					return false;
				
				return (this == this.get("hitchgraph").coordinate_selected);
			},
			is_ownable : function(username){
				if(this.has("owners")){
					return _(this.get("owners")).indexOf(username)>-1;										
				}else
					return true; 
			}
		});
		
		models.CoordinateCollection = Backbone.Collection.extend({
			model: models.Coordinate,
			
			initialize: function(){
			}
		    
		});
		
		
		models.Displacement = Backbone.RelationalModel.extend({
			relations: [
			            {		            	
			            	type: Backbone.HasOne,
			            	key: 'source',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: {
			            		type: Backbone.HasOne,
			            		key: 'displacement_source_related',
			            		includeInJSON: 'id'
			            	},
			            },
			            {		            	
			            	type: Backbone.HasOne,
			            	key: 'destination',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: {
			            		type: Backbone.HasOne,
			            		key: 'displacement_destination_related',
			            		includeInJSON: 'id'
			            	},		            		
			            }
			           ],
	
		    initialize: function(){
		        
		    }
		});
		
		models.DisplacementExtreme = Backbone.RelationalModel.extend({
			relations: [
			            {		            	
			            	type: Backbone.HasOne,
			            	key: 'source',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: {
			            		type: Backbone.HasOne,
			            		key: 'extreme_source_related',
			            		includeInJSON: 'id'
			            	},
			            },
			            {		            	
			            	type: Backbone.HasOne,
			            	key: 'destination',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: {
			            		type: Backbone.HasOne,
			            		key: 'extreme_destination_related',
			            		includeInJSON: 'id'
			            	},		            		
			            }
			           ],
	
		    initialize: function(){
		        
		    }
		});
		
		models.DisplacementCollection = Backbone.Collection.extend({
			model: models.Displacement,
			
			initialize: function(){
			}
		});
		
		models.HitchGraph = Backbone.RelationalModel.extend({
			urlRoot : "/expeditions/res/hitchgraphs/",
			relations: [
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'displacement_set',
			            	relatedModel: models.Displacement,
			            	reverseRelation: {
			            		key: 'displacement_of_hitchgraph',
			            		includeInJSON: 'id'
			            	},
			            },
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'coordinate_set',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: {
			            		key: 'hitchgraph',
			            		includeInJSON: 'id'
			            	},
			            },
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'userprofile_set',
			            	relatedModel: models.UserProfile,
			            	reverseRelation: {
			            		key: 'userprofile_of_hitchgraph',
			            		includeInJSON: 'id'
			            	},
			            },
			            {		            	
			            	type: Backbone.HasOne,
			            	key: 'start',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: false
			            },
			            {		            	
			            	type: Backbone.HasOne,
			            	key: 'finish',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: false
			            },
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'start_set',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: {
			            		key: 'is_start_of',
			            		includeInJSON: 'id'
			            	}
			            },
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'finish_set',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: {
			            		key: 'is_finish_of',
			            		includeInJSON: 'id'
			            	}
			            },
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'coordinate_before_user_set',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: {
			            		key: 'before_start_of',
			            		includeInJSON: 'id'
			            	}
			            },
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'coordinate_after_user_set',
			            	relatedModel: models.Coordinate,
			            	reverseRelation: {
			            		key: 'after_finish_of',
			            		includeInJSON: 'id'
			            	}
			            },
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'displacement_before_user_set',
			            	relatedModel: models.DisplacementExtreme,
			            	reverseRelation: {
			            		key: 'displacement_before_start_of',
			            		includeInJSON: 'id'
			            	}
			            },
			            {		            	
			            	type: Backbone.HasMany,
			            	key: 'displacement_after_user_set',
			            	relatedModel: models.DisplacementExtreme,
			            	reverseRelation: {
			            		key: 'displacement_after_finish_of',
			            		includeInJSON: 'id'
			            	}
			            }/**/
			],
			selectCoordinate : function(coordinate){
				if(this.coordinate_selected)
					this.coordinate_selected.trigger("selected", this, false);
				this.coordinate_selected = coordinate;
				this.coordinate_selected.trigger("selected", this, true);
				this.trigger("coordinate_selected",this.coordinate_selected);
			},
		    initialize: function(){
		    }
		});
		
		models.HitchGraphCollection = Backbone.Collection.extend({
			url : "/expeditions/res/hitchgraphs/",
			model: models.HitchGraph,
			initialize: function(){
			}
		    
		});		
		
	return models;
	
	
}); //define
