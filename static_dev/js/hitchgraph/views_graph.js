define([
  'jquery',
  'underscore',
  'gmaps',
  'd3',
  'backbone',  
  'hitchgraph/utils',
  'hitchgraph/app',
  'hitchgraph/constants',
  //templates
  'handlebars',
  'templates'  
], function(
		$, _, gmaps, d3, Backbone, utils, app, constants,
		handlebars
		){
	
	var views = {};
		////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////// Views ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////
		views.PopoverView = Backbone.Marionette.ItemView.extend({
			template : handlebars.templates["popover.html"],
			className : "graph-node-description popover right",
			serializeData: function(){
				var data = _(this.model.toJSON()).extend(app.server_config); 
				data.datetime_formatted = utils.datetime_formatted(this.model);
				if(this.model.has("battery_state")){
					data.battery_state = this.model.get("battery_state")==1?"ok":"low";
				}
				return data;
			},
			modelEvents: {
			    "selected"				: "coordinateSelected",
			    "change:name"			: "render",
			    "popover_modal:switch"	: "popover_modal_switch"
			},
			events: {
				"click .changeto-modal" : "switchto_modal",
				"click .close" : "hide"
			},
			
			attributes : function () {				
			    return {
			      style : "width : 264px;",			      
			      id : this.model.id
			    };
			},
			switchto_modal: function(){
				this.model.trigger("popover_modal:switch",true);
			},
			hide : function(){
				this.$el.hide();
			},
			popover_modal_switch : function(modal){
				if(modal){
					this.$el.hide();
				}else{
					this.$el.show();
				}
			},
			coordinateSelected : function(hitchgraph,selected){				
				if(selected){
					if(!this.model.get("geometry").pageX){
						this.$el.hide();
						return;
					}						
					this.$el
						.css("left", (this.model.get("geometry").pageX) + "px")
						.css("top", (this.model.get("geometry").pageY) + "px").show();					
					this.$el
						.css("top", (this.model.get("geometry").pageY - this.$el.height()/2) + "px").show();					
				}else{
					this.$el.hide();
				}
			},
			initialize : function(){
				//listen to all my descriptions and render if any change
				this.model.get("coordinatedescription_set").each(function(coordinatedescription){
					this.listenTo(coordinatedescription, "change", this.render);
				},this);
				//TODO: add close to stop all that listening (and add/remove to collection)
			}
		});

		views.PopoverCollectionView = Backbone.Marionette.CollectionView.extend({
			itemView		: views.PopoverView,
			onItemviewRender : function(){				
				this.progress+=this.progress_step;				
				this.collection.trigger("popovers_building",this.progress);				
			},
			onRender : function(){				
				this.collection.trigger("popovers_built");
			},
			initialize : function(){
				this.progress_step = 100./this.collection.length;
				this.progress = 0;
			}
		});
		
		views.ModalView = Backbone.Marionette.ItemView.extend({
			template : handlebars.templates["modal.html"],
			className : "modal hide fade",
			serializeData: function(){
				var data = _(this.model.toJSON()).extend(app.server_config); 
				data.datetime_formatted = utils.datetime_formatted(this.model);
				if(this.model.has("battery_state")){
					data.battery_state = this.model.get("battery_state")==1?"ok":"low";
				}
				return data;
			},
			modelEvents: {
			    "popover_modal:switch"	: "popover_modal_switch",
			    "change:name"			: "render"
			},
			events : {
				"hidden" 				: "switchto_popover",
				"click .btn#close"		: "hide_modal"
			},
			popover_modal_switch : function(modal){
				if(modal){
					this.$el.modal();
				}else{
				}
			},
			hide_modal : function(){
				this.$el.modal('hide');
			},
			switchto_popover : function(){
				this.model.trigger("popover_modal:switch",false);
			},
			attributes : function () {				
			    return {
			      id : this.model.id
			    };
			},
			initialize : function(){				
				//listen to all my descriptions and render if any change
				this.model.get("coordinatedescription_set").each(function(coordinatedescription){
					this.listenTo(coordinatedescription, "change", this.render);
				},this);
				//TODO: add close to stop all that listening (and add/remove to collection)
			}
		});
		
		views.ModalCollectionView = Backbone.Marionette.CollectionView.extend({
			itemView		: views.ModalView,
			onItemviewRender : function(){				
				this.progress+=this.progress_step;
				this.collection.trigger("modals_building",this.progress);				
			},
			onRender : function(){				
				this.collection.trigger("modals_built");
			},
			initialize : function(){
				this.progress_step = 100./this.collection.length;
				this.progress = 0;
			}
		});

		views.HitchgraphView = Backbone.Marionette.Layout.extend({
			template: handlebars.templates["graph_layout.html"],
			className : "hitchgraph",			
			regions: {
				graph_control: "#graph-control",
				canvas: "#graph-canvas",
				popovers: "#graph-popovers",
				modals: "#graph-modals"
			},
			onRender : function(){
				this.graph_control.show(new views.GraphControlView({
					model : this.model,
					attached_el : this.canvas.el
					})
				);
				this.canvas.show(new views.GraphView({
						model : this.model
					})
				);
				this.popovers.show(new views.PopoverCollectionView({
			    		collection : this.model.get("coordinate_set")
			    	})
				);
				this.modals.show(new views.ModalCollectionView({
			    		collection : this.model.get("coordinate_set")
			    	})
				);
			}
		});

		views.GraphControlView = Backbone.Marionette.ItemView.extend({
			template: handlebars.templates["graph_control.html"],
			events : {
				"click #previous" : "coordinate_prev",
				"click #next" : "coordinate_next",
				"click #first" : "coordinate_first",
				"click #last" : "coordinate_last"
			},
			ui: {
				btn_prev	: "#previous",
				btn_next	: "#next",
				btn_first	: "#first",
				btn_last	: "#last"
			},
			modelEvents: {
				"coordinate_selected"		: "coordinateSelected"
			},
			coordinateSelected : function(coordinate){
				if(!coordinate.has("displacement_source_related")){ //we are last, disable last & next
					this.ui.btn_next.addClass("disabled");
					this.ui.btn_last.addClass("disabled");
				}else{
					this.ui.btn_next.removeClass("disabled");
					this.ui.btn_last.removeClass("disabled");
				}
				if(!coordinate.has("displacement_destination_related")){//if we are first, disable first & prev
					this.ui.btn_prev.addClass("disabled");
					this.ui.btn_first.addClass("disabled");
				}else{
					this.ui.btn_prev.removeClass("disabled");
					this.ui.btn_first.removeClass("disabled");
				}
			},			
			setup_coord_geometry : function(coordinate){
				//FIXME: parent().parent() not generic, won't work with different layouts
				coordinate.get("geometry").pageX = $(this.attached_el,this.$el.parent().parent()).position().left + $(this.attached_el,this.$el.parent().parent()).width();
    			coordinate.get("geometry").pageY = $(this.attached_el,this.$el.parent().parent()).position().top + $(this.attached_el,this.$el.parent().parent()).height()/2;
			},
		    coordinate_prev : function(e){
		    	e.preventDefault();
		    	//look for displacement of which we are destination
		    	if(this.model.coordinate_selected.has("displacement_destination_related")){//if we have a next neighbour
		    		var prev_coordinate = this.model.coordinate_selected.get("displacement_destination_related").get("source");
		    		this.setup_coord_geometry(prev_coordinate);
		    		this.model.selectCoordinate(prev_coordinate);
		    	}
		    },
		    coordinate_next : function(e){
		    	e.preventDefault();
		    	//look for displacement of which we are source
		    	if(this.model.coordinate_selected.has("displacement_source_related")){//if we have a previous neighbour
		    		var next_coordinate = this.model.coordinate_selected.get("displacement_source_related").get("destination");
		    		this.setup_coord_geometry(next_coordinate);
		    		this.model.selectCoordinate(next_coordinate);
		    	}
		    },
		    coordinate_first : function(e){
		    	e.preventDefault();
		    	if(this.model.coordinate_selected.has("displacement_destination_related")){//if we have a previous neighbour
		    		var first_coordinate = this.model.get("start");
		    		this.setup_coord_geometry(first_coordinate);
		    		this.model.selectCoordinate(first_coordinate);
		    	}
		    },
		    coordinate_last : function(e){
		    	e.preventDefault();
		    	if(this.model.coordinate_selected.has("displacement_source_related")){//if we have a next neighbour
		    		var last_coordinate = this.model.get("finish");
		    		this.setup_coord_geometry(last_coordinate);
		    		this.model.selectCoordinate(last_coordinate);
		    	}
		    },
		    onRender : function(){
		    	this.model.selectCoordinate(this.model.get("start")); //init at the first coordinate
		    },
		    initialize : function(options){
		    	this.attached_el = options.attached_el;
		    }
		});
		
		views.GraphView = Backbone.Marionette.View.extend({
			attributes : {
				width : "100%"
			},
			modelEvents: {
				"coordinate_selected"		: "coordinateSelected",
				"coordinate_changed"		: "update_svg"
			},
			collectionEvents : {
				"add"						: "coordinate_added",
				"remove"					: "coordinate_removed"
			},
			coordinateSelected : function(coordinate){
				this.radius_of_last_element = coordinate.get("geometry").r_original;
				this.render_svg();
			},
			//suboptimal coordinate add, no need to compute spiral in render_svg in some cases
			coordinate_added : function(model,collection){
				var self = this;
				var datum = {coordinate : model};
				this.data_nodes[this.model.get("coordinate_set")] = datum;

		    	var node = this.svg.selectAll(".node").data(this.data_nodes).enter().append("g");
		    	var text = node.append("text").style("text-anchor", "middle");
		    	text.append('tspan').attr("dy",0).attr("x",0).attr("class","tspan-first")
		    	text.append('tspan').attr("dy",15).attr("x",0).attr("class","tspan-second")
		    	text.append('tspan').attr("dy",15).attr("x",0).attr("class","tspan-third");
		    	node.append("circle").style("fill-opacity", .2 ).style("stroke", "steelblue").style("fill", "purple")
		    	.on("click",function(d,i){
	    			self.radius_of_last_element = d.coordinate.get("geometry").r_original;	    			 
	    			d.coordinate.get("geometry").pageX = self.$el.position().left + self.$el.width();
	    			d.coordinate.get("geometry").pageY = d3.event.pageY;
	    			d.coordinate.select();		
	    		});
		    	node.append("title");
				this.render_svg();
			},
			coordinate_removed : function(model,collection){
		    	//this.render_svg();
			},
			initialize : function(options){	
				var self = this;
				this.collection = this.model.get("coordinate_set");
				
		    	this.max_radius = 250;	// radius of viewport limit
		    	this.radius_of_last_element = this.max_radius; //
		    	
		    	this.max_circle_radius_hide_text = 15;
		    	this.max_circle_radius_hide_newlines = 30;
		    	
		    	this.spiral_options = {base_exp:1.04,max_circle_radius:50};
		    	
		    	_(this).bindAll('render_svg');
		    	
		    	this.compute_spiral(this.spiral_options.base_exp,this.spiral_options.max_circle_radius);
		    	this.create_svg({x:300,y:300},{w:670,h:this.max_radius*2.8});
		    	
		    	self.el_width = $(self.el).width();
		    	setInterval(function(){
		    		if(!self.$el.is(":visible"))
		    			return;
		    		if(Math.abs(self.el_width-$(self.el).width())>50){		    			
		    			self.el_width = $(self.el).width();
		    			self.svg_width = self.el_width;
		    			self.svg_height = self.el_width;
		    			
		    			self.max_radius = $(self.el).width()/2.68;
		    			self.radius_of_last_element = self.max_radius;
		    			self.spiral_options.base_exp = -0.000704*self.max_radius+1.226; //linear approximation of exponent evolution fn of max radius. An order 2 or 3 approx would obviously be bettre

		    			d3.select(self.el).select("svg").attr("width", "100%").attr("height", self.svg_height);
		    			self.svg.attr("transform", "translate(" + $(self.el).width()/2.3 + "," + $(self.el).width()/2.3 + ")");
		    			d3.select(self.el).select("rect").attr("width", self.svg_width).attr("height", self.svg_height);
		    			self.compute_spiral(self.spiral_options.base_exp,self.spiral_options.max_circle_radius);

		    			if(self.model.coordinate_selected && Math.abs(self.model.coordinate_selected.get("geometry").r_original-self.max_radius)>100){
		    				//self.compute_spiral(self.spiral_options.base_exp,self.spiral_options.max_circle_radius);
		    				self.coordinateSelected(self.model.coordinate_selected);
		    			}else{
		    				self.update_svg();
		    			}
		    			$(self.el).css("height",$(self.el).width());
		    		}		    		
		    	},1000);
		    },
		    render : function(){		    	
		    	this.render_svg();		    	
		    },
		    compute_spiral : function(base_exp,max_circle_radius){
		    	var self = this;
		    	if(!this.data_nodes)
		    		this.data_nodes = [];
		    	
		    	var theta = 0;
		    	var r=this.max_radius*this.max_radius/this.radius_of_last_element;
		    	var rc=max_circle_radius*this.max_radius/this.radius_of_last_element;
		    	
		    	var coordinate_start;
		    	var els_processed = 0;
		    	var coordinate;
		    	var group=0;
		    	this.data_nodes.length = this.model.get("coordinate_set").length;
		    	
		    	var start_set = this.model.get("start_set").length?this.model.get("start_set"):_([this.model.get("start")]);
		    	var progress_step = 100./this.model.get("coordinate_set").length;
		    	var progress = 0;
		    	start_set.each(function(coordinate_start){
			    	for(coordinate = coordinate_start;
			    		;
			    		els_processed++
			    		){		    		
			    		r1=r;
			    		r2=r1/base_exp;		    		
			    		alpha = rc/r1;		    		
			    		
			    		coordinate.set("geometry",{
			    					x : r* Math.cos(theta),		    					
			    					y : r* Math.sin(theta),
			    					
			    					r : r,
			    					r_original : r*self.radius_of_last_element/self.max_radius,
			    					radius : rc,
			    					group : group
			    		});			    		
			    		/*
			    		self.data_nodes.push({
			    			coordinate: coordinate
			    			});*/
			    		
			    		if(this.data_nodes[this.model.get("coordinate_set").indexOf(coordinate)])
			    			this.data_nodes[this.model.get("coordinate_set").indexOf(coordinate)].coordinate = coordinate;
			    		else
			    			this.data_nodes[this.model.get("coordinate_set").indexOf(coordinate)] = {coordinate:coordinate};
			    		r=r2;
			    		rc/=base_exp;
			    		theta+=(2*alpha);
			    		if(!coordinate.has("displacement_source_related"))
				    		break;
			    		else
			    			coordinate = coordinate.get("displacement_source_related").get("destination");
			    		
			    		progress+=progress_step;
			    		self.model.trigger("spiral_building",progress);
				    		
			    	}
			    				    	
			    	//coordinate_start = this.model.get("coordinate_set").at(index_in_collection+1); //because the collection is implicitly sorted by date
			    	group++;
			    	
		    	},this);
		    	self.model.trigger("spiral_built");
		    	//console.log(this.model.get("coordinate_set").toJSON());
		    },
		    create_svg : function(center,size){		    	
		    	var self = this;
		    	var x_offset = center.x;
		    	var y_offset = center.y;
		    	this.svg_width = size.w;
		    	this.svg_height = size.h;
		    	this.svg = d3.select(this.el)		    	
		    		.append("svg").attr("width", "100%").attr("height", this.svg_height);
		    	this.svg.append("rect").attr("width", this.svg_width).attr("height", this.svg_height).style("fill","white").style("stroke","steelblue");
		    	
		    	this.svg = this.svg.append("g").attr("transform", function(d) {
		    		return "translate(" + x_offset + "," + y_offset + ")";
		    	});
		    	
		    	var node = this.svg.selectAll(".node").data(this.data_nodes).enter().append("g");
		    	var text = node.append("text").style("text-anchor", "middle");
		    	text.append('tspan').attr("dy",0).attr("x",0).attr("class","tspan-first")
		    	text.append('tspan').attr("dy",15).attr("x",0).attr("class","tspan-second")
		    	text.append('tspan').attr("dy",15).attr("x",0).attr("class","tspan-third");
		    	node.append("image");
		    	node.append("circle").style("fill-opacity", .2 ).style("stroke", "steelblue").style("fill", "purple")		    	
		    	.on("click",function(d,i){
	    			self.radius_of_last_element = d.coordinate.get("geometry").r_original;
	    			d.coordinate.get("geometry").pageX = self.$el.position().left + self.$el.width();
	    			d.coordinate.get("geometry").pageY = d3.event.pageY;
	    			d.coordinate.select();	    			
	    		});
		    	node.append("title");		    	
		    	return this.svg;
		    },
		    update_svg : function(){
		    	var self = this;
		    	//console.log(this.data_nodes);
		    	//console.log(this.svg.selectAll("g").data(this.data_nodes).exit());
		    	//console.log(this.svg.selectAll("g").data(this.data_nodes).transition());
		    	node = this.svg.selectAll("g").data(this.data_nodes).exit().remove();		    	
		    	node = this.svg.selectAll("g").data(this.data_nodes).transition().attr("transform", function(d) {
		    		return "translate(" + d.coordinate.get("geometry").x + "," + d.coordinate.get("geometry").y + ")";
		    	});
		    	
		    	tblock = node.select("text").attr("font-size",function(d){
		    		if(d.coordinate.isNew()){
		    			return "50";
		    		}
		    	});
		    	tblock.select('tspan.tspan-first').text(function(d){
		    		if(d.coordinate.isNew()) return "";
		    		if (d.coordinate.has("name"))
		    			label = d.coordinate.get("name");
		    		else
		    			label = d.coordinate.get("address");
		    		if(d.coordinate.get("geometry").radius<self.max_circle_radius_hide_text) return "";
		    		return label.substring(0, d.coordinate.get("geometry").radius / 3);
		    		
		    	});
		    	tblock.select('tspan.tspan-second').text(function(d){		    		
		    		if (d.coordinate.has("name"))
		    			label = d.coordinate.get("name");
		    		else
		    			label = d.coordinate.get("address");
		    		
		    		if(d.coordinate.isNew()) return label;
		    		
		    		if(d.coordinate.get("geometry").radius<self.max_circle_radius_hide_newlines) return "";
		    		if(label.length>d.coordinate.get("geometry").radius / 3){
		    			return "[...]";
		    		}else{
		    			return "";
		    		}
		    	});
		    	tblock.select('tspan.tspan-third').text(function(d){
		    		if(d.coordinate.isNew()) return "";
		    		if (d.coordinate.has("name"))
		    			label = d.coordinate.get("name");
		    		else
		    			label = d.coordinate.get("address");
		    		
		    		if(d.coordinate.get("geometry").radius<self.max_circle_radius_hide_newlines) return "";
		    		if(label.length>d.coordinate.get("geometry").radius / 3){
		    			return label.substring(label.length-(d.coordinate.get("geometry").radius / 4));
		    		}else{
		    			return "";
		    		}
		    	});
		    	
		    	node.select("image")
		        	.attr("xlink:href", function(d) {
		        		if(d.coordinate.get("initiator") == constants.initiator_types.GPS){
		        			var url = app.server_config.STATIC_URL + "img/hitchgraph/";
		        			switch(d.coordinate.get("category")){
		        			case constants.gps_types.OK:
		        				 url+="signalOK.png";
		        				 break;
		        			case constants.gps_types.CAMP:
		        				url+="signalCAMP.png";
		        				break;
		        			case constants.gps_types.HELP:
		        				url+="signalHELP.png";
		        				break;
		        			case constants.gps_types.SOS:
		        				url+="signalSOS.png";
		        				break;
		        			}
		        			return url;
		        		}
		        		
		        	})
		        	.attr("width", function(d) {return d.coordinate.get("geometry").radius;})
		        	.attr("height", function(d) {return d.coordinate.get("geometry").radius;});

		    	node.select("circle").attr("r", function(d) {return d.coordinate.get("geometry").radius;})
		    		.style("stroke-width", function(d){
		    			if(d.coordinate.is_selected())
		    				return 10;
		    			else
		    				return 1;
		    		}).style("fill", function(d){
		    			if(d.coordinate.isNew()){ //end of a control group
		    				return "yellow";
		    			}
		    			if(d.coordinate.get("geometry").group%2)
		    				return "steelblue";
		    			else
		    				return "purple";
		    		}).style("stroke-dasharray",function(d){
		    			if(d.coordinate.isNew()){ //end of a control group
		    				return "5,5";
		    			}
		    		});
		    		
		    	node.select("title").text(function(d){		    			
			    		if (d.coordinate.has("name"))
			    			label = d.coordinate.get("name");
			    		else
			    			label = d.coordinate.get("address");

			    		return label;
			    	});		    	

		    },
		    render_svg : function(){
		    	var self = this;
		    	this.compute_spiral(this.spiral_options.base_exp,this.spiral_options.max_circle_radius);
		    	this.update_svg();
		    	
		    }
			

		});

	
	return views;
		
}); //-- End of requirejs define
