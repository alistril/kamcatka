define([
  'jquery',
  'underscore',
  'backbone',
  'hitchgraph/app',
  'handlebars',
  'templates'  
], function(
		$, _, Backbone, app,handlebars
		){
	
	var views = {};
		////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////// Views ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////

	views.ProgressView = Backbone.Marionette.ItemView.extend({
		ui : {
			bar: ".bar",
			progress: ".loading_progress",
			done	: ".loading_done"
		},
		complete : function(){
			this.progress(100);
			this.ui.progress.hide();
			this.ui.done.show();
			this.trigger("complete");
		},
		progress : function(p){
			this.ui.bar.width(p+"%");
		},
		serializeData: function(){			
			return app.server_config;
		},
		template: handlebars.templates["progress_bar.html"],
		tagName: "div"
	});
	
	views.DataProgressView = views.ProgressView.extend({
		collectionEvents: {
			"progress"  : "progress",
			"sync" 		: "complete"
		}
	});
	
	views.HitchgraphBuildingView = views.ProgressView.extend({
	});	
	
	views.SpiralBuildingView = views.HitchgraphBuildingView.extend({
		modelEvents: {
			"spiral_building"  	: "progress",
			"spiral_built"  	: "complete",
		},
		className	 : "row-fluid"
	});
	
	views.ModalsBuildingView = views.HitchgraphBuildingView.extend({
		collectionEvents: {
			"modals_building"  	: "progress",
			"modals_built"  	: "complete",
		},
		className	 : "row-fluid"
	});
	
	views.PopoversBuildingView = views.HitchgraphBuildingView.extend({
		collectionEvents: {
			"popovers_building"  	: "progress",
			"popovers_built"  		: "complete",
		},
		className	 : "row-fluid"
	});

	views.HitchgraphBuildingLayout = Backbone.Marionette.Layout.extend({
		template : handlebars.templates["hitchgraph_progress.html"],
		serializeData: function(){			
			return _(this.model.toJSON()).extend(app.server_config);
		},
		regions	 : {
			spiral	: "#progress-spiral",
			modals	: "#progress-modals",
			popovers: "#progress-popovers"
		},
		onRender : function(){					
			this.spiral.show(this.spiralview);
			this.modals.show(this.modalsview);
			this.popovers.show(this.popoversview);
		},		
		initialize : function(){
			this.spiral_complete = false;
			this.modals_complete = false;
			this.popovers_complete = false;
			this.spiralview = new views.SpiralBuildingView({ 	model : this.model });
			this.modalsview = new views.ModalsBuildingView({	 collection : this.model.get("coordinate_set")});
			this.popoversview = new views.PopoversBuildingView({collection : this.model.get("coordinate_set")});
			
			this.listenTo(this.spiralview, "complete", this.spiral_completed);
			this.listenTo(this.modalsview, "complete", this.modals_completed);
			this.listenTo(this.popoversview, "complete", this.popovers_completed);
		},
		spiral_completed : function(){
			if(!this.spiral_complete){ //spiral might be recomputed several times
				this.spiral_complete = true;
				this.check_completion();
			}
		},
		modals_completed : function(){
			this.modals_complete = true;
			this.check_completion();
		},
		popovers_completed : function(){
			this.popovers_complete = true;
			this.check_completion();
		},
		check_completion : function(){
			if(this.spiral_complete && this.modals_complete && this.popovers_complete){
				this.trigger("complete");
			}
		}

	}); 
	views.HitchgraphsBuildingView = Backbone.Marionette.CollectionView.extend({
		itemView : views.HitchgraphBuildingLayout, 
		className	 : "container-fluid",
		
		onItemviewComplete : function(){
			this.completed_nb++;
			if(this.completed_nb>=this.collection.length){
				this.trigger("complete");
			}
		},
		
		initialize : function(){
			this.completed_nb = 0;
		}
	});
		
	return views;
		
}); //-- End of requirejs define
