define([
  'jquery',
  'underscore',
  'gmaps',
  'backbone',  
  'hitchgraph/app',  
  //templates
  'handlebars',
  'templates',
  
  'hitchgraph/views_map',
  'hitchgraph/views_graph',
  'hitchgraph/views_progress'
], function(
		$, _, gmaps, Backbone, app,
		handlebars,templates,map,graph,progress
		){
	
	var views = {};
		////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////// Views ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////
		
		
			views.TabNavView = Backbone.Marionette.ItemView.extend({
				events: {
					"click a"	: "tab_selected"
				},
				tab_selected	: function(e){
					e.preventDefault();
					$("a",this.$el).tab('show');
					this.model.trigger("selected");
				},		
				template: handlebars.templates["tab_title.html"],
				tagName: "li"
			});
			
			views.GraphMapView = Backbone.Marionette.Layout.extend({
				template	: handlebars.templates["graph_map.html"],
				attributes : function () {				
				    return { id : "hitch" + this.model.id };
				},			
				regions	: {
					map: "#expedition_map",
					graph: "#expedition-hitchgraph #graph-content"
				},
				onRender : function(){
					this.map.show(new map.MapView({ model : this.model }));
					this.graph.show(new graph.HitchgraphView({model : this.model}));
				}
			});
			
			views.TabContentView = views.GraphMapView.extend({
				className	: "tab-pane"
			});
			
			views.TabNavCollectionView = Backbone.Marionette.CollectionView.extend({
				itemView		: views.TabNavView,
				tagName			: "ul",
				className		: "nav nav-tabs",			
				onRender :function(){
					 $("a:first",this.$el).tab('show');
				}
			});
			
			views.TabContentCollectionView = Backbone.Marionette.CollectionView.extend({
				itemView		: views.TabContentView,
				className		: "tab-content"
			});
			
			views.Tabs = Backbone.Marionette.Layout.extend({
				template: handlebars.templates["tabs.html"],
				regions: {
				    navs: "#itineraries-tabs-navs",
				    contents: "#itineraries-tabs-contents"
				 },
				 onRender : function(){
					//run in this order (nav needs contents)
					this.contents.show	(new views.TabContentCollectionView	({ collection 	: this.collection }));
					this.navs.show		(new views.TabNavCollectionView		({ collection 	: this.collection }));
				 }
			});

			views.LoadingHitchgraphsView = Backbone.Marionette.Layout.extend({
				template : handlebars.templates["general_progress.html"],
				regions	 : {
					progress_data: "#progress-data-load",
					progress_graphs: "#progress-hitchgraphs-build"
				},
				onRender : function(){					
					this.progress_data.show(this.dataview);
					this.progress_graphs.show(this.hgview);
				},
				initialize : function(){
					this.data_complete = false;
					this.hg_complete = false;
					
					this.dataview = new progress.DataProgressView({ collection : this.collection });
					this.hgview = new progress.HitchgraphsBuildingView({collection : this.collection});
					
					this.listenTo(this.dataview, "complete", this.data_completed);
					this.listenTo(this.hgview, "complete", this.hg_completed);
				},
				data_completed : function(){
					this.data_complete = true;
					this.check_completion();
				},
				hg_completed : function(){
					this.hg_complete = true;
					this.check_completion();
				},
				check_completion : function(){
					if(this.hg_complete && this.data_complete){
						this.trigger("complete");
					}
				}
			});

	return views;
		
}); //-- End of requirejs define
