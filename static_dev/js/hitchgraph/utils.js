define(['moment'], function(moment){
	var utils = {};

	utils.datetime_formatted = function(model){
		var ret;
		if(!model.has("datetime")){
			console.warn("No date&time specified for model",model);
			ret  = moment().format("LLLL");
		}else ret = model.get("datetime").format('LLLL');
		return ret;
	}
	
	return utils;
});