define([
  'jquery',
  'underscore',
  'gmaps',
  'backbone',  
  'hitchgraph/utils',
  'hitchgraph/app',
  //templates
  'handlebars',
  'templates'  
], function(
		$, _, gmaps, Backbone, utils, app,
		handlebars
		){
	
	var views = {};
		////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////// Views ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////
		
		//defines
		if(!views.MAP_HEIGHT)
			views.MAP_HEIGHT = 700;
				
		views.MarkerTooltipView = Backbone.Marionette.ItemView.extend({
			template : function(data){	
				var profiles = _(data.coordinatedescription_set).pluck("userprofile");
				var names = [];
				if(profiles.length && profiles[0]){
					names = _(profiles).pluck("username");
				}
				var text="";
				if(data.name){
					text = data.name+": ";    		
		    	}
				if(names.length)
					text += ("Travelling with " + names.join(","));
				
		    	if(data.datetime){ //almost always true
		    		text+= " (" + data.datetime_formatted + ")";
		    	}		    	
				return text;
			},
			serializeData : function(){
				var data = this.model.toJSON(); 
				data.datetime_formatted = utils.datetime_formatted(this.model);
				return data;
			},

			collectionEvents: {
			    "add": "render"
			},
		    initialize: function(options){ 
		    	this.marker = options.marker;
		    },		    
		    onRender : function(){   
		    	this.marker.setTitle(this.$el.html());
		    }
		});
		
		views.InfoWindowView = Backbone.Marionette.ItemView.extend({ //treat the whole bubble as an item
			template : handlebars.templates["infowindow.html"],
			collectionEvents: {
			    "add": "render"
			},
			serializeData: function(){
				var data = _(this.model.toJSON()).extend(app.server_config); 
				data.datetime_formatted = utils.datetime_formatted(this.model);
				return data;
			},
		    initialize: function(options){
		    	this.infowindow = options.infowindow;
		    },		    
		    onRender: function(){
		    	this.infowindow.setContent(this.$el.html());
		    }
		});
		
		views.MarkerView = Backbone.Marionette.ItemView.extend({
			template : function(){return "";},
			collectionEvents: {
			    "add": "render"
			},
			modelEvents: {
			    "selected"			: "coordinateSelected",
			    "change:informed"	: "coordinateInformed",
			    "change:location" 	: "location_changed"
			},
			location_changed : function(model,location){				
				this.marker.setPosition(location);
			},
			coordinateInformed : function(model,informed){
				this.marker.setAnimation(null);
			},
		    initialize: function(options){
		    	var self = this;
		    	this.map = options.map;
		    	this.collection = this.model.get("coordinatedescription_set");
		    			    			
		    	this.marker = new gmaps.Marker({
					 position: this.model.get("location"),
					 map: this.map,
					 draggable : (this.model.get("draggable") && this.model.is_selected()) 
		    	});

		    	this.infowindow = new gmaps.InfoWindow();
		    	this.infowindow.setPosition(this.marker.getPosition());
		    	
		    	_(this).bindAll('marker_clicked');		    	
		    	gmaps.event.addListener(this.marker, 'click', this.marker_clicked);
		    	gmaps.event.addListener(this.marker, "drag", function(event) {
					self.model.set("location",self.marker.getPosition());					
				});
		    	
		    	this._markerTooltipView = new views.MarkerTooltipView({
		    		model		: this.model,
		    		collection 	: this.model.get("coordinatedescription_set"),
		    		marker 		: this.marker   		
		    	});
		    	
		    	this._infoWindowView = new views.InfoWindowView({
		    		model		: this.model, 
		    		collection 	: this.model.get("coordinatedescription_set"),
		    		infowindow 	: this.infowindow   		
		    	});		
		    },
		    
		    marker_clicked : function(event){
		    	if(this.model.get("draggable")){
		    		this.model.select();
		    	}else{
		    		this.infowindow.open(this.marker.getMap());
		    	}
		    },		    
		    
		    coordinateSelected : function(hitchgraph, selected){		  
		    	var pinIcon;
		    	if(selected){
		    		this.old_zIndex = this.marker.getZIndex();
		    		this.marker.set("zIndex", gmaps.Marker.MAX_ZINDEX);
		    				    		
		    		if(this.model.isNew()){
		    			pinIcon = this._buildMarkerIcon({selected:true, size:new google.maps.Size(39*1.5,34*1.5)});
		    			this.marker.setAnimation(gmaps.Animation.BOUNCE);
		    		}else{
		    			pinIcon = this._buildMarkerIcon({selected:true, size:new google.maps.Size(30,51)});
		    			this.marker.setAnimation(gmaps.Animation.DROP);
		    		}
		    		this.map.setCenter(this.model.get("location"));
		    		if(this.model.get("draggable")){
		    			this.marker.setDraggable(true);
		    		}
		    	}else{
		    		this.marker.setDraggable(false);		    		
		    		this.marker.setAnimation(null);
		    		pinIcon = this._buildMarkerIcon();
		    		this.marker.set("zIndex", this.old_zIndex);
		    	}
		    	
		    	this.marker.setIcon(pinIcon);
		    },
		    _buildMarkerIcon : function(options){
		    	if(!options) options ={};
		    	if(this.model.isNew()){		    		
		    		image =  "http://www.google.com/mapfiles/arrow.png";		    		
		    	}else if(this.model.has_details()){
		    		if(options.selected)
		    			image =  "http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";
		    		else
		    			image =  "http://maps.google.com/mapfiles/marker_purpleI.png";		    		
		    	}else{
		    		if(options.selected)
		    			image = "http://maps.google.com/mapfiles/marker_orange.png"
		    		else
		    			image = "http://maps.google.com/mapfiles/marker.png"		    		
		    	}
		    	
		    	if(!options.size){
		    		if(this.model.isNew()){
		    			options.size = new google.maps.Size(39,34);
			    	}else{
			    		if(options.selected)
			    			options.size = new google.maps.Size(30,51);
			    		else
			    			options.size = new google.maps.Size(20,34);
			    	}
		    	}
    			var pinIcon = new google.maps.MarkerImage(
    					image,
    				    null, /* size is determined at runtime */
    				    null, /* origin is 0,0 */
    				    null, /* anchor is bottom center of the scaled image */
    				    options.size
    				);  
    			return pinIcon;
		    },
		    onRender : function(){		    	
		    	this._markerTooltipView.render();
		    	this._infoWindowView.render();
		    	var pinIcon = this._buildMarkerIcon({selected : this.model.is_selected()});
		    	this.marker.setIcon(pinIcon);		    	
		    },
		    onClose : function(){
		    	this.marker.setMap(null);
		    }
		});
		
		views.MarkerCollectionView = Backbone.Marionette.CollectionView.extend({
			itemView: views.MarkerView,
			itemViewOptions: function(model) {
			    return {
			      map 	: this.map
			    };   
			},
			onItemAdded : function(itemView){
				this.bounds.extend (itemView.model.get("location"));  
			},
		    initialize: function(options){
		    	this.map = options.map;
		    	this.bounds = new gmaps.LatLngBounds ();
		    	this.collection.each(function(coordinate){
		    		this.bounds.extend (coordinate.get("location"));
		    	},this);		    	
		    }
		});
		
		views.ArrowView = Backbone.Marionette.View.extend({
		    initialize: function(options){
		    	this.map = options.map;
		    	this.listenTo(this.model.get("source"), "change:location", this.render);
		    	this.listenTo(this.model.get("destination"), "change:location", this.render);
		    	
		    	var lineSymbol = {
					path: gmaps.SymbolPath.FORWARD_CLOSED_ARROW
				};
		    	this.line = new gmaps.Polyline(
			    		 {
					       path: [this.model.get("source").get("location"),this.model.get("destination").get("location")],
					       icons: [{
					    	   		icon: lineSymbol,
					    	   		offset: '100%'
					       			}],
					       map: this.map
			    		 }
			    );

		    },		    
		    render : function(){
		    	this.line.setPath([this.model.get("source").get("location"),this.model.get("destination").get("location")]);
		    },
		    onClose : function(){
		    	this.line.setMap(null); //remove line		    	
		    }
		});
		
		views.ArrowCollectionView = Backbone.Marionette.CollectionView.extend({
			
			itemView: views.ArrowView,
			itemViewOptions: function(model) {			    
			    return {
			      map 	: this.map
			    };   
			},
		    initialize: function(options){
		    	this.map = options.map;
		    }
		});
		
		views.MapView = Backbone.Marionette.View.extend({
			modelEvents: {
			    "selected": "map_selected"
			},
			className : "map_canvas",			
			map_selected: function(){				
				gmaps.event.trigger(this.map, 'resize');
				this.map.setZoom(this.map.getZoom());
				this.map.fitBounds (this._markerCollectionView.bounds);
			},
			initialize: function() {
				var self = this;
				var mapOptions = {
						mapTypeId: gmaps.MapTypeId.ROADMAP
				}
				this.map = new gmaps.Map(this.el, mapOptions);				
								
				this._markerCollectionView = new views.MarkerCollectionView({
					collection : this.model.get("coordinate_set"),
					map : this.map
				});
				
				this._arrowCollectionView = new views.ArrowCollectionView({
					collection : this.model.get("displacement_set"),
					map : this.map
				});
				
		    	// bind the functions 'add' and 'remove' to the view.
		    	_(this).bindAll('add_coordinate'/*, 'remove_coordinate'*/);
		   	 
		        // bind this view to the add and remove events of the collection!
		    	this.model.get("coordinate_set").bind('add', this.add_coordinate);
		        //this.model.get("coordinate_set").bind('remove', this.remove_coordinate);
		    	//make sure map is square
		    	this.el_width = this.$el.width();
		    	setInterval(function(){
		    		if(Math.abs(self.el_width-self.$el.width())>100){
		    			self.el_width = self.$el.width();
		    			self.$el.css("width","100%").css("height",self.$el.width());
		    			gmaps.event.trigger(self.map, 'resize')
		    			if(self._markerCollectionView)
		    				self.map.fitBounds (self._markerCollectionView.bounds);
		    		}
		    	},1000);
			},
			
			//fit bounds when a new coordinate is added
			add_coordinate : function(coordinate){
				if (this._rendered) {
					this.map.fitBounds (this._markerCollectionView.bounds);
		        }
			},
			
			render : function(){				
				// We keep track of the rendered state of the view
		        this._rendered = true;
		        
				this._markerCollectionView.render();
				this._arrowCollectionView.render();
				this.map.fitBounds (this._markerCollectionView.bounds);
				var height;
				if(this.el_width && this.el_width>100) 
					height = this.el_width 
				else 
					height = views.MAP_HEIGHT; 
				this.$el.css("width","100%").css("height",height);				
			}
		}); //-- End of Map view
	
	return views;
		
}); //-- End of requirejs define
