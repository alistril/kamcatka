$(function(){
	$(".hitchgraph .toolbar .details-show").click(function(){
		$(this).parent("li").addClass("active");
		$(".hitchgraph .toolbar .details-hide").parent("li").removeClass("active");
		$('.hitchgraph-blocks',$(this).parents(".hitchgraph")).removeClass("scrollable");
	});
	
	$(".hitchgraph .toolbar .details-hide").click(function(){
		$(this).parent("li").addClass("active");
		$(".hitchgraph .toolbar .details-show").parent("li").removeClass("active");
		$('.hitchgraph-blocks',$(this).parents(".hitchgraph")).addClass("scrollable");
	});
	
});
