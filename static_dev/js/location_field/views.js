define([
  'jquery',
  'underscore',
  'gmaps',
  'backbone',  
  'location_field/app'
], function(
		$, _, gmaps, Backbone, app
		){
	
	var views = {};
		////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////// Views ///////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////
		
		views.LocationInput = Backbone.Marionette.View.extend({
			el : "#id_location",
			events : {
				"change" : "input_changed"
			},
			modelEvents : {
				"change:location": "location_changed" 
			},
			location_changed : function(){
				var lat = Math.round(this.model.get("location").lat()*10000)/10000;
				var lng = Math.round(this.model.get("location").lng()*10000)/10000;
				this.$el.val(lat+","+lng);
			},
			input_changed : function(){
				var arr = this.$el.val().split(",");
				this.model.set("location",new gmaps.LatLng(arr[0],arr[1]));
			},
			initialize 		: function(){
				if(this.model.has("location"))
					this.$el.val(this.model.get("location").lat() + "," + this.model.get("location").lng());
				else
					console.warn("location expected");
			}
		});
		
		views.PlaceInput = Backbone.Marionette.View.extend({
			events : {
				"change" : "input_changed",
				"keyup"  : "input_changed"
			},
			modelEvents : {
				"change:location": "location_changed",
				"change:name"	:  "name_changed"
			},
			name_changed : function(model,name){
				this.$el.val(name);
			},
			location_changed : function(){
				var self = this;
	            if(!self.$el.is(":focus")){
		            if (this.geocoder) {
		                this.geocoder.geocode({"latLng": this.model.get("location")}, function(results, status) {
		                	if (status == google.maps.GeocoderStatus.OK) {
	                    		var arrAddress = results[0].address_components;
	                    		
	                    		var found = false;
		                    	_(["country","locality","postal_town"]).each(function(type){
		                    		_(arrAddress).each(function (address_component) {
			                    	    if (address_component.types[0] == type){ // locality type			                    	    	
			                    	    	self.model.set("name",address_component.long_name);
			                    	    	found = true;
			                    	        return false; // break the loop
			                    	    }
			                        });
		                    	});		                    	
		                    	if(!found){
		                    		self.model.set("name","Unknown");
		                    	}
		                    }else{
	                    		self.model.set("name","Unknown");
		                    }
		                });
		            }
	            }
			},
			input_changed : function(){
				var self = this;
		        
	            if (this.geocoder) {
	                this.geocoder.geocode({"address": this.$el.val()}, function(results, status) {
	                    if (status == gmaps.GeocoderStatus.OK) {	                    	
	                    	self.model.set("location",results[0].geometry.location);	                        
	                    }
	                });
	            }
			},
			initialize 	: function(){
				this.geocoder = new gmaps.Geocoder();
				if(this.model.has("address"))
					this.$el.val(this.model.get("address"));
				else
					console.warn("location expected")
			}
		});
		
		views.MapView = Backbone.Marionette.View.extend({
			el	:	"#map_location",
			modelEvents : {
				"change:location": "location_changed",				
			},
			location_changed : function(model,loc){
				this.render();
			},
			render: function() {
				this.map.panTo(this.model.get("location"));
				
				this.marker.setPosition(this.model.get("location"));
			},			
			initialize : function(options){
				var self = this;
				var mapOptions = {
					mapTypeId: gmaps.MapTypeId.ROADMAP
				}
				this.map = new gmaps.Map(this.el, mapOptions);
				this.map.setZoom(7);
				if(this.model.has("location"))
					this.map.setCenter(this.model.get("location"));
				this.marker = new gmaps.Marker({
					 position: this.model.get("location"),
					 map: this.map,
					 draggable : true
		    	});
				gmaps.event.addListener(this.marker, "dragend", function(event) {
					self.model.set("location",self.marker.getPosition());
				});
				gmaps.event.addListener(this.map, 'click', function(event) {				    
				    self.model.set("location",event.latLng);
				});

				this.$el.css("width","100%").css("height","100%");
			}
		}); //-- End of Map view
		
		views.LocationLayout = Backbone.Marionette.Layout.extend({			  
		  regions: {
		    location_input	: "#text-container",
		    map				: "#map-container"
		  },
		  initialize : function(){			  
		  }
		});

	
	return views;
		
}); //-- End of requirejs define
