define([
  'underscore',
  'backbone',
  'gmaps',
  'location_field/app',
], function(_, Backbone, gmaps, app){
	
	var models = {}
		////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////// Models //////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////
		
		models.Coordinate = Backbone.Model.extend({
			initialize : function(data){
				if(!data) return;
				if(_(data).has("location")){
					var location_arr = data.location.split(",");
					this.set("location",new gmaps.LatLng(location_arr[0],location_arr[1]));
				}
			}
		});
				
	
	return models;
	
}); //define
