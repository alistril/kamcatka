// Filename: main.js

// Require.js allows us to configure shortcut alias
// There usage will become more apparent further along in the tutorial.
require.config({
  baseUrl: server_context.STATIC_URL + "js/",
  paths: {	
	//require.js plugins
    async						: 'libs/require/plugins/async'							,
	
	//other libs
	backbone					: 'libs/backbone/backbone'							,
	'backbone.babysitter'		: 'libs/backbone/babysitter/backbone.babysitter.min'	,
	'backbone.marionette'		: 'libs/backbone/marionette/backbone.marionette.min'	,
	'backbone.relational'		: 'libs/backbone/relational/backbone-relational'		,
	'backbone.wreqr'			: 'libs/backbone/wreqr/backbone.wreqr.min'				,
	bootstrap					: 'bootstrap'										,
	datetimepicker				: '../datetimepicker/js/bootstrap-datetimepicker.min'	,
	d3							: 'libs/d3/d3.v3'										,
	fileupload					: '../bootstrap-fileupload/bootstrap-fileupload'		,
	handlebars					: 'libs/handlebars/handlebars'							,    	
	'iframe-transport'			: 'libs/iframe-transport/jquery.iframe-transport'		,    
    jquery						: 'libs/jquery/jquery-1.9.1.min'						,
	json_js						: 'libs/JSON-js/json.js'								,	
	moment						: 'libs/moment/moment'									,	
    underscore					: 'libs/underscore/underscore-min'						,	
	
	templates					: 'hitchgraph/templates',


  },
  shim:
	{
		backbone					: {
	      deps						: ['underscore', 'jquery'],
	      exports					: 'Backbone'
	    },		
		'backbone.relational'		: {
		  deps						: ['backbone'],
		},
	    bootstrap					: {
	      deps						: ["jquery"],
	      exports					: "$.fn.popover"
		},
		datetimepicker				: {
		  deps 						: ['jquery'/*,'bootstrap'*/]
		},
		d3							: { exports : 'd3' },
		fileupload					: {
		  deps						: ['jquery'/*,'bootstrap'*/]
		},
        handlebars					: {
          exports					: 'Handlebars'
        },
        'iframe-transport'			: {
          deps						: ['jquery']
        },
		jquery						: {
		  exports 					: '$'
		},
		json_js 					: {
		  exports 					: "JSON"
		},
	    underscore 					: {
	      exports 					: "_"
	    },
	    moment 						: {
		  exports 					: "moment"
		},
		templates : {
        	deps 					: ['handlebars'],
        	exports: 				  'Handlebars'
        }
	}
});

define('gmaps', ['async!http://maps.google.com/maps/api/js?v=3&sensor=false'],
		function(){
		    // return the gmaps namespace for brevity
		    return window.google.maps;
		});

require(['behaviours/your_way','jquery','bootstrap'], function(Behaviour,$){
  // The "behaviour" dependency is passed in as "Behaviour"
	
	Behaviour.server_config = server_context;
	Behaviour.start(server_context.app_args);
});
