// Filename: main.js

// Require.js allows us to configure shortcut alias
// There usage will become more apparent further along in the tutorial.
require.config({
  baseUrl: server_context.STATIC_URL + "js/",
  paths: {	
	//require.js plugins
    async						: 'libs/require/plugins/async'							,
	
	//other libs
	backbone					: 'libs/backbone/backbone'								,	
	'backbone.babysitter'		: 'libs/backbone/babysitter/backbone.babysitter'		,
	'backbone.marionette'		: 'libs/backbone/marionette/backbone.marionette'		,
	'backbone.relational'		: 'libs/backbone/relational/backbone-relational'		,
	'backbone.wreqr'			: 'libs/backbone/wreqr/backbone.wreqr'					,
	bootstrap					: 'bootstrap',
	datepicker 					: '../datepicker/js/bootstrap-datepicker'				,
	d3							: 'libs/d3/d3.v3'										,	
	handlebars					: 'libs/handlebars/handlebars'							,    	
    jquery						: 'libs/jquery/jquery-1.9.1.min'						,
	json_js						: 'libs/JSON-js/json.js'								,	
	moment						: 'libs/moment/moment.min'								,	
    underscore					: 'libs/underscore/underscore-min'						,	
	
	templates					: 'hitchgraph/templates',


  },
  shim:
	{
		backbone					: {
	      deps						: ['underscore', 'jquery'],
	      exports					: 'Backbone'
	    },
		bootstrap					: {
	      deps						: ["jquery"],
	      exports					: "$.fn.popover"
		},
		'backbone.relational'		: {
		  deps						: ['backbone'],
		},
		datepicker					: {
		  deps 						: ['jquery']
		},
		d3							: { exports : 'd3' },
        handlebars					: {
          exports					: 'Handlebars'
        },
		jquery						: {
		  exports 					: '$'
		},
		json_js 					: {
		  exports 					: "JSON"
		},
	    underscore 					: {
	      exports 					: "_"
	    },
	    moment 						: {
		  exports 					: "moment"
		},
		templates : {
        	deps 					: ['handlebars'],
        	exports: 				  'Handlebars'
        }
	}
});

define('gmaps', ['async!http://maps.google.com/maps/api/js?v=3&sensor=false'],
		function(){
		    // return the gmaps namespace for brevity
		    return window.google.maps;
		});

require(['behaviours/hitchgraph_map','jquery','bootstrap'], function(Behaviour,$){
  // The "behaviour" dependency is passed in as "Behaviour"
	
	Behaviour.server_config = server_context;
	Behaviour.start(server_context.app_args);
});
