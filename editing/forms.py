from ckeditor.fields import RichTextField

class MyRichTextField(RichTextField):
    def is_hidden(self):
        return False