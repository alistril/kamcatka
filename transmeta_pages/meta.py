from transmeta import TransMeta
from mezzanine.pages.models import Page

class MetaPageTranslate(Page.__metaclass__, TransMeta):
    pass
